/*
const express = require('express')
const path = require('path')
const PORT = process.env.PORT || 5000

express()
  .use(express.static(path.join(__dirname, 'public')))
  .set('views', path.join(__dirname, 'views'))
  .set('view engine', 'ejs')
  .get('/', (req, res) => res.render('pages/index'))
  .listen(PORT, () => console.log(`Listening on ${ PORT }`))
*/

const app = require('express')();
const express = require('express');
const path = require('path');

const http = require('http').Server(app);
const io = require('socket.io')(http);

app.use(express.static(path.join(__dirname, 'public')));

app.get('/', (req, res) => {
  res.sendFile(__dirname + '/views/pages/index.html');
});

app.get('/text', (req, res) => {
  res.sendFile(__dirname + '/views/pages/text.html');
});

app.get('/text1', (req, res) => {
  res.sendFile(__dirname + '/views/pages/text1.html');
});

app.get('/video', (req, res) => {
  res.sendFile(__dirname + '/views/pages/video.html');
});

app.get('/map', (req, res) => {
  res.sendFile(__dirname + '/views/pages/map.html');
});

app.get('/dashboard', (req, res) => {
  res.sendFile(__dirname + '/views/pages/dashboard.html');
});

/*==================== Testing pages ====================*/

app.get('/text_insert', (req, res) => {
  res.sendFile(__dirname + '/views/pages/text_insert.html');
});

app.get('/map_insert', (req, res) => {
  res.sendFile(__dirname + '/views/pages/map_insert.html');
});

app.get('/video_insert', (req, res) => {
  res.sendFile(__dirname + '/views/pages/video_insert.html');
});

app.get('/text_consume', (req, res) => {
  res.sendFile(__dirname + '/views/pages/text_consume.html');
});

app.get('/map_consume', (req, res) => {
  res.sendFile(__dirname + '/views/pages/map_consume.html');
});

app.get('/video_consume', (req, res) => {
  res.sendFile(__dirname + '/views/pages/video_consume.html');
});

io.on('connection', (socket) => {

  //socket.on('chat message', (msg) => {
    //send message to everybody
    //io.emit('chat message', msg);

    //send message to everybody except the sender
    //socket.broadcast.emit('chat message', msg);
  //});
  console.log('a user connected');
  socket.on('disconnect', () => {
    console.log('user disconnected');
  });

  socket.on('chat message', (msg) => {
  io.emit('chat message', msg);
  });

  socket.on('ann color', (annotationsJSON) => {
    io.emit('ann color', annotationsJSON);
  });

  socket.on('setup annotations', (annotationsJSON) => {
    io.emit('setup annotations', annotationsJSON);
  });


  socket.on('videoduration', (duration) => {
    io.emit('videoduration', duration);
  });

  socket.on('videocurrenttime', (current) => {
    io.emit('videocurrenttime', current);
  });

  socket.on('start', (start) => {
    io.emit('start', start);
  });

  socket.on('img annotation', (imageAnnotation) => {
    io.emit('img annotation', imageAnnotation);
  });

  socket.on('holo annotation', (holoAnnotation) => {
    io.emit('holo annotation', holoAnnotation);
  });

  socket.on('annotationOK', (confirmation) => {
    io.emit('annotationOK', confirmation);
  });

  socket.on('HololensAnnotationOK', (confirmation) => {
    io.emit('HololensAnnotationOK', confirmation);
  });

  socket.on('relayout', (confirmation) => {
    io.emit('relayout', confirmation);
  });

  socket.on('videorelayout', (videoannotationsJSON) => {
    io.emit('videorelayout', videoannotationsJSON);
  });

  /*==================== dashboard  ====================*/
  socket.on('loadTextScene', (confirmation) => {
    io.emit('loadTextScene', confirmation);
  });

  socket.on('loadMapScene', (confirmation) => {
    io.emit('loadMapScene', confirmation);
  });

  socket.on('loadVideoScene', (confirmation) => {
    io.emit('loadVideoScene', confirmation);
  });

  socket.on('loadConsumeTextScene', (confirmation) => {
    io.emit('loadConsumeTextScene', confirmation);
  });

  socket.on('loadConsumeMapScene', (confirmation) => {
    io.emit('loadConsumeMapScene', confirmation);
  });

  socket.on('loadConsumeVideoScene', (confirmation) => {
    io.emit('loadConsumeVideoScene', confirmation);
  });

  socket.on('reloadWebpage', (confirmation) => {
    io.emit('reloadWebpage', confirmation);
  });

  socket.on('loadTextPage', (confirmation) => {
    io.emit('loadTextPage', confirmation);
  });

  socket.on('loadMapPage', (confirmation) => {
    io.emit('loadMapPage', confirmation);
  });

  socket.on('loadVideoPage', (confirmation) => {
    io.emit('loadVideoPage', confirmation);
  });

  socket.on('loadConsumeTextPage', (confirmation) => {
    io.emit('loadConsumeTextPage', confirmation);
  });

  socket.on('loadConsumeMapPage', (confirmation) => {
    io.emit('loadConsumeMapPage', confirmation);
  });

  socket.on('loadConsumeVideoPage', (confirmation) => {
    io.emit('loadConsumeVideoPage', confirmation);
  });

});

http.listen(process.env.PORT || 5000, () => {
  console.log('listening on *:5000');
});
