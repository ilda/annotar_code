#Mobile-Headset project


##Connection

The library used for web is [Socket.Io](https://socket.io/). [Here](https://socket.io/get-started/chat)'s a Get Started tutorial to check the basics of this library on Node.js.

The library used for Unity is [Socket.IO Client Library for Unity](https://github.com/floatinghotpot/socket.io-unity).


This folder contains the same files as the one hosted on Heroku on [https://mobile-headset.herokuapp.com](https://mobile-headset.herokuapp.com), with some small modifications for cleaning the structure. 

It contains *index.js*, which handles all the server-side functions.

A html media page needs to have:

```
<script src="/socket.io/socket.io.js"></script>
```

And then the .js file connected to it, containing:

```
var socket = io();
```

is able to connect with the server, which will then handle a message.

Let's look at an example.

From the dashboard page *view/pages/dashboard.html/*, or (*https://mobile-headset.herokuapp.com/dashboard/*), the user wants to reload the Insert Text scene in Unity. 

So they click on the **Load scene INSERT Text** button.

From */js/main_dashboard.js*, the following function is activated:

```
buttonText.onclick = function () {
  socket.emit('loadTextScene', confirmation);
};
```

This sends the **loadTextScene** message to the server.

In *index.js*, the message is received in:

```
io.on('connection', (socket) => {
	[...]
	socket.on('loadTextScene', (confirmation) => {
	io.emit('loadTextScene', confirmation);
	});
	[...]
}
```

In which the message is then sent to the clients (note that in this project no received is specified, as the client that receives it is always related to which page/scene is opened, in but it can be done and it should, in case a specific message could be in conflict with the client that is open in the moment the message is received).

In Unity, the initial connection is setup in this way in the Start() method of the script inside the **ConnectionObject** in any scene:

```
socket = IO.Socket("http://mobile-headset.herokuapp.com/");

socket.On(QSocket.EVENT_CONNECT, () =>
{
    Debug.Log("Connected");
    socket.Emit("chat", "test");
});
```

Lastly, in Unity, in the scene that it's currently opened, the message is received in the Start() method in:

```
socket.On("loadTextScene", confirmation =>
{
    receivedLoadText = true;
});
```

Inside the Update() method, Unity checks if a message has set any of the booleans to true, which then sets up the events related to it. In this case:

```
if (receivedLoadText)
{
    receivedLoadText = false;
    SceneManager.LoadScene("TextTabletScene");
}
```

Sending a message from Unity to the Web Client is really similar. From example, if an annotation is received correctly, the script inside the ConnectionObject sends the message with:

```
socket.Emit("annotationOK", "ok");
```

and *index.js* receives it in:

```
io.on('connection', (socket) => {
	[...]
	socket.on('annotationOK', (confirmation) => {
		io.emit('annotationOK', confirmation);
	});
	[...]
}
```

then the .js file, related to a specific media page, receives the message in:

```
socket.on('annotationOK', function(confirmation) {
 [...]
});
```

and can execute the necessary code. 


##Structure

The structure of the project is:

In this folder, *index.js* handles all the functionalities of the app, including routing and the connection. Hence, for example:

```
app.get('/text_insert', (req, res) => {
  res.sendFile(__dirname + '/views/pages/text_insert.html');
});
```
connects the page *text_insert* to the url *https://mobile-headset.herokuapp.com/text_insert*.


Then, each media has its *\_consume.html* and *\_insert.html* files in the */views/pages* folder (example: *text\_consume.html* and *text\_insert.html*).
Each of these files has its own individual .js file in */public/js*.

In Unity:

The structure is really similar, The Insert scenes are:

- TextMapScene;
- TextTabletScene;
- VideoTabletScene;

and the Consume scenes are:

- ConsumeTextScene;
- ConsumeMapScene;
- ConsumeVideoScene;

Each scene contains a ConnectionObject, which has a Script attached to it. This script is the one that handles the received messages, and consequentially all the methods related to them, like inserting annotations and setting their position in the view.

For the project to work, the mobile device should have the browser on the chosen page, and the Hololens on the respective scene.

A dashboard *view/dashboard.html* gives the user the possibility of **moving between all the pages/scenes previously mentioned**. If one of those pages/scene is opened, then clicking on one of the buttons in the dashboard will make it load the corresponding page/scene. The ideal setup is to have the dashboard open on a separate device, like a laptop, to have the mobile device and the hololens moving to the selected pages/scenes. 

p.s. All the other pages (with their related .js files) are old versions and unnecessary.

##To Solve:

-  A ray occasionaly spawing in the Unity scene, basically freezing it and not allowing the user to interact with it. Sometimes reloading the scene through the dashboard fixes the problem.

- Repetitions of code in Javascript files: as mentioned, each page has its own .js file, however a lot of functions are repeated in between those. This is a problem because, if the editor wants to change a common function, they have to change it on all the separate files. 
This problem is also present in having separate scripts in each ConnectionObject scene in Unity, as there are big common functions that could be potentially separated. 

---

##Standard notes for the initial Node.js Heroku setup

These notes are standard when you download the initial Node.js Heroku folder, and explain how to setup a project.

## node-js-getting-started

A barebones Node.js app using [Express 4](http://expressjs.com/).

This application supports the [Getting Started on Heroku with Node.js](https://devcenter.heroku.com/articles/getting-started-with-nodejs) article - check it out.

## Running Locally

Make sure you have [Node.js](http://nodejs.org/) and the [Heroku CLI](https://cli.heroku.com/) installed.

```sh
$ git clone https://github.com/heroku/node-js-getting-started.git # or clone your own fork
$ cd node-js-getting-started
$ npm install
$ npm start
```

Your app should now be running on [localhost:5000](http://localhost:5000/).

## Deploying to Heroku

```
$ heroku create
$ git push heroku main
$ heroku open
```
or

[![Deploy to Heroku](https://www.herokucdn.com/deploy/button.png)](https://heroku.com/deploy)

## Documentation

For more information about using Node.js on Heroku, see these Dev Center articles:

- [Getting Started on Heroku with Node.js](https://devcenter.heroku.com/articles/getting-started-with-nodejs)
- [Heroku Node.js Support](https://devcenter.heroku.com/articles/nodejs-support)
- [Node.js on Heroku](https://devcenter.heroku.com/categories/nodejs)
- [Best Practices for Node.js Development](https://devcenter.heroku.com/articles/node-best-practices)
- [Using WebSockets on Heroku with Node.js](https://devcenter.heroku.com/articles/node-websockets)
