/*============== SOCKET.IO ==============*/

var socket = io();
 var okay = 1;
 /*var form = document.getElementById('form');
 var input = document.getElementById('input');

 form.addEventListener('submit', function(e) {
   e.preventDefault();
   if (input.value) {
     socket.emit('chat message', input.value);
     input.value = '';
   }
 });*/

   socket.on('okay', function() {
      var okay = 1;
    });

    socket.on('reloadWebpage', function() {
      window.location.reload();
    });

    socket.on('loadTextPage', function() {
      window.location = "https://mobile-headset.herokuapp.com/text_insert";
    });

    socket.on('loadMapPage', function() {
      window.location = "https://mobile-headset.herokuapp.com/map_insert";
    });

    socket.on('loadVideoPage', function() {
      window.location = "https://mobile-headset.herokuapp.com/video_insert";
    });

    socket.on('loadConsumeTextPage', function() {
      window.location = "https://mobile-headset.herokuapp.com/text_consume";
    });

    socket.on('loadConsumeMapPage', function() {
      window.location = "https://mobile-headset.herokuapp.com/map_consume";
    });

    socket.on('loadConsumeVideoPage', function() {
      window.location = "https://mobile-headset.herokuapp.com/video_consume";
    });

/*============== Menu ==============*/
    $( "#menu_open" ).hide();

    $( "#menu_button" ).click(function() {
      var $this=$(this);
      $this.hide();
      $( "#menu_open" ).show('fast', 'linear', function() {
           //callback function after animation finished
      });
    });

    $( "#topmenu-x" ).click(function() {
      var $this=$(this);
      $( "#menu_open" ).hide();
      $( "#menu_button" ).show('fast', 'linear', function() {
           //callback function after animation finished
      });
    });

/*============== Map Handler ==============*/
    let markers = [];
    let drawingLayers = [];
    var mymap = L.map('mapid', { zoomControl: false }).setView([48.858255, 2.348092], 17);

    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        maxZoom: 17,
        minZoom: 17,
        id: 'mapbox/streets-v11',
        tileSize: 512,
        zoomOffset: -1,
        accessToken: 'pk.eyJ1IjoiZmRpZ2lvaWEiLCJhIjoiY2tvMnlrZW1qMTQ4MTJycGdreWxjeDdwOCJ9.ywsPzKb9eBo9-tNNqUvEfg'
    }).addTo(mymap);


        // create a polygon from an array of LatLng points
    var latlngsExternal = [[48.86440892046152, 2.3466324806213383],[48.86255501343389, 2.3543858536868356],[48.852578495367275, 2.348520756422659],[48.85541165237536, 2.3414254195631656]];
    var polygonBoundsExternal = L.polygon(latlngsExternal, {color: '#fe6f5e', fillColor: '#C0C0C0', weight: '5', dashArray: '10'}).addTo(mymap);


    var colorIcons = [];
    var colors = ["#1B9E77", "#D95F02", "#7570B3", "#E7298A", "#66A61E", "#E6AB02", "#A6761D"];
    var indexColor = 0;

    var icon1B9E77 = L.icon({
      iconUrl: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|1B9E77&chf=a,s,ee00FFFF',

        iconSize:     [21, 34] // size of the icon
    });
    colorIcons.push(icon1B9E77);

    var iconD95F02 = L.icon({
      iconUrl: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|D95F02&chf=a,s,ee00FFFF',

        iconSize:     [21, 34] // size of the icon
    });
    colorIcons.push(iconD95F02);


    var icon7570B3 = L.icon({
      iconUrl: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|7570B3&chf=a,s,ee00FFFF',

        iconSize:     [21, 34] // size of the icon
    });
    colorIcons.push(icon7570B3);

    var iconE7298A = L.icon({
      iconUrl: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|E7298A&chf=a,s,ee00FFFF',

        iconSize:     [21, 34] // size of the icon
    });
    colorIcons.push(iconE7298A);

    var icon66A61E = L.icon({
      iconUrl: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|66A61E&chf=a,s,ee00FFFF',

        iconSize:     [21, 34] // size of the icon
    });
    colorIcons.push(icon66A61E);

    var iconE6AB02 = L.icon({
      iconUrl: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|E6AB02&chf=a,s,ee00FFFF',

        iconSize:     [21, 34] // size of the icon
    });
    colorIcons.push(iconE6AB02);

    var iconA6761D = L.icon({
      iconUrl: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|A6761D&chf=a,s,ee00FFFF',

        iconSize:     [21, 34] // size of the icon
    });
    colorIcons.push(iconA6761D);

    var greenIcon = L.icon({
        iconUrl: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|00FF00&chf=a,s,ee00FFFF',

        iconSize:     [21, 34] // size of the icon
    });

    var yellowIcon = L.icon({
        iconUrl: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|FFFF00&chf=a,s,ee00FFFF',

        iconSize:     [21, 34] // size of the icon
    });

    var purpleIcon = L.icon({
        iconUrl: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|FF00FF&chf=a,s,ee00FFFF',

        iconSize:     [21, 34] // size of the icon
    });

    //extended marker with additional attributes
    annotationMarker = L.Marker.extend({
       options: {
          mark_id: '0',
          mark_color: '0'
       }
    });

    /*var myMarker1 = new annotationMarker([48.713875, 2.169905], {
    title: 'Marker',
    icon: iconD95F02,
    mark_id: '1',
    mark_color: '#D95F02'
    });
    myMarker1.addTo(mymap);
    $(myMarker1._icon).addClass('annotation');
    markers.push(myMarker1);

    var myMarker2 = new annotationMarker([48.713475, 2.171420], {
    title: 'Marker',
    icon: yellowIcon,
    mark_id: '2',
    mark_color: '#ffff00'
    });
    myMarker2.addTo(mymap);
    $(myMarker2._icon).addClass('annotation');
    markers.push(myMarker2);

    var myMarker3 = new annotationMarker([48.714598, 2.170569], {
    title: 'Marker',
    icon: icon7570B3,
    mark_id: '3',
    mark_color: '#7570B3'
    });
    myMarker3.addTo(mymap);
    $(myMarker3._icon).addClass('annotation');
    markers.push(myMarker3);*/

    var updateId = true;
    var randomId;

    function generateId(){
      if(updateId){
        randomId = Math.floor(Math.random() * (500 - 1) + 1);
        updateId = false;
      }
      return randomId;
    }

    function getAnnColor(){
      if(indexColor == colors.length){
        indexColor = 0;
      }
      return colors[indexColor];
    }


    function updateForNextAnn() {
      updateId = true;
      indexColor++;
      var newId = generateId();

      polygonDrawer.setOptions({shapeOptions: {color: colors[indexColor]}});

      newMarker= new annotationMarker([1,1], {
      title: 'Marker',
      icon: colorIcons[indexColor],
      mark_id: newId,
      mark_color: colors[indexColor]
      });
      newMarker.addTo(mymap);
      $(newMarker._icon).addClass('annotation');

      mymap.removeEventListener('click', addMarker);


      //reset initial menu
      if (buttonScribble.classList.contains("pressed")) {
        buttonScribble.classList.remove("pressed");
      }
      if (buttonPin.classList.contains("pressed")) {
        buttonPin.classList.remove("pressed");
      }
      mymap.dragging.enable();
      contextualMenu.style.display = 'none';
    }

    /*============== Contextual Menu on pin ==============*/
    var contextualMenu = document.getElementById('contextualmenu');
    var menuClosed = document.getElementById("menu_button");
    var menuOpen = document.getElementById("menu_open");

    //Function to check whether a point is inside a rectangle
    function isInside(pos, rect){
        return pos.x > rect.x && pos.x < rect.x+rect.width && pos.y < rect.y+rect.height && pos.y > rect.y
    }

    function isInsideMobile(pos, rect){
        return pos.clientX > rect.x && pos.clientX < rect.x+rect.width && pos.clientY < rect.y+rect.height && pos.clientY > rect.y
    }

    function onMouseMove(event) {
      mousePosition = event.pageY;

      if (!isInside(event, contextualMenu.getBoundingClientRect())) {
        moveAt(event.pageX, event.pageY);
      }

    }

    function onMouseMoveWithPosition(position) {
      mousePosition = position.y;

      if (!isInside(position, contextualMenu.getBoundingClientRect())) {
        moveAt(position.x, position.y);
      }

    }

    function moveAt(pageX, pageY) {
      contextualMenu.style.display = 'block';
      contextualMenu.style.left = pageX - contextualMenu.offsetWidth / 2 + 'px';
      contextualMenu.style.top = pageY+65 - contextualMenu.offsetHeight / 2 + 'px';
    }

    /* //click for adding new markers (to be enabled opening menu)
    newMarkerGroup = new L.LayerGroup();
    mymap.on('click', addMarker);*/
    var newId = generateId();
    var newMarker= new annotationMarker([1,1], {
    title: 'Marker',
    icon: colorIcons[indexColor],
    mark_id: newId,
    mark_color: colors[indexColor]
    });
    newMarker.addTo(mymap);
    $(newMarker._icon).addClass('annotation');

    function addMarker(e){

        // Add marker to map at click location; add popup window
        if (!isInside(e.layerPoint, contextualMenu.getBoundingClientRect()) && !isInside(e.layerPoint, menuClosed.getBoundingClientRect()) && !isInside(e.layerPoint, menuOpen.getBoundingClientRect())) {
          newMarker.setLatLng(e.latlng);
          onMouseMove(e.originalEvent);
        }
        //onTouchend(e);

    }


    /*============== Note Canvas ==============*/
    var noteCanvas;
    var noteCanvasContainer;
    var contextNote;
    var noteSketcher;
    var noteCanvasX;
    var noteCanvasText;
    var noteCanvasScribble;
    var noteCanvasConfirm;

    var minSide = Math.min( window.innerWidth, window.innerHeight );
    var minSidePerc = ((90*minSide)/100).toFixed(0);

    function createNoteCanvas(color)
    {
      // Create a blank div where we are going to put the canvas into.
       noteCanvasContainer = document.createElement('div');
      // Add the div into the document
       document.body.appendChild(noteCanvasContainer);
       noteCanvasContainer.style.position="fixed";
       // centering the container in the screen
       noteCanvasContainer.style.left="50%";
       noteCanvasContainer.style.top="50%";
       noteCanvasContainer.style.width=minSidePerc+"px";
       noteCanvasContainer.style.height=minSidePerc+"px";
       var negativeHalfSide = -minSidePerc/2;
       noteCanvasContainer.style.marginLeft=negativeHalfSide+"px";
       noteCanvasContainer.style.marginTop=negativeHalfSide+"px";

       // Set to high index so that this is always above everything else
       // (might need to be increased if you have other element at higher index)
       noteCanvasContainer.style.zIndex="2000";

       // Now we create the canvas
       noteCanvas = document.createElement('canvas');
       noteCanvas.style.width = noteCanvasContainer.scrollWidth+"px";
       noteCanvas.style.height = noteCanvasContainer.scrollHeight+"px";
       // You must set this otherwise the canvas will be streethed to fit the container
       noteCanvas.width=noteCanvasContainer.scrollWidth;
       noteCanvas.height=noteCanvasContainer.scrollHeight;
       noteCanvas.style.overflow = 'visible';
       noteCanvas.style.position = 'absolute';
       noteCanvas.style.border = '2px solid black';

       contextNote=noteCanvas.getContext('2d');
       contextNote.fillStyle = color;
       contextNote.strokeStyle = 'black';
       contextNote.fillRect(0,0, noteCanvas.width, noteCanvas.height);

       // Add int into the container
       noteCanvasContainer.appendChild(noteCanvas);

       noteCanvasX = document.createElement('a');
       noteCanvasX.classList.add("canvas-x");
       noteCanvasContainer.appendChild(noteCanvasX);

       noteCanvasText = document.createElement('a');
       noteCanvasText.classList.add("canvas-text");
       noteCanvasContainer.appendChild(noteCanvasText);

       noteCanvasScribble = document.createElement('a');
       noteCanvasScribble.classList.add("canvas-scribble");
       noteCanvasContainer.appendChild(noteCanvasScribble);

       noteCanvasConfirm = document.createElement('a');
       noteCanvasConfirm.classList.add("canvas-confirm");
       noteCanvasContainer.appendChild(noteCanvasConfirm);

    }

    //document.getElementById("draw").addEventListener("click", createAndStop, false);
    var drawButton = document.getElementById("draw");
    drawButton.onclick = function() {
     clickOnDrawButton();
    }
    //drawButton.addEventListener("click", drawButton);

    function clickOnDrawButton(){

      createNoteCanvas('rgba(220,220,220,1)');
      noteSketcher  = new Sketchable(noteCanvas, {
        graphics: {
          lineWidth: 5,
          firstPointSize: 0,
          strokeStyle: 'black'
        }});

        noteSketcher.config({ interactive: true });
        noteCanvasScribble.classList.add("pressed");

      contextualMenu.style.display = 'none';

      noteCanvasX.addEventListener('click', function(evt) {
        noteCanvas.remove();
        noteCanvas = null;
        noteCanvasContainer.remove();
        noteCanvasContainer = null;

      });

      function textOnNote(e){
        if (hasInput) return;
        addInput(e.clientX, e.clientY);
      }

      function textOnNoteMobile(e){
        if (hasInput) return;
        addInput(e.changedTouches[0].clientX, e.changedTouches[0].clientY);
      }

      noteCanvasText.onclick = function() {

        this.classList.add("pressed");

        if (noteCanvasScribble.classList.contains("pressed")) {
          noteCanvasScribble.classList.remove("pressed");
        }

        //noteCanvas.addEventListener('click', textOnNote);
        //noteCanvas.addEventListener('touchstart', textOnNoteMobile);

        addFixedInput();

        noteSketcher.config({ interactive: false });
      }

      noteCanvasScribble.onclick = function(){

        this.classList.add("pressed");

        if (noteCanvasText.classList.contains("pressed")) {
          noteCanvasText.classList.remove("pressed");
        }

        noteSketcher.config({ interactive: true });
        //noteCanvas.onclick = '';
        noteCanvas.removeEventListener('click', textOnNote);
        noteCanvas.removeEventListener('touchstart', textOnNoteMobile);

      }

      noteCanvasConfirm.onclick = function () {
        drawTextOnNote();
        var base64NoteCanvas = getBase64ImageFromCanvas(noteCanvas);
        //document.getElementById("title1").innerHTML = base64NoteCanvas;

        var newIdCanvasNote = generateId();
        var newColor = getAnnColor();

        //get mouse position, TODO separate from highlight and scribble
        //document.getElementById("title").innerHTML = mousePosition;

        let imageAnnotation = {
         "id": newIdCanvasNote,
         "color": newColor,
         "position": mousePosition,
         "img": base64NoteCanvas
        }
        //send single annotations
        socket.emit('img annotation', imageAnnotation);

        let canvasAnnotationToAdd = {
         "id": newIdCanvasNote,
         "color": newColor,
         "position": mousePosition
        }

        //add it to array of textAnnotations
        //textAnnotations.push(canvasAnnotationToAdd);
        //add it to array of textAnnotations
        var checkIfDrawing = drawingLayers.find(c => c.options.id === newId);

        if(checkIfDrawing == undefined){
          markers.push(newMarker);
        }

        if(noteCanvas!=null){
          noteCanvas.remove();
          noteCanvas = null;
          noteCanvasContainer.remove();
          noteCanvasContainer = null;
        }
      }

    }

    /*============== TEXT on CANVAS NOTE ==============*/


    var font = '40px sans-serif';
    var hasInput = false;
    var leftNoteCanvasCont;
    var topNoteCanvasCont;

    //Function to dynamically add an input box:
    function addInput(x, y) {

        var input = document.createElement('input');

        var pixelsLeft = document.body.clientWidth*(50/100);
        var pixelsTop = document.body.clientHeight*(50/100);
        var marginLeft = parseInt(noteCanvasContainer.style.marginLeft, 10);
        var marginTop = parseInt(noteCanvasContainer.style.marginTop, 10);

        leftNoteCanvasCont = pixelsLeft + marginLeft;
        topNoteCanvasCont = pixelsTop + marginTop;

        input.type = 'text';
        input.style.position = 'absolute';
        input.style.left = (x - 4 - leftNoteCanvasCont) + 'px';
        input.style.top = (y - 4 - topNoteCanvasCont) + 'px';
        input.style.fontsize = '30px';

        input.onkeydown = handleEnter;

        noteCanvasContainer.appendChild(input);

        input.focus();

        hasInput = true;
    }


    var inputSpan;

    //Function to dynamically add an input box:
    function addFixedInput() {

      inputSpan = document.createElement('span');
      inputSpan.contentEditable = "true";
      noteCanvasContainer.appendChild(inputSpan);

      inputSpan.classList.add("textarea_map");
      inputSpan.style.position = 'absolute';
      //inputSpan.style.left = (30 - 4 - leftNoteCanvasCont) + 'px';
      //inputSpan.style.top = (30 - 4 - topNoteCanvasCont) + 'px';
      inputSpan.style.fontsize = '40px';
      inputSpan.style.left = '20px';
      inputSpan.style.top = '80px';
      inputSpan.style.width = '90%';


      var pixelsLeft = document.body.clientWidth*(50/100);
      var pixelsTop = document.body.clientHeight*(50/100);
      var marginLeft = parseInt(noteCanvasContainer.style.marginLeft, 10);
      var marginTop = parseInt(noteCanvasContainer.style.marginTop, 10);

      leftNoteCanvasCont = pixelsLeft + marginLeft;
      topNoteCanvasCont = pixelsTop + marginTop;



      inputSpan.onkeydown = handleEnter;

      //noteCanvasContainer.appendChild(inputSpan);

      inputSpan.focus();

      hasInput = true;
    }

    //Key handler for input box:
    function handleEnter(e) {
        var keyCode = e.keyCode;
        if (keyCode === 13) {
            drawTextOnNote();
            hasInput = false;
        }
    }

    function drawTextOnNote() {
      if(noteCanvasContainer.contains(inputSpan)){
        var percentsTextArea = parseInt(inputSpan.style.width);
        var parentWidthNoteCont = parseInt(noteCanvasContainer.style.width);
        var pixelsTextArea = parentWidthNoteCont*(percentsTextArea/100);

        drawText(inputSpan.innerHTML, parseInt(inputSpan.style.left, 10), parseInt(inputSpan.style.top, 10),pixelsTextArea);
        noteCanvasContainer.removeChild(inputSpan);
      }
    }

    function wrapText(context, text, x, y, maxWidth, lineHeight) {
      var words = text.split(' ');
      var line = '';

      for(var n = 0; n < words.length; n++) {
        var testLine = line + words[n] + ' ';
        var metrics = context.measureText(testLine);
        var testWidth = metrics.width;
        if (testWidth > maxWidth && n > 0) {
          context.fillText(line, x, y);
          line = words[n] + ' ';
          y += lineHeight;
        }
        else {
          line = testLine;
        }
      }
      context.fillText(line, x, y);
    }

    //Draw the text onto canvas:
    function drawText(txt, x, y, width) {
        var rect = noteCanvas.getBoundingClientRect();
        contextNote.textBaseline = 'top';
        contextNote.textAlign = 'left';
        contextNote.font = font;
        contextNote.fillStyle = 'black';
        //contextNote.fillText(txt, x-rect.left, y-rect.top);
        //contextNote.fillText(txt, x+4, y+4);
        wrapText(contextNote,txt,x+4,y+4,width,45);
    }


  /*============== SEND NOTE CANVAS ==============*/


  function getBase64ImageFromCanvas(canvas) {
    var dataURL = canvas.toDataURL("image/png");
    return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
  }

  socket.on('canvasAnnotationOK', function(confirmation) {
    //delete canvas
    noteCanvas.remove();
    noteCanvas = null;
    noteCanvasContainer.remove();
    noteCanvasContainer = null;
    updateForNextAnn();
    // there should be a new signal that includes resetting the view TODO
     //socket.emit('ann color', annotations);
     searchInAnnotations();

   });

   /*============== SEND IMAGE NOTE ==============*/

   function getBase64Image(img) {
     var canvas = document.createElement("canvas");
     canvas.width = img.width;
     canvas.height = img.height;
     var ctx = canvas.getContext("2d");
     ctx.drawImage(img, 0, 0);
     var dataURL = canvas.toDataURL("image/png");
     return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
   }

   var imageInputElement = document.getElementById("file-upload");
   imageInputElement.onchange = function(event) {
     event.stopImmediatePropagation();
      //get file from input
      var fileImage = imageInputElement.files[0];
      //put it in an image element
      var imageNote = document.createElement('img');
      imageNote.src = URL.createObjectURL(fileImage);
      //messages.appendChild(imageNote);
      //convert it to a base64
      var base64ImageNote;
      imageNote.onload = function () {
      base64ImageNote = getBase64Image(imageNote);
      var newId = generateId();
      var newColor = getAnnColor();

      //get mouse position, TODO separate from highlight and scribble
      //document.getElementById("title").innerHTML = mousePosition;

      let imageAnnotation = {
       "id": newId,
       "color": newColor,
       "position": mousePosition,
       "img": base64ImageNote
      }
      //send single annotations
      socket.emit('img annotation', imageAnnotation);

      let imageAnnotation1 = {
       "id": newId,
       "color": newColor,
       "position": mousePosition
      }

      //add it to array of textAnnotations
      var checkIfDrawing = drawingLayers.find(c => c.options.id === newId);

      if(checkIfDrawing == undefined){
        markers.push(newMarker);
      }

      //socket.emit('ann color', textAnnotations);
      contextualMenu.style.display = 'none';

      };
   }

   socket.on('annotationOK', function(confirmation) {
     if(noteCanvas!=null){
       noteCanvas.remove();
       noteCanvas = null;
       noteCanvasContainer.remove();
       noteCanvasContainer = null;
     }
     updateForNextAnn();

     searchInAnnotations();
    //socket.emit('ann color', annotations);
    });

    /*============== SEND Hololens NOTE ==============*/

    var headsetButton = document.getElementById("headset");
    let holoAnnotation;
    headsetButton.onclick = function () {

     var newId = generateId();
     var newColor = getAnnColor();

     holoAnnotation = {
     "id": newId,
     "color": newColor,
     "position": mousePosition
     }

     socket.emit('holo annotation', holoAnnotation);

     //add it to array of textAnnotations
     var checkIfDrawing = drawingLayers.find(c => c.options.id === newId);

     if(checkIfDrawing == undefined){
       markers.push(newMarker);
     }

     contextualMenu.style.display = 'none';

    };


    socket.on('HololensAnnotationOK', function(confirmation) {
      //textAnnotations.push(holoAnnotation);
      updateForNextAnn();
      //socket.emit('ann color', textAnnotations);
      //socket.emit('relayout', "relayout");

      searchInAnnotations();
     });

    /*var drawMode = true;
    //var myDrawing = L.polyline([]).addTo(mymap);;

    //some button that toggles on drawing, for now just manually doing it in console for testing.

    mymap.on('click', function()
    {
      if (drawMode)
      {
        myDrawing = L.polyline([]).addTo(mymap);
      }
    });

    mymap.on('mousemove', function(e)
    {
      if (drawMode)
      {
        myDrawing.addLatLng(e.latlng);
      }
    });*/

    var drawer = new L.FreeHandShapes({
			polygon : {
				color : '#009933',
				fillColor : '#66ff66',
				weight:3,
				smoothFactor: 1
			},
			polyline : {
			    color: '#66ff66',
			    smoothFactor: 0
			}
		});

    // Define you draw handler somewhere where you click handler can access it. N.B. pass any draw options into the handler
    var polygonDrawer = new L.Draw.Polygon(mymap);

    polygonDrawer.setOptions({shapeOptions: {color: colors[indexColor]}});

    // Assumming you have a Leaflet map accessible
    mymap.on('draw:created', function (e) {
        var type = e.layerType,
            layer = e.layer;
        if(type == "polygon"){
          var newId = generateId();
          layer.options.id = newId;
          //indexColor++;
          //polygonDrawer.setOptions({shapeOptions: {color: colors[indexColor]}});
          //var point = map.latLngToLayerPoint(latLng);
          drawingLayers.push(layer);

          //move contextual menu under first point of the drawing
          var pointposition = mymap.latLngToContainerPoint(layer._latlngs[0][0]);
          onMouseMoveWithPosition(pointposition);

        }
        // Do whatever you want with the layer.
        // e.type will be the type of layer that has been draw (polyline, marker, polygon, rectangle, circle)
        // E.g. add it to the map
        layer.addTo(mymap);
    });



    //drawer.addTo(mymap);
    // enable drawing
    //drawer.setMode('add');
    var buttonXtop = document.getElementById("topmenu-x");
    buttonXtop.onclick = function () {
      if (buttonScribble.classList.contains("pressed")) {
        buttonScribble.classList.remove("pressed");
      }
      if (buttonPin.classList.contains("pressed")) {
        buttonPin.classList.remove("pressed");
      }
      mymap.dragging.enable();
      contextualMenu.style.display = 'none';
      mymap.removeEventListener('click', addMarker);

    };

    var buttonPin = document.getElementById("pin");
    buttonPin.onclick = function () {
      buttonPin.classList.add("pressed");

      if (buttonScribble.classList.contains("pressed")) {
        buttonScribble.classList.remove("pressed");
      }
      mymap.dragging.disable();

      polygonDrawer.disable();


      newMarkerGroup = new L.LayerGroup();
      mymap.addEventListener('click', addMarker);
      //mymap.on('click', addMarker);
    };

    var buttonScribble = document.getElementById("scribble");
    buttonScribble.onclick = function () {
      buttonScribble.classList.add("pressed");

      if (buttonPin.classList.contains("pressed")) {
        buttonPin.classList.remove("pressed");
      }

      mymap.dragging.disable();

      polygonDrawer.enable();
      mymap.removeEventListener('click', addMarker);


    };

/*============== ON SCROLL ==============*/
var number;
var str;
var color;
let annotations = [];
let tempAnnotations = [];
var delta = 80;

var annotationsJSON;

function compare(array1, array2) {
  if (array1.length != array2.length) {
    return false;
  }

  array1 = array1.slice();
  //might be unnecessary if I'm sure that I'm adding those already sorted
  array1.sort((a, b) => parseFloat(a.position) - parseFloat(b.position));

  array2 = array2.slice();
  //might be unnecessary if I'm sure that I'm adding those already sorted
  array2.sort((a, b) => parseFloat(a.position) - parseFloat(b.position));

  for (var i = 0; i < array1.length; i++) {
    if (array1[i].id != array2[i].id) {
      return false;
    } else if (array1[i].position < (array2[i].position-delta) || array1[i].position > (array2[i].position+delta)){ //this will become: check if the position is different of a delta
      return false;
    }
  }
  return true;
}

function searchInAnnotations() {
  //reset variables for new count
  number = 0;
  str = '';
  color = '';
  tempAnnotations = [];

  /*if(drawingLayers.length > 0){
    var test = mymap.latLngToContainerPoint(drawingLayers[0]._latlngs[0][0]);
    document.getElementById("title").innerHTML = test.y;
  }*/
  //document.getElementById("title").innerHTML = JSON.stringify(tempAnnotations);
  //document.getElementById("title1").innerHTML = str;

    //all the markers in the markers array
    Array.from(drawingLayers).forEach((element, index) => {
          if(mymap.getBounds().contains(element._latlngs[0][0])){
            number += 1;
            //if it's rgb, convert it to hex
            //color = rgb2hex(element.style.backgroundColor);
            //tempAnnotations.push(color);
            var pointposition = mymap.latLngToContainerPoint(element._latlngs[0][0]);
            var position = pointposition.y;
            var color = element.options.color;
            var id = element.options.id.toString();
            //str = str + ' ' + color; //up
            str = str + ' ' + id;
            tempAnnotations.push({id: id, color: color, position: position});
            //document.getElementById("title").innerHTML = JSON.stringify(tempAnnotations);
            //document.getElementById("title1").innerHTML = str;
          }
      });

      //all the markers in the markers array
      Array.from(markers).forEach((element, index) => {
            if(isElementInViewport(element._icon)){
              number += 1;
              //if it's rgb, convert it to hex
              //color = rgb2hex(element.style.backgroundColor);
              //tempAnnotations.push(color);
              var y = $(element._icon).offset().top - $(window).scrollTop();
              var position = Math.round(y * 100) / 100;
              var color = element.options.mark_color;
              var id = element.options.mark_id.toString();
              //str = str + ' ' + color; //up
              str = str + ' ' + id;
              tempAnnotations.push({id: element.options.mark_id, color: element.options.mark_color, position: position});
              //document.getElementById("title").innerHTML = JSON.stringify(annotations);

              //document.getElementById("title1").innerHTML = str;
            }
        });

        tempAnnotations.sort((a, b) => parseFloat(a.position) - parseFloat(b.position));


    //document.getElementById("title").innerHTML = JSON.stringify(annotations);

    if(!compare(annotations, tempAnnotations)){
      annotations = tempAnnotations.slice();
      //document.getElementById("title").innerHTML = JSON.stringify(annotations);
      //annotationsJSON = JSON.stringify(annotations);
      //add a if check okay signal
      socket.emit('ann color', annotations);
    }

  }

function rgb2hex(rgb) {
    if (/^#[0-9A-F]{6}$/i.test(rgb)) return rgb;

    rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
    function hex(x) {
        return ("0" + parseInt(x).toString(16)).slice(-2);
    }
    return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
}

function isElementInViewport(el) {
    var rect = el.getBoundingClientRect();

    return rect.bottom > 0 &&
        rect.right > 0 &&
        rect.left < (window.innerWidth || document.documentElement.clientWidth) /* or $(window).width() */ &&
        rect.top < (window.innerHeight || document.documentElement.clientHeight) /* or $(window).height() */;
}

searchInAnnotations();

//important TODO maybe send the update only when it's stable (so if it doesn't change for half a second?)
window.onscroll = function () {

  searchInAnnotations();
}

window.onresize = function () {
  searchInAnnotations();
}

mymap.on('zoom', function() {
  searchInAnnotations();
});

mymap.on('move', function() {
  searchInAnnotations();
});
