/*============== SOCKET.IO ==============*/

var socket = io();
 var okay = 1;
 /*var form = document.getElementById('form');
 var input = document.getElementById('input');

 form.addEventListener('submit', function(e) {
   e.preventDefault();
   if (input.value) {
     socket.emit('chat message', input.value);
     input.value = '';
   }
 });*/

   socket.on('okay', function() {
      var okay = 1;
    });

    socket.on('reloadWebpage', function() {
      window.location.reload();
    });

    socket.on('loadTextPage', function() {
      window.location = "https://mobile-headset.herokuapp.com/text_insert";
    });

    socket.on('loadMapPage', function() {
      window.location = "https://mobile-headset.herokuapp.com/map_insert";
    });

    socket.on('loadVideoPage', function() {
      window.location = "https://mobile-headset.herokuapp.com/video_insert";
    });

    socket.on('loadConsumeTextPage', function() {
      window.location = "https://mobile-headset.herokuapp.com/text_consume";
    });

    socket.on('loadConsumeMapPage', function() {
      window.location = "https://mobile-headset.herokuapp.com/map_consume";
    });

    socket.on('loadConsumeVideoPage', function() {
      window.location = "https://mobile-headset.herokuapp.com/video_consume";
    });


/*============== Menu ==============*/
    $( "#menu_open" ).hide();

    $( "#menu_button" ).click(function() {
      var $this=$(this);
      $this.hide();
      $( "#menu_open" ).show('fast', 'linear', function() {
           //callback function after animation finished
      });
    });

    $( "#menu_open" ).click(function() {
      var $this=$(this);
      $this.hide();
      $( "#menu_button" ).show('fast', 'linear', function() {
           //callback function after animation finished
      });
    });


    /*============== Overlay Canvas ==============*/
    var myCanvas;
    var canvasContainer;
    var sketcher;
    var body = document.body;
    let videoCanvases = [];


    function createOverlayCanvas(color)
    {
      // Create a blank div where we are going to put the canvas into.
       canvasContainer = document.createElement('div');
      // Add the div into the document
       document.getElementById('my-video').appendChild(canvasContainer);
       canvasContainer.classList.add("video-canvas");
       var newId = generateId();
       canvasContainer.id = newId;
       canvasContainer.setAttribute("startInterval", myPlayer.currentTime());
       canvasContainer.setAttribute("intervalTime", 1);
       canvasContainer.style.position="absolute";
       // it will have the dimensions of the video frame
       canvasContainer.style.left="0px";
       canvasContainer.style.top="0px";
       canvasContainer.style.width=canvasContainer.parentElement.clientWidth+"px";
       canvasContainer.style.height=canvasContainer.parentElement.clientHeight-30+"px";
       // Set to high index so that this is always above everything else
       // (might need to be increased if you have other element at higher index)
       canvasContainer.style.zIndex="1000";
       videoCanvasContainer = document.getElementById('col-video');

       // Now we create the canvas
       myCanvas = document.createElement('canvas');
       myCanvas.style.width = canvasContainer.scrollWidth+"px";
       myCanvas.style.height = canvasContainer.scrollHeight+"px";
       // You must set this otherwise the canvas will be streethed to fit the container
       myCanvas.width=canvasContainer.scrollWidth;
       myCanvas.height=canvasContainer.scrollHeight;
       myCanvas.style.overflow = 'visible';
       myCanvas.style.position = 'absolute';

       var context=myCanvas.getContext('2d');
       context.fillStyle = color;
       context.fillRect(0,0, myCanvas.width, myCanvas.height);

       // Add int into the container
       canvasContainer.appendChild(myCanvas);

       videoCanvases.push(canvasContainer);

       startInterval = myPlayer.currentTime();
       intervalTime = 1;

    }

    function onMouseMove(event) {
       moveAt(event.pageX, event.pageY);
    }

    var menuClosed = document.getElementById("menu_button");
    var menuOpen = document.getElementById("menu_open");

    function onTouchend(event) {
      if (!isInsideMobile(event.changedTouches[0], contextualMenu.getBoundingClientRect()) && !isInsideMobile(event.changedTouches[0], menuClosed.getBoundingClientRect()) && !isInsideMobile(event.changedTouches[0], menuOpen.getBoundingClientRect())) {

        moveAt(event.changedTouches[0].pageX, event.changedTouches[0].pageY);
      }
    }


    function moveAt(pageX, pageY) {
      contextualMenu.style.left = pageX - contextualMenu.offsetWidth / 2 + 'px';
      contextualMenu.style.top = pageY - contextualMenu.offsetHeight / 2 + 'px';
      contextualMenu.style.display = 'block';
    }



/*============== Video Handler ==============*/
    var myPlayer = videojs('my-video');
    var currentTime;

    /*var myVideo = document.getElementById('my-video');
    myVideo.onloadedmetadata = function() {
      var duration = myVideo.duration();
      alert(duration);
      socket.emit('videoduration', duration);
    };*/

    let videoAnnotations = [];

/*    let ann8 = {
     "id": "8",
     "time": 69,
     "interval": 3,
     "color": "#80FBAB",
     "active": 0
    }
    videoAnnotations.push(ann8);

    let ann9 = {
     "id": "9",
     "time": 75,
     "interval": 15,
     "color": "#FF8DD9",
     "active": 0
    }
    videoAnnotations.push(ann9);*/

    window.addEventListener("load", function(){
      if (myPlayer.readyState() < 1) {
          // wait for loadedmetdata event
          myPlayer.on("loadedmetadata", onLoadedMetadata);
      }
      else {
          // metadata already loaded
          onLoadedMetadata();
      }

      function onLoadedMetadata() {
          var duration = myPlayer.duration();
          //alert(duration);
          socket.emit('videoduration', duration);
          socket.emit('setup annotations', videoAnnotations);
          //$('#duration').html("Duration: " + myPlayer.duration());
      }
    });


/*============== ON SCROLL ==============*/
var number;
var str;
var color;
let annotations = [];
let tempAnnotations = [];
var delta = 80;
var prevIndex;
var minAnnSize = 5;
let navigator = {
 "id": "1000",
 "index": "1000",
 "prev": "1000",
 "next": "1000"
}

var annotationsJSON;

function changeBackground(color) {
   document.body.style.background = color;
}

function compare(array1, array2) {
  if (array1.length != array2.length) {
    return false;
  }

  array1 = array1.slice();
  //might be unnecessary if I'm sure that I'm adding those already sorted
  array1.sort((a, b) => parseFloat(a.time) - parseFloat(b.time));

  array2 = array2.slice();
  //might be unnecessary if I'm sure that I'm adding those already sorted
  array2.sort((a, b) => parseFloat(a.time) - parseFloat(b.time));

  for (var i = 0; i < array1.length; i++) {
    if (array1[i].id != array2[i].id) {
      return false;
    } else if (array1[i].active != array2[i].active) {
      return false;
    }
  }
  return true;
}

function checkAnnOnScreen(caseAnn){
  /*
  Array.from(videoAnnotations).forEach((ann, index) => {
    if(index == 0 && myPlayer.currentTime() < ann.time){
      changeBackground('white');
      break;
    }
    if(ann.time <= myPlayer.currentTime() &&  myPlayer.currentTime() <= (ann.time + ann.interval)){
      changeBackground(ann.color);
      break;
    }
    if(myPlayer.currentTime() > ann.time){
      changeBackground('white');
      break;
    }
    if(index == (videoAnnotations.length-1) && myPlayer.currentTime() > (ann.time + ann.interval)){
      changeBackground('white');
      break;
    }
  });*/
  /*var prevActiveAnn = {
   "id": "0",
   "time": 0,
   "interval": 0,
   "color": "#000000",
   "active": 0
 };*/
//var prevActiveAnnId = 0;
//var prevActiveAnn;

  for (const [index, ann] of videoAnnotations.entries()) {
    if(index == 0 && myPlayer.currentTime() < ann.time){
      sendAnnGroup(index, caseAnn);
      changeBackground('white');
      videoAnnotations.forEach(element => element.active = 0);
      /*if(prevActiveAnnId != 0){
        prevActiveAnn = videoAnnotations.filter(obj => {
          return obj.id === prevActiveAnnId
        })
        prevActiveAnn.active = 0;
      }
      prevActiveAnnId = 0;*/
    } else if(ann.time <= myPlayer.currentTime() &&  myPlayer.currentTime() <= (ann.time + ann.interval)){
      var time = ann.time + ann.interval;
      /*if(prevActiveAnnId != 0){
        prevActiveAnn = videoAnnotations.filter(obj => {
          return obj.id === prevActiveAnnId
        })
        prevActiveAnn.active = 0;
      }*/
      videoAnnotations.forEach(element => element.active = 0);

      ann.active = 1;
      //prevActiveAnnId = ann.id;
      //document.getElementById("title").innerHTML = "current index: " + index + " & current id: " + ann.id;
      sendAnnGroup(index, caseAnn);
      //document.getElementById("title").innerHTML =  myPlayer.currentTime() + ' < ' + time;
      changeBackground(ann.color);
      break;
    } else if((videoAnnotations[index+1])!= null && myPlayer.currentTime() > (ann.time + ann.interval) && myPlayer.currentTime() < videoAnnotations[index+1].time){
      //document.getElementById("title").innerHTML = "previous index: " + index;
      videoAnnotations.forEach(element => element.active = 0);

      sendAnnGroup(index, caseAnn);
      changeBackground('white');
    } else if(index == (videoAnnotations.length-1) && myPlayer.currentTime() > (ann.time + ann.interval)){
      videoAnnotations.forEach(element => element.active = 0);

      sendAnnGroup(index, caseAnn);
      changeBackground('white');
    }
  }

}

function sendAnnGroup(index, caseAnn){
  prevAnnotations = [];
  //document.getElementById("title1").innerHTML = "index: " + index + " length: " +  (videoAnnotations.length);
  if (videoAnnotations.length < minAnnSize){
    /*if (prevIndex != null){
      videoAnnotations[prevIndex].active = 0;
      videoAnnotations[index].active = 1;
      prevIndex = index;
    }*/
    tempAnnotations = videoAnnotations;
  } else {
    if (index < 2){
      tempAnnotations = videoAnnotations.slice(0, minAnnSize);
      /*we don't need all this because it's not set as active
      if(index == 0){
        tempAnnotations = videoAnnotations.slice(0, minAnnSize-1);
      } else {
        for (i = (index - 0); i > 0; i--) {
          if(i == 0){
            tempAnnotations = videoAnnotations.slice(0, minAnnSize-1);
          }
        }
      }*/

    } else if (index > (videoAnnotations.length - 3) ){
      tempAnnotations = videoAnnotations.slice(videoAnnotations.length-minAnnSize, videoAnnotations.length);
        /*for (i = (index + 1); i < index + 3; i ++){
          if (videoAnnotations[i] == null){
            tempAnnotations = videoAnnotations.slice(i-minAnnSize, i);
            break;
          }
        }*/
    } else {
      tempAnnotations = videoAnnotations.slice(index-2, index+3);
    }
  }

  if(!compare(prevAnnotations, tempAnnotations)){
    prevAnnotations = tempAnnotations.slice();
    //document.getElementById("title").innerHTML = JSON.stringify(tempAnnotations);
    if(caseAnn == 1){
      socket.emit('ann color', tempAnnotations);
    } else if (caseAnn == 2){
      socket.emit('videorelayout', tempAnnotations);
    }
  }

}

var colors = ["#1B9E77", "#D95F02", "#7570B3", "#E7298A", "#66A61E", "#E6AB02", "#A6761D"];
var indexColor = 0;

var updateId = true;
var randomId;

function generateId(){
  if(updateId){
    randomId = Math.floor(Math.random() * (500 - 1) + 1);
    updateId = false;
  }
  return randomId;
}

function getAnnColor(){
  if(indexColor == colors.length){
    indexColor = 0;
  }
  return colors[indexColor];
}

function updateForNextAnn() {
  contextualMenu.style.display = 'none';

  if (buttonScribble.classList.contains("pressed")) {
    buttonScribble.classList.remove("pressed");
  }
  if (buttonVideoframe.classList.contains("pressed")) {
    buttonVideoframe.classList.remove("pressed");
  }

  updateId = true;
  indexColor++;
  var newId = generateId();
}

/*============== Top Menu Controller ==============*/

var buttonXtop = document.getElementById("button-x");
buttonXtop.onclick = function () {
  if (buttonScribble.classList.contains("pressed")) {
    buttonScribble.classList.remove("pressed");
  }
  if (buttonVideoframe.classList.contains("pressed")) {
    buttonVideoframe.classList.remove("pressed");
  }

  //remove event listeners
  contextualMenu.style.display = 'none';
  document.removeEventListener('mouseup', onMouseMove);
  document.removeEventListener('touchend', onTouchend);

};

var buttonVideoframe = document.getElementById("videoframe");
buttonVideoframe.onclick = function () {
  buttonVideoframe.classList.add("pressed");

  if (buttonScribble.classList.contains("pressed")) {
    buttonScribble.classList.remove("pressed");
  }

  intervalDiv.style.display = 'block';

};

var buttonScribble = document.getElementById("scribble");
buttonScribble.onclick = function () {
  buttonScribble.classList.add("pressed");

  if (buttonVideoframe.classList.contains("pressed")) {
    buttonVideoframe.classList.remove("pressed");
  }

  createOverlayCanvas('rgba(192,192,192,0.1)');
  var contextualMenu = document.getElementById('contextualmenu');
  var annColor = getAnnColor();
  (function() {
    sketcher = new Sketchable(myCanvas, {
      graphics: {
        lineWidth: 8,
        firstPointSize: 0,
        strokeStyle: annColor
      }});
    document.addEventListener('mouseup', onMouseMove);
    document.addEventListener('touchend', onTouchend);
  })();

};

/*============== Interval menu ==============*/
var intervalDiv = document.createElement('div');
intervalDiv.classList.add("intervaldiv");
// Add the div into the video div
document.getElementById('my-video').appendChild(intervalDiv);

var intervalText = document.createElement('div');
intervalText.classList.add("intervaltext");

intervalText.innerHTML = "Select Start point <br> for Annotation Interval";
// Add the text into the interval div
intervalDiv.appendChild(intervalText);


var intervalButtonStart = document.createElement("BUTTON");   // Create a <button> element
intervalButtonStart.classList.add("intervalbutton");
intervalButtonStart.innerHTML = "Confirm";                   // Insert text
intervalDiv.appendChild(intervalButtonStart);

var intervalButtonEnd = document.createElement("BUTTON");   // Create a <button> element
intervalButtonEnd.classList.add("intervalbutton");
intervalButtonEnd.innerHTML = "Confirm";                   // Insert text
intervalDiv.appendChild(intervalButtonEnd);
intervalButtonEnd.style.display = 'none';

intervalDiv.style.display = 'none';

var startInterval;
var intervalTime;

intervalButtonStart.onclick = function () {
  intervalText.innerHTML = "Select End point <br> for Annotation Interval";
  intervalDiv.style.backgroundColor = '#FF9205';
  intervalButtonEnd.style.display = 'block';
  intervalButtonStart.style.display = 'none';
  startInterval = myPlayer.currentTime();
};

intervalButtonEnd.onclick = function () {
  if(myPlayer.currentTime() > startInterval){
    intervalDiv.style.backgroundColor = '#FEF857';
    intervalDiv.style.display = 'none';
    intervalText.innerHTML = "Select Start point <br> for Annotation Interval";
    intervalButtonEnd.style.display = 'none';
    intervalButtonStart.style.display = 'block';
    intervalTime = Math.round((myPlayer.currentTime() - startInterval) * 100) / 100;

    contextualMenu.style.display = 'block';
    contextualMenu.style.right="20px";
    contextualMenu.style.top="20px";
  }

};


/*============== Contextual Menu on pin ==============*/
var contextualMenu = document.getElementById('contextualmenu');
document.getElementById('my-video').appendChild(contextualMenu);

//Function to check whether a point is inside a rectangle
function isInside(pos, rect){
    return pos.x > rect.x && pos.x < rect.x+rect.width && pos.y < rect.y+rect.height && pos.y > rect.y
}

function isInsideMobile(pos, rect){
    return pos.clientX > rect.x && pos.clientX < rect.x+rect.width && pos.clientY < rect.y+rect.height && pos.clientY > rect.y
}

function onMouseMove(event) {
  mousePosition = event.pageY;

  if (!isInside(event, contextualMenu.getBoundingClientRect())) {
    moveAt(event.pageX, event.pageY);
  }

}

function moveAt(pageX, pageY) {
  contextualMenu.style.left = pageX - contextualMenu.offsetWidth / 2 + 'px';
  contextualMenu.style.top = pageY+65 - contextualMenu.offsetHeight / 2 + 'px';
  contextualMenu.style.display = 'block';
}


  /*============== Note Canvas ==============*/
  var noteCanvas;
  var noteCanvasContainer;
  var contextNote;
  var noteSketcher;
  var noteCanvasX;
  var noteCanvasText;
  var noteCanvasScribble;
  var noteCanvasConfirm;

  var minSide = Math.min( window.innerWidth, window.innerHeight );
  var minSidePerc = ((70*minSide)/100).toFixed(0);

  function createNoteCanvas(color)
  {
    // Create a blank div where we are going to put the canvas into.
     noteCanvasContainer = document.createElement('div');
    // Add the div into the document
     document.body.appendChild(noteCanvasContainer);
     noteCanvasContainer.style.position="fixed";
     // centering the container in the screen
     noteCanvasContainer.style.left="50%";
     noteCanvasContainer.style.top="50%";
     noteCanvasContainer.style.width=minSidePerc+"px";
     noteCanvasContainer.style.height=minSidePerc+"px";
     var negativeHalfSide = -minSidePerc/2;
     noteCanvasContainer.style.marginLeft=negativeHalfSide+"px";
     noteCanvasContainer.style.marginTop=negativeHalfSide+"px";

     // Set to high index so that this is always above everything else
     // (might need to be increased if you have other element at higher index)
     noteCanvasContainer.style.zIndex="2000";

     // Now we create the canvas
     noteCanvas = document.createElement('canvas');
     noteCanvas.style.width = noteCanvasContainer.scrollWidth+"px";
     noteCanvas.style.height = noteCanvasContainer.scrollHeight+"px";
     // You must set this otherwise the canvas will be streethed to fit the container
     noteCanvas.width=noteCanvasContainer.scrollWidth;
     noteCanvas.height=noteCanvasContainer.scrollHeight;
     noteCanvas.style.overflow = 'visible';
     noteCanvas.style.position = 'absolute';

     contextNote=noteCanvas.getContext('2d');
     contextNote.fillStyle = color;
     contextNote.strokeStyle = 'black';
     contextNote.fillRect(0,0, noteCanvas.width, noteCanvas.height);

     // Add int into the container
     noteCanvasContainer.appendChild(noteCanvas);

     noteCanvasX = document.createElement('a');
     noteCanvasX.classList.add("canvas-x");
     noteCanvasContainer.appendChild(noteCanvasX);

     noteCanvasText = document.createElement('a');
     noteCanvasText.classList.add("canvas-text");
     noteCanvasContainer.appendChild(noteCanvasText);

     noteCanvasScribble = document.createElement('a');
     noteCanvasScribble.classList.add("canvas-scribble");
     noteCanvasContainer.appendChild(noteCanvasScribble);

     noteCanvasConfirm = document.createElement('a');
     noteCanvasConfirm.classList.add("canvas-confirm");
     noteCanvasContainer.appendChild(noteCanvasConfirm);

  }

  //document.getElementById("draw").addEventListener("click", createAndStop, false);
  var drawButton = document.getElementById("draw");
  drawButton.onclick = function() {
   clickOnDrawButton();
  }
  //drawButton.addEventListener("click", drawButton);

  function clickOnDrawButton(){

    createNoteCanvas('rgba(220,220,220,1)');
    noteSketcher  = new Sketchable(noteCanvas, {
      graphics: {
        lineWidth: 5,
        firstPointSize: 0,
        strokeStyle: 'black'
      }});

      noteSketcher.config({ interactive: true });
      noteCanvasScribble.classList.add("pressed");

    contextualMenu.style.display = 'none';

    noteCanvasX.addEventListener('click', function(evt) {
      noteCanvas.remove();
      noteCanvas = null;
      noteCanvasContainer.remove();
      noteCanvasContainer = null;

    });

    function textOnNote(e){
      if (hasInput) return;
      addInput(e.clientX, e.clientY);
    }

    function textOnNoteMobile(e){
      if (hasInput) return;
      addInput(e.changedTouches[0].clientX, e.changedTouches[0].clientY);
    }

    noteCanvasText.onclick = function() {

      this.classList.add("pressed");

      if (noteCanvasScribble.classList.contains("pressed")) {
        noteCanvasScribble.classList.remove("pressed");
      }

      //noteCanvas.addEventListener('click', textOnNote);
      //noteCanvas.addEventListener('touchstart', textOnNoteMobile);

      addFixedInput();

      noteSketcher.config({ interactive: false });
    }

    noteCanvasScribble.onclick = function(){

      this.classList.add("pressed");

      if (noteCanvasText.classList.contains("pressed")) {
        noteCanvasText.classList.remove("pressed");
      }

      noteSketcher.config({ interactive: true });
      //noteCanvas.onclick = '';
      noteCanvas.removeEventListener('click', textOnNote);
      noteCanvas.removeEventListener('touchstart', textOnNoteMobile);

    }

    noteCanvasConfirm.onclick = function () {
      drawTextOnNote();

      var base64NoteCanvas = getBase64ImageFromCanvas(noteCanvas);
      //document.getElementById("title1").innerHTML = base64NoteCanvas;

      var newIdCanvasNote = generateId();
      var newColor = getAnnColor();

      //get mouse position, TODO separate from highlight and scribble
      //document.getElementById("title").innerHTML = mousePosition;

      let imageAnnotation = {
       "id": newIdCanvasNote,
       "color": newColor,
       "time": startInterval,
       "interval": intervalTime,
       "img": base64NoteCanvas
      }
      //send single annotations
      socket.emit('img annotation', imageAnnotation);


      let canvasAnnotationToAdd = {
       "id": newIdCanvasNote.toString(),
       "time": startInterval,
       "interval": intervalTime,
       "color": newColor,
       "active": 0
      }

      //add it to array of textAnnotations
      //textAnnotations.push(canvasAnnotationToAdd);
      //markers.push(newMarker);
      videoAnnotations.push(canvasAnnotationToAdd);
      videoAnnotations.sort((a, b) => parseFloat(a.time) - parseFloat(b.time));

      if(noteCanvas!=null){
        noteCanvas.remove();
        noteCanvas = null;
        noteCanvasContainer.remove();
        noteCanvasContainer = null;
      }

    }

  }

  /*============== TEXT on CANVAS NOTE ==============*/


  var font = '80px sans-serif';
  var hasInput = false;

  //Function to dynamically add an input box:
  function addInput(x, y) {

      var input = document.createElement('input');

      input.type = 'text';
      input.style.position = 'fixed';
      input.style.left = (x - 4) + 'px';
      input.style.top = (y - 4) + 'px';
      input.style.fontsize = '24px';

      input.onkeydown = handleEnter;

      noteCanvasContainer.appendChild(input);

      input.focus();

      hasInput = true;
  }

  var inputSpan;

  //Function to dynamically add an input box:
  function addFixedInput() {

    inputSpan = document.createElement('span');
    inputSpan.contentEditable = "true";
    noteCanvasContainer.appendChild(inputSpan);

    inputSpan.classList.add("textarea");
    inputSpan.style.position = 'absolute';
    //inputSpan.style.left = (30 - 4 - leftNoteCanvasCont) + 'px';
    //inputSpan.style.top = (30 - 4 - topNoteCanvasCont) + 'px';
    inputSpan.style.fontsize = '80px';
    inputSpan.style.left = '20px';
    inputSpan.style.top = '80px';
    inputSpan.style.width = '90%';


    var pixelsLeft = document.body.clientWidth*(50/100);
    var pixelsTop = document.body.clientHeight*(50/100);
    var marginLeft = parseInt(noteCanvasContainer.style.marginLeft, 10);
    var marginTop = parseInt(noteCanvasContainer.style.marginTop, 10);

    leftNoteCanvasCont = pixelsLeft + marginLeft;
    topNoteCanvasCont = pixelsTop + marginTop;

    inputSpan.classList.add("textarea");
    inputSpan.style.position = 'absolute';
    //inputSpan.style.left = (30 - 4 - leftNoteCanvasCont) + 'px';
    //inputSpan.style.top = (30 - 4 - topNoteCanvasCont) + 'px';
    inputSpan.style.fontsize = '80px';

    inputSpan.onkeydown = handleEnter;

    //noteCanvasContainer.appendChild(inputSpan);

    inputSpan.focus();

    hasInput = true;
  }

  //Key handler for input box:
  function handleEnter(e) {
      var keyCode = e.keyCode;
      if (keyCode === 13) {
          drawTextOnNote();
          hasInput = false;
      }
  }

  function drawTextOnNote() {
    if(noteCanvasContainer.contains(inputSpan)){
      var percentsTextArea = parseInt(inputSpan.style.width);
      var parentWidthNoteCont = parseInt(noteCanvasContainer.style.width);
      var pixelsTextArea = parentWidthNoteCont*(percentsTextArea/100);

      drawText(inputSpan.innerHTML, parseInt(inputSpan.style.left, 10), parseInt(inputSpan.style.top, 10),pixelsTextArea);
      noteCanvasContainer.removeChild(inputSpan);
    }
  }

  function wrapText(context, text, x, y, maxWidth, lineHeight) {
    var words = text.split(' ');
    var line = '';

    for(var n = 0; n < words.length; n++) {
      var testLine = line + words[n] + ' ';
      var metrics = context.measureText(testLine);
      var testWidth = metrics.width;
      if (testWidth > maxWidth && n > 0) {
        context.fillText(line, x, y);
        line = words[n] + ' ';
        y += lineHeight;
      }
      else {
        line = testLine;
      }
    }
    context.fillText(line, x, y);
  }

  //Draw the text onto canvas:
  function drawText(txt, x, y, width) {
      var rect = noteCanvas.getBoundingClientRect();
      contextNote.textBaseline = 'top';
      contextNote.textAlign = 'left';
      contextNote.font = font;
      contextNote.fillStyle = 'black';
      //contextNote.fillText(txt, x-rect.left, y-rect.top);
      //contextNote.fillText(txt, x+4, y+4);
      wrapText(contextNote,txt,x+4,y+4,width,90);
  }


  /*============== SEND NOTE CANVAS ==============*/


  function getBase64ImageFromCanvas(canvas) {
    var dataURL = canvas.toDataURL("image/png");
    return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
  }

  socket.on('canvasAnnotationOK', function(confirmation) {
    //delete canvas
    noteCanvas.remove();
    noteCanvas = null;
    noteCanvasContainer.remove();
    noteCanvasContainer = null;
    updateForNextAnn();
    contextualMenu.style.display = 'none';
    // there should be a new signal that includes resetting the view TODO
     //socket.emit('ann color', videoAnnotations);
     checkAnnOnScreen(2);
   });

   /*============== SEND IMAGE NOTE ==============*/

   function getBase64Image(img) {
     var canvas = document.createElement("canvas");
     canvas.width = img.width;
     canvas.height = img.height;
     var ctx = canvas.getContext("2d");
     ctx.drawImage(img, 0, 0);
     var dataURL = canvas.toDataURL("image/png");
     return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
   }

   var imageInputElement = document.getElementById("file-upload");
   imageInputElement.onchange = function(event) {
     event.stopImmediatePropagation();
      //get file from input
      var fileImage = imageInputElement.files[0];
      //put it in an image element
      var imageNote = document.createElement('img');
      imageNote.src = URL.createObjectURL(fileImage);
      //messages.appendChild(imageNote);
      //convert it to a base64
      var base64ImageNote;
      imageNote.onload = function () {
      base64ImageNote = getBase64Image(imageNote);
      var newId = generateId();
      var newColor = getAnnColor();

      //get mouse position, TODO separate from highlight and scribble
      //document.getElementById("title").innerHTML = mousePosition;

      let imageAnnotation = {
       "id": newId,
       "color": newColor,
       "time": startInterval,
       "interval": intervalTime,
       "img": base64ImageNote
      }
      //send single annotations
      socket.emit('img annotation', imageAnnotation);

      let canvasAnnotationToAdd = {
       "id": newId,
       "time": startInterval,
       "interval": intervalTime,
       "color": newColor,
       "active": 0
      }

      //add it to array of textAnnotations
      //textAnnotations.push(canvasAnnotationToAdd);
      //markers.push(newMarker);
      videoAnnotations.push(canvasAnnotationToAdd);
      videoAnnotations.sort((a, b) => parseFloat(a.time) - parseFloat(b.time));
      contextualMenu.style.display = 'none';

      };
   }

   socket.on('annotationOK', function(confirmation) {
     if(noteCanvas!=null){
       noteCanvas.remove();
       noteCanvas = null;
       noteCanvasContainer.remove();
       noteCanvasContainer = null;
     }
     contextualMenu.style.display = 'none';

     updateForNextAnn();

     //TODO save highlighted text if its text
      //socket.emit('ann color', videoAnnotations);
      checkAnnOnScreen(2);

    });

    /*============== SEND Hololens NOTE ==============*/

    var headsetButton = document.getElementById("headset");
    let holoAnnotation;
    headsetButton.onclick = function () {
     contextualMenu.style.display = 'none';

     var newId = generateId();
     var newColor = getAnnColor();


     holoAnnotation = {
     "id": newId,
     "time": startInterval,
     "interval": intervalTime,
     "color": newColor,
     "active": 0
    }
    videoAnnotations.push(holoAnnotation);
    videoAnnotations.sort((a, b) => parseFloat(a.time) - parseFloat(b.time));
    socket.emit('holo annotation', holoAnnotation);

    };


    socket.on('HololensAnnotationOK', function(confirmation) {
      //textAnnotations.push(holoAnnotation);
      updateForNextAnn();
      //socket.emit('ann color', textAnnotations);
      //socket.emit('relayout', "relayout");

      //socket.emit('ann color', videoAnnotations);
      checkAnnOnScreen(2);

     });

/*
function searchInAnnotations() {
  //reset variables for new count
  number = 0;
  str = '';
  color = '';
  tempAnnotations = [];
  document.getElementById("title").innerHTML = JSON.stringify(tempAnnotations);
  document.getElementById("title1").innerHTML = str;

    //all the markers in the markers array
    Array.from(videoAnnotations).forEach((element, index) => {
      element
        if(isElementInViewport(element)){
          number += 1;
          //if it's rgb, convert it to hex
          //color = rgb2hex(element.style.backgroundColor);
          //tempAnnotations.push(color);
          var y = $(element._icon).offset().top - $(window).scrollTop();
          var position = Math.round(y * 100) / 100;
          var color = element.options.mark_color;
          var id = element.options.mark_id;
          //str = str + ' ' + color; //up
          str = str + ' ' + id;
          tempAnnotations.push({id: element.options.mark_id, color: element.options.mark_color, position: position});
          document.getElementById("title").innerHTML = JSON.stringify(tempAnnotations);
          //document.getElementById("title1").innerHTML = str;
        }
    });

    if(!compare(annotations, tempAnnotations)){
      annotations = tempAnnotations.slice();
      //annotationsJSON = JSON.stringify(annotations);
      //add a if check okay signal
      socket.emit('ann color', annotations);
    }

  }

function rgb2hex(rgb) {
    if (/^#[0-9A-F]{6}$/i.test(rgb)) return rgb;

    rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
    function hex(x) {
        return ("0" + parseInt(x).toString(16)).slice(-2);
    }
    return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
}

function isElementInViewport(el) {
    var rect = el.getBoundingClientRect();

    return rect.bottom > 0 &&
           rect.right > 0 &&
           rect.left < (window.innerWidth || document.documentElement.clientWidth) /* or $(window).width()
           rect.top < (window.innerHeight || document.documentElement.clientHeight) /* or $(window).height() ;
}

searchInAnnotations();

//important TODO maybe send the update only when it's stable (so if it doesn't change for half a second?)
window.onscroll = function () {

  searchInAnnotations();
}

window.onresize = function () {
  searchInAnnotations();
}*/

//for a jump on the timeline
myPlayer.on('seeked', function () {
    //document.getElementById("title1").innerHTML = myPlayer.currentTime();
    checkAnnOnScreen(2);
});

//only send it when it's playing
$(function(){
 setInterval(updateTimeline, 300);
});

function updateTimeline() {
  var current = myPlayer.currentTime();
  socket.emit('videocurrenttime', current);

  if(!myPlayer.paused()){
    checkAnnOnScreen(1);
    //document.getElementById("title").innerHTML = myPlayer.currentTime();
  }
}

//every time the time is updated, no separate cases
myPlayer.on('timeupdate', function () {
  videoCanvases.forEach((item, i) => {
    var currentTime = myPlayer.currentTime();
    var startInterval = parseFloat(item.getAttribute("startInterval"));
    var interval = parseFloat(item.getAttribute("intervalTime"));
    var endInterval = startInterval + interval;
    if(startInterval <= currentTime &&  currentTime <= endInterval){
      item.style.display = 'block';
    } else {
      item.style.display = 'none';
    }
  });

  //checkAnnOnScreen(1);
    //document.getElementById("title").innerHTML = myPlayer.currentTime();
});

myPlayer.on('loadedmetadata', function () {
  //document.getElementById("title").innerHTML = myPlayer.currentTime();
});
