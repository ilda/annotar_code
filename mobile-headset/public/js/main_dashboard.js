/*============== SOCKET.IO ==============*/

var socket = io();

var confirmation;

 var buttonText = document.getElementById("buttontext");
 buttonText.onclick = function () {

  socket.emit('loadTextScene', confirmation);
};

var buttonMap = document.getElementById("buttonmap");
buttonMap.onclick = function () {

  socket.emit('loadMapScene', confirmation);
};

var buttonVideo = document.getElementById("buttonvideo");
buttonVideo.onclick = function () {

  socket.emit('loadVideoScene', confirmation);
};

var buttonTextConsume = document.getElementById("buttontextconsume");
buttonTextConsume.onclick = function () {

 socket.emit('loadConsumeTextScene', confirmation);
};

var buttonMapConsume = document.getElementById("buttonmapconsume");
buttonMapConsume.onclick = function () {

 socket.emit('loadConsumeMapScene', confirmation);
};

var buttonVideoConsume = document.getElementById("buttonvideoconsume");
buttonVideoConsume.onclick = function () {
 socket.emit('loadConsumeVideoScene', confirmation);
};


var buttonReload = document.getElementById("buttonreload");
buttonReload.onclick = function () {
  socket.emit('reloadWebpage', confirmation);
};

var buttonPageText = document.getElementById("buttontextpage");
buttonPageText.onclick = function () {
  socket.emit('loadTextPage', confirmation);
};

var buttonPageMap = document.getElementById("buttonmappage");
buttonPageMap.onclick = function () {
  socket.emit('loadMapPage', confirmation);
};

var buttonPageVideo = document.getElementById("buttonvideopage");
buttonPageVideo.onclick = function () {
  socket.emit('loadVideoPage', confirmation);
};

var buttonPageConsumeText = document.getElementById("buttonconsumetextpage");
buttonPageConsumeText.onclick = function () {
  socket.emit('loadConsumeTextPage', confirmation);
};

var buttonPageConsumeMap = document.getElementById("buttonconsumemappage");
buttonPageConsumeMap.onclick = function () {
  socket.emit('loadConsumeMapPage', confirmation);
};

var buttonPageConsumeVideo = document.getElementById("buttonconsumevideopage");
buttonPageConsumeVideo.onclick = function () {
  socket.emit('loadConsumeVideoPage', confirmation);
};
