/*============== SOCKET.IO ==============*/

var socket = io();
 var okay = 1;
 /*var form = document.getElementById('form');
 var input = document.getElementById('input');

 form.addEventListener('submit', function(e) {
   e.preventDefault();
   if (input.value) {
     socket.emit('chat message', input.value);
     input.value = '';
   }
 });*/

 socket.on('chat message', function(msg) {
    var item = document.createElement('li');
    item.textContent = msg;
    messages.appendChild(item);
    window.scrollTo(0, document.body.scrollHeight);
  });

  socket.on('ann color', function(annotations) {
     var item1 = document.createElement('li');
     item1.textContent = JSON.stringify(annotations);
     messages.appendChild(item1);
     //window.scrollTo(0, document.body.scrollHeight);
   });

   socket.on('okay', function() {
      var okay = 1;
    });

/*============== Menu ==============*/
    $( "#menu_open" ).hide();

    $( "#menu_button" ).click(function() {
      var $this=$(this);
      $this.hide();
      $( "#menu_open" ).show('fast', 'linear', function() {
           //callback function after animation finished
      });
    });

    $( "#menu_open" ).click(function() {
      var $this=$(this);
      $this.hide();
      $( "#menu_button" ).show('fast', 'linear', function() {
           //callback function after animation finished
      });
    });

/*============== Map Handler ==============*/

    var mymap = L.map('mapid', { zoomControl: false }).setView([48.86, 2.35], 13);

    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        maxZoom: 18,
        id: 'mapbox/streets-v11',
        tileSize: 512,
        zoomOffset: -1,
        accessToken: 'pk.eyJ1IjoiZmRpZ2lvaWEiLCJhIjoiY2tvMnlrZW1qMTQ4MTJycGdreWxjeDdwOCJ9.ywsPzKb9eBo9-tNNqUvEfg'
    }).addTo(mymap);


    var greenIcon = L.icon({
        iconUrl: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|00FF00&chf=a,s,ee00FFFF',

        iconSize:     [21, 34] // size of the icon
    });

    var yellowIcon = L.icon({
        iconUrl: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|FFFF00&chf=a,s,ee00FFFF',

        iconSize:     [21, 34] // size of the icon
    });

    var purpleIcon = L.icon({
        iconUrl: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|FF00FF&chf=a,s,ee00FFFF',

        iconSize:     [21, 34] // size of the icon
    });

    var marker1 = L.marker([48.86, 2.35], {icon: greenIcon}).addTo(mymap);
    $(marker1._icon).addClass('annotation');

    var marker2 = L.marker([48.86, 2.36], {icon: yellowIcon}).addTo(mymap);
    $(marker2._icon).addClass('annotation');

    var marker3 = L.marker([48.865, 2.355], {icon: purpleIcon}).addTo(mymap);
    $(marker3._icon).addClass('annotation');

    /* click for adding new markers (to be enabled opening menu)
    newMarkerGroup = new L.LayerGroup();
    mymap.on('click', addMarker);

    function addMarker(e){
        // Add marker to map at click location; add popup window
        var newMarker = new L.marker(e.latlng).addTo(mymap);
    }*/


/*============== ON SCROLL ==============*/
var number;
var str;
var color;
let annotations = [];
let tempAnnotations = [];
var delta = 80;

var annotationsJSON;

function compare(array1, array2) {
  if (array1.length != array2.length) {
    return false;
  }

  array1 = array1.slice();
  //might be unnecessary if I'm sure that I'm adding those already sorted
  array1.sort((a, b) => parseFloat(a.position) - parseFloat(b.position));

  array2 = array2.slice();
  //might be unnecessary if I'm sure that I'm adding those already sorted
  array2.sort((a, b) => parseFloat(a.position) - parseFloat(b.position));

  for (var i = 0; i < array1.length; i++) {
    if (array1[i].id != array2[i].id) {
      return false;
    } else if (array1[i].position < (array2[i].position-delta) || array1[i].position > (array2[i].position+delta)){ //this will become: check if the position is different of a delta
      return false;
    }
  }
  return true;
}

function searchInAnnotations() {
  //reset variables for new count
  number = 0;
  str = '';
  color = '';
  tempAnnotations = [];
  document.getElementById("title").innerHTML = JSON.stringify(tempAnnotations);
  document.getElementById("title1").innerHTML = str;

  /*
  //for all the elements in the annotation class
  const elements = document.querySelectorAll('.annotation');
  Array.from(elements).forEach((element, index) => {
        if(isElementInViewport(element)){
          number += 1;
          //if it's rgb, convert it to hex
          color = rgb2hex(element.style.backgroundColor);
          //tempAnnotations.push(color);
          var y = $(element).offset().top - $(window).scrollTop();
          var position =  Math.round(y * 100) / 100;
          str = str + ' ' + position;
          tempAnnotations.push({id: element.id, color: color, position: position});
          document.getElementById("title").innerHTML = JSON.stringify(tempAnnotations);
          //document.getElementById("title1").innerHTML = str;
        }
    });*/

    if(!compare(annotations, tempAnnotations)){
      annotations = tempAnnotations.slice();
      //annotationsJSON = JSON.stringify(annotations);
      //add a if check okay signal
      socket.emit('ann color', annotations);
    }

    const elements = document.querySelectorAll('.leaflet-marker-icon.annotation');
    Array.from(elements).forEach((element, index) => {
          if(isElementInViewport(element)){
            number += 1;
            //if it's rgb, convert it to hex
            //color = rgb2hex(element.style.backgroundColor);
            //tempAnnotations.push(color);
            var y = $(element).offset().top - $(window).scrollTop();
            var position =  Math.round(y * 100) / 100;
            str = str + ' ' + position;
            tempAnnotations.push({id: element.options.mark_id, color: element.options.mark_id, position: position});
            document.getElementById("title").innerHTML = JSON.stringify(tempAnnotations);
            //document.getElementById("title1").innerHTML = str;
          }
      });

  }

function rgb2hex(rgb) {
    if (/^#[0-9A-F]{6}$/i.test(rgb)) return rgb;

    rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
    function hex(x) {
        return ("0" + parseInt(x).toString(16)).slice(-2);
    }
    return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
}

function isElementInViewport(el) {
    var rect = el.getBoundingClientRect();

    return rect.bottom > 0 &&
        rect.right > 0 &&
        rect.left < (window.innerWidth || document.documentElement.clientWidth) /* or $(window).width() */ &&
        rect.top < (window.innerHeight || document.documentElement.clientHeight) /* or $(window).height() */;
}

searchInAnnotations();

//important TODO maybe send the update only when it's stable (so if it doesn't change for half a second?)
window.onscroll = function () {

  searchInAnnotations();
}

window.onresize = function () {
  searchInAnnotations();
}

mymap.on('zoomend', function() {
  searchInAnnotations();
});

mymap.on('moveend', function() {
  searchInAnnotations();
});
