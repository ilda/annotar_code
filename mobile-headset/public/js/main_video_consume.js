/*============== SOCKET.IO ==============*/

var socket = io();
 var okay = 1;
 /*var form = document.getElementById('form');
 var input = document.getElementById('input');

 form.addEventListener('submit', function(e) {
   e.preventDefault();
   if (input.value) {
     socket.emit('chat message', input.value);
     input.value = '';
   }
 });*/

   socket.on('okay', function() {
      var okay = 1;
    });

    socket.on('reloadWebpage', function() {
      window.location.reload();
    });

    socket.on('loadTextPage', function() {
      window.location = "https://mobile-headset.herokuapp.com/text_insert";
    });

    socket.on('loadMapPage', function() {
      window.location = "https://mobile-headset.herokuapp.com/map_insert";
    });

    socket.on('loadVideoPage', function() {
      window.location = "https://mobile-headset.herokuapp.com/video_insert";
    });

    socket.on('loadConsumeTextPage', function() {
      window.location = "https://mobile-headset.herokuapp.com/text_consume";
    });

    socket.on('loadConsumeMapPage', function() {
      window.location = "https://mobile-headset.herokuapp.com/map_consume";
    });

    socket.on('loadConsumeVideoPage', function() {
      window.location = "https://mobile-headset.herokuapp.com/video_consume";
    });


/*============== Menu ==============*/
    $( "#menu_open" ).hide();

    $( "#menu_button" ).click(function() {
      var $this=$(this);
      $this.hide();
      $( "#menu_open" ).show('fast', 'linear', function() {
           //callback function after animation finished
      });
    });

    $( "#menu_open" ).click(function() {
      var $this=$(this);
      $this.hide();
      $( "#menu_button" ).show('fast', 'linear', function() {
           //callback function after animation finished
      });
    });


    /*============== Overlay Canvas ==============*/
    var myCanvas;
    var canvasContainer;
    var sketcher;
    var body = document.body;
    let videoCanvases = [];


    function createOverlayCanvas(color)
    {
      // Create a blank div where we are going to put the canvas into.
       canvasContainer = document.createElement('div');
      // Add the div into the document
       document.getElementById('my-video').appendChild(canvasContainer);
       canvasContainer.classList.add("video-canvas");
       var newId = generateId();
       canvasContainer.id = newId;
       canvasContainer.setAttribute("startInterval", myPlayer.currentTime());
       canvasContainer.setAttribute("intervalTime", 1);
       canvasContainer.style.position="absolute";
       // it will have the dimensions of the video frame
       canvasContainer.style.left="0px";
       canvasContainer.style.top="0px";
       canvasContainer.style.width=canvasContainer.parentElement.clientWidth+"px";
       canvasContainer.style.height=canvasContainer.parentElement.clientHeight-30+"px";
       // Set to high index so that this is always above everything else
       // (might need to be increased if you have other element at higher index)
       canvasContainer.style.zIndex="1000";
       videoCanvasContainer = document.getElementById('col-video');

       // Now we create the canvas
       myCanvas = document.createElement('canvas');
       myCanvas.style.width = canvasContainer.scrollWidth+"px";
       myCanvas.style.height = canvasContainer.scrollHeight+"px";
       // You must set this otherwise the canvas will be streethed to fit the container
       myCanvas.width=canvasContainer.scrollWidth;
       myCanvas.height=canvasContainer.scrollHeight;
       myCanvas.style.overflow = 'visible';
       myCanvas.style.position = 'absolute';

       var context=myCanvas.getContext('2d');
       context.fillStyle = color;
       context.fillRect(0,0, myCanvas.width, myCanvas.height);

       // Add int into the container
       canvasContainer.appendChild(myCanvas);

       videoCanvases.push(canvasContainer);

       startInterval = myPlayer.currentTime();
       intervalTime = 1;

    }

    function onMouseMove(event) {
       moveAt(event.pageX, event.pageY);
    }

    var menuClosed = document.getElementById("menu_button");
    var menuOpen = document.getElementById("menu_open");

    function onTouchend(event) {
      if (!isInsideMobile(event.changedTouches[0], contextualMenu.getBoundingClientRect()) && !isInsideMobile(event.changedTouches[0], menuClosed.getBoundingClientRect()) && !isInsideMobile(event.changedTouches[0], menuOpen.getBoundingClientRect())) {

        moveAt(event.changedTouches[0].pageX, event.changedTouches[0].pageY);
      }
    }


    function moveAt(pageX, pageY) {
      contextualMenu.style.left = pageX - contextualMenu.offsetWidth / 2 + 'px';
      contextualMenu.style.top = pageY - contextualMenu.offsetHeight / 2 + 'px';
      contextualMenu.style.display = 'block';
    }



/*============== Video Handler ==============*/
    var myPlayer = videojs('my-video');
    var currentTime;

    /*var myVideo = document.getElementById('my-video');
    myVideo.onloadedmetadata = function() {
      var duration = myVideo.duration();
      alert(duration);
      socket.emit('videoduration', duration);
    };*/

    let videoAnnotations = [];

/*    let ann8 = {
     "id": "8",
     "time": 69,
     "interval": 3,
     "color": "#80FBAB",
     "active": 0
    }
    videoAnnotations.push(ann8);

    let ann9 = {
     "id": "9",
     "time": 75,
     "interval": 15,
     "color": "#FF8DD9",
     "active": 0
    }
    videoAnnotations.push(ann9);*/

    window.addEventListener("load", function(){
      if (myPlayer.readyState() < 1) {
          // wait for loadedmetdata event
          myPlayer.on("loadedmetadata", onLoadedMetadata);
      }
      else {
          // metadata already loaded
          onLoadedMetadata();
      }

      function onLoadedMetadata() {
          var duration = myPlayer.duration();
          //alert(duration);
          socket.emit('videoduration', duration);
          socket.emit('setup annotations', videoAnnotations);
          //$('#duration').html("Duration: " + myPlayer.duration());
      }
    });


/*============== ON SCROLL ==============*/
var number;
var str;
var color;
let annotations = [];
let tempAnnotations = [];
var delta = 80;
var prevIndex;
var minAnnSize = 5;
let navigator = {
 "id": "1000",
 "index": "1000",
 "prev": "1000",
 "next": "1000"
}

var annotationsJSON;

function changeBackground(color) {
   document.body.style.background = color;
}

function compare(array1, array2) {
  if (array1.length != array2.length) {
    return false;
  }

  array1 = array1.slice();
  //might be unnecessary if I'm sure that I'm adding those already sorted
  array1.sort((a, b) => parseFloat(a.time) - parseFloat(b.time));

  array2 = array2.slice();
  //might be unnecessary if I'm sure that I'm adding those already sorted
  array2.sort((a, b) => parseFloat(a.time) - parseFloat(b.time));

  for (var i = 0; i < array1.length; i++) {
    if (array1[i].id != array2[i].id) {
      return false;
    } else if (array1[i].active != array2[i].active) {
      return false;
    }
  }
  return true;
}

function checkAnnOnScreen(caseAnn){
  /*
  Array.from(videoAnnotations).forEach((ann, index) => {
    if(index == 0 && myPlayer.currentTime() < ann.time){
      changeBackground('white');
      break;
    }
    if(ann.time <= myPlayer.currentTime() &&  myPlayer.currentTime() <= (ann.time + ann.interval)){
      changeBackground(ann.color);
      break;
    }
    if(myPlayer.currentTime() > ann.time){
      changeBackground('white');
      break;
    }
    if(index == (videoAnnotations.length-1) && myPlayer.currentTime() > (ann.time + ann.interval)){
      changeBackground('white');
      break;
    }
  });*/
  /*var prevActiveAnn = {
   "id": "0",
   "time": 0,
   "interval": 0,
   "color": "#000000",
   "active": 0
 };*/
//var prevActiveAnnId = 0;
//var prevActiveAnn;

  for (const [index, ann] of videoAnnotations.entries()) {
    if(index == 0 && myPlayer.currentTime() < ann.time){
      sendAnnGroup(index, caseAnn);
      changeBackground('white');
      videoAnnotations.forEach(element => element.active = 0);
      /*if(prevActiveAnnId != 0){
        prevActiveAnn = videoAnnotations.filter(obj => {
          return obj.id === prevActiveAnnId
        })
        prevActiveAnn.active = 0;
      }
      prevActiveAnnId = 0;*/
    } else if(ann.time <= myPlayer.currentTime() &&  myPlayer.currentTime() <= (ann.time + ann.interval)){
      var time = ann.time + ann.interval;
      /*if(prevActiveAnnId != 0){
        prevActiveAnn = videoAnnotations.filter(obj => {
          return obj.id === prevActiveAnnId
        })
        prevActiveAnn.active = 0;
      }*/
      videoAnnotations.forEach(element => element.active = 0);

      ann.active = 1;
      //prevActiveAnnId = ann.id;
      //document.getElementById("title").innerHTML = "current index: " + index + " & current id: " + ann.id;
      sendAnnGroup(index, caseAnn);
      //document.getElementById("title").innerHTML =  myPlayer.currentTime() + ' < ' + time;
      changeBackground(ann.color);
      break;
    } else if((videoAnnotations[index+1])!= null && myPlayer.currentTime() > (ann.time + ann.interval) && myPlayer.currentTime() < videoAnnotations[index+1].time){
      //document.getElementById("title").innerHTML = "previous index: " + index;
      videoAnnotations.forEach(element => element.active = 0);

      sendAnnGroup(index, caseAnn);
      changeBackground('white');
    } else if(index == (videoAnnotations.length-1) && myPlayer.currentTime() > (ann.time + ann.interval)){
      videoAnnotations.forEach(element => element.active = 0);

      sendAnnGroup(index, caseAnn);
      changeBackground('white');
    }
  }

}

function sendAnnGroup(index, caseAnn){
  prevAnnotations = [];
  //document.getElementById("title1").innerHTML = "index: " + index + " length: " +  (videoAnnotations.length);
  if (videoAnnotations.length < minAnnSize){
    /*if (prevIndex != null){
      videoAnnotations[prevIndex].active = 0;
      videoAnnotations[index].active = 1;
      prevIndex = index;
    }*/
    tempAnnotations = videoAnnotations;
  } else {
    if (index < 2){
      tempAnnotations = videoAnnotations.slice(0, minAnnSize);
      /*we don't need all this because it's not set as active
      if(index == 0){
        tempAnnotations = videoAnnotations.slice(0, minAnnSize-1);
      } else {
        for (i = (index - 0); i > 0; i--) {
          if(i == 0){
            tempAnnotations = videoAnnotations.slice(0, minAnnSize-1);
          }
        }
      }*/

    } else if (index > (videoAnnotations.length - 3) ){
      tempAnnotations = videoAnnotations.slice(videoAnnotations.length-minAnnSize, videoAnnotations.length);
        /*for (i = (index + 1); i < index + 3; i ++){
          if (videoAnnotations[i] == null){
            tempAnnotations = videoAnnotations.slice(i-minAnnSize, i);
            break;
          }
        }*/
    } else {
      tempAnnotations = videoAnnotations.slice(index-2, index+3);
    }
  }

  if(!compare(prevAnnotations, tempAnnotations)){
    prevAnnotations = tempAnnotations.slice();
    //document.getElementById("title").innerHTML = JSON.stringify(tempAnnotations);
    if(caseAnn == 1){
      socket.emit('ann color', tempAnnotations);
    } else if (caseAnn == 2){
      socket.emit('videorelayout', tempAnnotations);
    }
  }

}

var colors = ["#1B9E77", "#D95F02", "#7570B3", "#E7298A", "#66A61E", "#E6AB02", "#A6761D"];
var indexColor = 0;

var updateId = true;
var randomId;

function generateId(){
  if(updateId){
    randomId = Math.floor(Math.random() * (500 - 1) + 1);
    updateId = false;
  }
  return randomId;
}

function getAnnColor(){
  if(indexColor == colors.length){
    indexColor = 0;
  }
  return colors[indexColor];
}

function updateForNextAnn() {
  contextualMenu.style.display = 'none';

  if (buttonScribble.classList.contains("pressed")) {
    buttonScribble.classList.remove("pressed");
  }
  if (buttonVideoframe.classList.contains("pressed")) {
    buttonVideoframe.classList.remove("pressed");
  }

  updateId = true;
  indexColor++;
  var newId = generateId();
}

/*============== Top Menu Controller ==============*/

var buttonXtop = document.getElementById("button-x");
buttonXtop.onclick = function () {
  if (buttonScribble.classList.contains("pressed")) {
    buttonScribble.classList.remove("pressed");
  }
  if (buttonVideoframe.classList.contains("pressed")) {
    buttonVideoframe.classList.remove("pressed");
  }

  //remove event listeners
  contextualMenu.style.display = 'none';
  document.removeEventListener('mouseup', onMouseMove);
  document.removeEventListener('touchend', onTouchend);

};

var buttonVideoframe = document.getElementById("videoframe");
buttonVideoframe.onclick = function () {
  buttonVideoframe.classList.add("pressed");

  if (buttonScribble.classList.contains("pressed")) {
    buttonScribble.classList.remove("pressed");
  }

  intervalDiv.style.display = 'block';

};

var buttonScribble = document.getElementById("scribble");
buttonScribble.onclick = function () {
  buttonScribble.classList.add("pressed");

  if (buttonVideoframe.classList.contains("pressed")) {
    buttonVideoframe.classList.remove("pressed");
  }

  createOverlayCanvas('rgba(192,192,192,0.1)');
  var contextualMenu = document.getElementById('contextualmenu');
  var annColor = getAnnColor();
  (function() {
    sketcher = new Sketchable(myCanvas, {
      graphics: {
        lineWidth: 8,
        firstPointSize: 0,
        strokeStyle: annColor
      }});
    document.addEventListener('mouseup', onMouseMove);
    document.addEventListener('touchend', onTouchend);
  })();

};

/*============== Interval menu ==============*/
var intervalDiv = document.createElement('div');
intervalDiv.classList.add("intervaldiv");
// Add the div into the video div
document.getElementById('my-video').appendChild(intervalDiv);

var intervalText = document.createElement('div');
intervalText.classList.add("intervaltext");

intervalText.innerHTML = "Select Start point <br> for Annotation Interval";
// Add the text into the interval div
intervalDiv.appendChild(intervalText);


var intervalButtonStart = document.createElement("BUTTON");   // Create a <button> element
intervalButtonStart.classList.add("intervalbutton");
intervalButtonStart.innerHTML = "Confirm";                   // Insert text
intervalDiv.appendChild(intervalButtonStart);

var intervalButtonEnd = document.createElement("BUTTON");   // Create a <button> element
intervalButtonEnd.classList.add("intervalbutton");
intervalButtonEnd.innerHTML = "Confirm";                   // Insert text
intervalDiv.appendChild(intervalButtonEnd);
intervalButtonEnd.style.display = 'none';

intervalDiv.style.display = 'none';

var startInterval;
var intervalTime;

intervalButtonStart.onclick = function () {
  intervalText.innerHTML = "Select End point <br> for Annotation Interval";
  intervalDiv.style.backgroundColor = '#FF9205';
  intervalButtonEnd.style.display = 'block';
  intervalButtonStart.style.display = 'none';
  startInterval = myPlayer.currentTime();
};

intervalButtonEnd.onclick = function () {
  if(myPlayer.currentTime() > startInterval){
    intervalDiv.style.backgroundColor = '#FEF857';
    intervalDiv.style.display = 'none';
    intervalText.innerHTML = "Select Start point <br> for Annotation Interval";
    intervalButtonEnd.style.display = 'none';
    intervalButtonStart.style.display = 'block';
    intervalTime = Math.round((myPlayer.currentTime() - startInterval) * 100) / 100;

    contextualMenu.style.display = 'block';
    contextualMenu.style.right="20px";
    contextualMenu.style.top="20px";
  }

};


/*============== Contextual Menu on pin ==============*/
var contextualMenu = document.getElementById('contextualmenu');
document.getElementById('my-video').appendChild(contextualMenu);

//Function to check whether a point is inside a rectangle
function isInside(pos, rect){
    return pos.x > rect.x && pos.x < rect.x+rect.width && pos.y < rect.y+rect.height && pos.y > rect.y
}

function isInsideMobile(pos, rect){
    return pos.clientX > rect.x && pos.clientX < rect.x+rect.width && pos.clientY < rect.y+rect.height && pos.clientY > rect.y
}

function onMouseMove(event) {
  mousePosition = event.pageY;

  if (!isInside(event, contextualMenu.getBoundingClientRect())) {
    moveAt(event.pageX, event.pageY);
  }

}

function moveAt(pageX, pageY) {
  contextualMenu.style.left = pageX - contextualMenu.offsetWidth / 2 + 'px';
  contextualMenu.style.top = pageY+65 - contextualMenu.offsetHeight / 2 + 'px';
  contextualMenu.style.display = 'block';
}


  /*============== Note Canvas ==============*/
  var noteCanvas;
  var noteCanvasContainer;
  var contextNote;
  var noteSketcher;
  var noteCanvasX;
  var noteCanvasText;
  var noteCanvasScribble;
  var noteCanvasConfirm;

  var minSide = Math.min( window.innerWidth, window.innerHeight );
  var minSidePerc = ((70*minSide)/100).toFixed(0);

  function createNoteCanvas(color)
  {
    // Create a blank div where we are going to put the canvas into.
     noteCanvasContainer = document.createElement('div');
    // Add the div into the document
     document.body.appendChild(noteCanvasContainer);
     noteCanvasContainer.style.position="fixed";
     // centering the container in the screen
     noteCanvasContainer.style.left="50%";
     noteCanvasContainer.style.top="50%";
     noteCanvasContainer.style.width=minSidePerc+"px";
     noteCanvasContainer.style.height=minSidePerc+"px";
     var negativeHalfSide = -minSidePerc/2;
     noteCanvasContainer.style.marginLeft=negativeHalfSide+"px";
     noteCanvasContainer.style.marginTop=negativeHalfSide+"px";

     // Set to high index so that this is always above everything else
     // (might need to be increased if you have other element at higher index)
     noteCanvasContainer.style.zIndex="2000";

     // Now we create the canvas
     noteCanvas = document.createElement('canvas');
     noteCanvas.style.width = noteCanvasContainer.scrollWidth+"px";
     noteCanvas.style.height = noteCanvasContainer.scrollHeight+"px";
     // You must set this otherwise the canvas will be streethed to fit the container
     noteCanvas.width=noteCanvasContainer.scrollWidth;
     noteCanvas.height=noteCanvasContainer.scrollHeight;
     noteCanvas.style.overflow = 'visible';
     noteCanvas.style.position = 'absolute';

     contextNote=noteCanvas.getContext('2d');
     contextNote.fillStyle = color;
     contextNote.strokeStyle = 'black';
     contextNote.fillRect(0,0, noteCanvas.width, noteCanvas.height);

     // Add int into the container
     noteCanvasContainer.appendChild(noteCanvas);

     noteCanvasX = document.createElement('a');
     noteCanvasX.classList.add("canvas-x");
     noteCanvasContainer.appendChild(noteCanvasX);

     noteCanvasText = document.createElement('a');
     noteCanvasText.classList.add("canvas-text");
     noteCanvasContainer.appendChild(noteCanvasText);

     noteCanvasScribble = document.createElement('a');
     noteCanvasScribble.classList.add("canvas-scribble");
     noteCanvasContainer.appendChild(noteCanvasScribble);

     noteCanvasConfirm = document.createElement('a');
     noteCanvasConfirm.classList.add("canvas-confirm");
     noteCanvasContainer.appendChild(noteCanvasConfirm);

  }

  //document.getElementById("draw").addEventListener("click", createAndStop, false);
  var drawButton = document.getElementById("draw");
  drawButton.onclick = function() {
   clickOnDrawButton();
  }
  //drawButton.addEventListener("click", drawButton);

  function clickOnDrawButton(){

    createNoteCanvas('rgba(220,220,220,1)');
    noteSketcher  = new Sketchable(noteCanvas, {
      graphics: {
        lineWidth: 3,
        firstPointSize: 0,
        strokeStyle: 'black'
      }});

    noteSketcher.config({ interactive: true });
    noteCanvasScribble.classList.add("pressed");

    //remove event listeners
    contextualMenu.style.display = 'none';
    document.removeEventListener('mouseup', onMouseMove);
    document.removeEventListener('touchend', onTouchend);

    noteCanvasX.addEventListener('click', function(evt) {
      noteCanvas.remove();
      noteCanvas = null;
      noteCanvasContainer.remove();
      noteCanvasContainer = null;

    });

    function textOnNote(e){
      if (hasInput) return;
      addInput(e.clientX, e.clientY);
    }

    function textOnNoteMobile(e){
      if (hasInput) return;
      addInput(e.changedTouches[0].clientX, e.changedTouches[0].clientY);
    }

    noteCanvasText.onclick = function() {

      this.classList.add("pressed");

      if (noteCanvasScribble.classList.contains("pressed")) {
        noteCanvasScribble.classList.remove("pressed");
      }

      noteCanvas.addEventListener('click', textOnNote);
      noteCanvas.addEventListener('touchstart', textOnNoteMobile);


      noteSketcher.config({ interactive: false });
    }

    noteCanvasScribble.onclick = function(){

      this.classList.add("pressed");

      if (noteCanvasText.classList.contains("pressed")) {
        noteCanvasText.classList.remove("pressed");
      }

      noteSketcher.config({ interactive: true });
      //noteCanvas.onclick = '';
      noteCanvas.removeEventListener('click', textOnNote);
      noteCanvas.removeEventListener('touchstart', textOnNoteMobile);

    }

    noteCanvasConfirm.onclick = function () {
      var contents = sketcher.serializer.save();

      var base64NoteCanvas = getBase64ImageFromCanvas(noteCanvas);
      //document.getElementById("title1").innerHTML = base64NoteCanvas;

      var newIdCanvasNote = generateId();
      var newColor = getAnnColor();

      //get mouse position, TODO separate from highlight and scribble
      //document.getElementById("title").innerHTML = mousePosition;

      let imageAnnotation = {
       "id": newIdCanvasNote,
       "color": newColor,
       "time": startInterval,
       "interval": intervalTime,
       "img": base64NoteCanvas
      }
      //send single annotations
      socket.emit('img annotation', imageAnnotation);


      let canvasAnnotationToAdd = {
       "id": newIdCanvasNote.toString(),
       "time": startInterval,
       "interval": intervalTime,
       "color": newColor,
       "active": 0
      }

      //add it to array of textAnnotations
      //textAnnotations.push(canvasAnnotationToAdd);
      //markers.push(newMarker);
      videoAnnotations.push(canvasAnnotationToAdd);
      videoAnnotations.sort((a, b) => parseFloat(a.time) - parseFloat(b.time));

      if(noteCanvas!=null){
        noteCanvas.remove();
        noteCanvas = null;
        noteCanvasContainer.remove();
        noteCanvasContainer = null;
      }

    }

  }

  /*============== TEXT on CANVAS NOTE ==============*/


  var font = '80px sans-serif';
  var hasInput = false;

  //Function to dynamically add an input box:
  function addInput(x, y) {

      var input = document.createElement('input');

      input.type = 'text';
      input.style.position = 'fixed';
      input.style.left = (x - 4) + 'px';
      input.style.top = (y - 4) + 'px';
      input.style.fontsize = '24px';

      input.onkeydown = handleEnter;

      noteCanvasContainer.appendChild(input);

      input.focus();

      hasInput = true;
  }

  var inputSpan;

  //Function to dynamically add an input box:
  function addFixedInput() {

    inputSpan = document.createElement('span');
    inputSpan.contentEditable = "true";
    noteCanvasContainer.appendChild(inputSpan);

    inputSpan.classList.add("textarea");
    inputSpan.style.position = 'absolute';
    //inputSpan.style.left = (30 - 4 - leftNoteCanvasCont) + 'px';
    //inputSpan.style.top = (30 - 4 - topNoteCanvasCont) + 'px';
    inputSpan.style.fontsize = '80px';
    inputSpan.style.left = '20px';
    inputSpan.style.top = '80px';
    inputSpan.style.width = '90%';


    var pixelsLeft = document.body.clientWidth*(50/100);
    var pixelsTop = document.body.clientHeight*(50/100);
    var marginLeft = parseInt(noteCanvasContainer.style.marginLeft, 10);
    var marginTop = parseInt(noteCanvasContainer.style.marginTop, 10);

    leftNoteCanvasCont = pixelsLeft + marginLeft;
    topNoteCanvasCont = pixelsTop + marginTop;

    inputSpan.classList.add("textarea");
    inputSpan.style.position = 'absolute';
    //inputSpan.style.left = (30 - 4 - leftNoteCanvasCont) + 'px';
    //inputSpan.style.top = (30 - 4 - topNoteCanvasCont) + 'px';
    inputSpan.style.fontsize = '80px';

    inputSpan.onkeydown = handleEnter;

    //noteCanvasContainer.appendChild(inputSpan);

    inputSpan.focus();

    hasInput = true;
  }

  //Key handler for input box:
  function handleEnter(e) {
      var keyCode = e.keyCode;
      if (keyCode === 13) {
          drawTextOnNote();
          hasInput = false;
      }
  }

  function drawTextOnNote() {
    if(noteCanvasContainer.contains(inputSpan)){
      var percentsTextArea = parseInt(inputSpan.style.width);
      var parentWidthNoteCont = parseInt(noteCanvasContainer.style.width);
      var pixelsTextArea = parentWidthNoteCont*(percentsTextArea/100);

      drawText(inputSpan.innerHTML, parseInt(inputSpan.style.left, 10), parseInt(inputSpan.style.top, 10),pixelsTextArea);
      noteCanvasContainer.removeChild(inputSpan);
    }
  }

  function wrapText(context, text, x, y, maxWidth, lineHeight) {
    var words = text.split(' ');
    var line = '';

    for(var n = 0; n < words.length; n++) {
      var testLine = line + words[n] + ' ';
      var metrics = context.measureText(testLine);
      var testWidth = metrics.width;
      if (testWidth > maxWidth && n > 0) {
        context.fillText(line, x, y);
        line = words[n] + ' ';
        y += lineHeight;
      }
      else {
        line = testLine;
      }
    }
    context.fillText(line, x, y);
  }

  //Draw the text onto canvas:
  function drawText(txt, x, y, width) {
      var rect = noteCanvas.getBoundingClientRect();
      contextNote.textBaseline = 'top';
      contextNote.textAlign = 'left';
      contextNote.font = font;
      contextNote.fillStyle = 'black';
      //contextNote.fillText(txt, x-rect.left, y-rect.top);
      //contextNote.fillText(txt, x+4, y+4);
      wrapText(contextNote,txt,x+4,y+4,width,90);
  }


  /*============== SEND NOTE CANVAS ==============*/


  function getBase64ImageFromCanvas(canvas) {
    var dataURL = canvas.toDataURL("image/png");
    return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
  }

  socket.on('canvasAnnotationOK', function(confirmation) {
    //delete canvas
    noteCanvas.remove();
    noteCanvas = null;
    noteCanvasContainer.remove();
    noteCanvasContainer = null;
    updateForNextAnn();
    contextualMenu.style.display = 'none';
    // there should be a new signal that includes resetting the view TODO
     //socket.emit('ann color', videoAnnotations);
     checkAnnOnScreen(2);
   });

   /*============== SEND IMAGE NOTE ==============*/

   function getBase64Image(img) {
     var canvas = document.createElement("canvas");
     canvas.width = img.width;
     canvas.height = img.height;
     var ctx = canvas.getContext("2d");
     ctx.drawImage(img, 0, 0);
     var dataURL = canvas.toDataURL("image/png");
     return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
   }

   var imageInputElement = document.getElementById("file-upload");
   imageInputElement.onchange = function(event) {
     event.stopImmediatePropagation();
      //get file from input
      var fileImage = imageInputElement.files[0];
      //put it in an image element
      var imageNote = document.createElement('img');
      imageNote.src = URL.createObjectURL(fileImage);
      //messages.appendChild(imageNote);
      //convert it to a base64
      var base64ImageNote;
      imageNote.onload = function () {
      base64ImageNote = getBase64Image(imageNote);
      var newId = generateId();
      var newColor = getAnnColor();

      //get mouse position, TODO separate from highlight and scribble
      //document.getElementById("title").innerHTML = mousePosition;

      let imageAnnotation = {
       "id": newId,
       "color": newColor,
       "time": startInterval,
       "interval": intervalTime,
       "img": base64ImageNote
      }
      //send single annotations
      socket.emit('img annotation', imageAnnotation);

      let canvasAnnotationToAdd = {
       "id": newId,
       "time": startInterval,
       "interval": intervalTime,
       "color": newColor,
       "active": 0
      }

      //add it to array of textAnnotations
      //textAnnotations.push(canvasAnnotationToAdd);
      //markers.push(newMarker);
      videoAnnotations.push(canvasAnnotationToAdd);
      videoAnnotations.sort((a, b) => parseFloat(a.time) - parseFloat(b.time));
      contextualMenu.style.display = 'none';

      };
   }

   socket.on('annotationOK', function(confirmation) {
     if(noteCanvas!=null){
       noteCanvas.remove();
       noteCanvas = null;
       noteCanvasContainer.remove();
       noteCanvasContainer = null;
     }
     contextualMenu.style.display = 'none';

     updateForNextAnn();

     //TODO save highlighted text if its text
      //socket.emit('ann color', videoAnnotations);
      checkAnnOnScreen(2);

    });

    /*============== SEND Hololens NOTE ==============*/

    var headsetButton = document.getElementById("headset");
    let holoAnnotation;
    headsetButton.onclick = function () {
     contextualMenu.style.display = 'none';

     var newId = generateId();
     var newColor = getAnnColor();


     holoAnnotation = {
     "id": newId,
     "time": startInterval,
     "interval": intervalTime,
     "color": newColor,
     "active": 0
    }
    videoAnnotations.push(holoAnnotation);
    videoAnnotations.sort((a, b) => parseFloat(a.time) - parseFloat(b.time));
    socket.emit('holo annotation', holoAnnotation);

    };


    socket.on('HololensAnnotationOK', function(confirmation) {
      //textAnnotations.push(holoAnnotation);
      updateForNextAnn();
      //socket.emit('ann color', textAnnotations);
      //socket.emit('relayout', "relayout");

      //socket.emit('ann color', videoAnnotations);
      checkAnnOnScreen(2);

     });

/*
function searchInAnnotations() {
  //reset variables for new count
  number = 0;
  str = '';
  color = '';
  tempAnnotations = [];
  document.getElementById("title").innerHTML = JSON.stringify(tempAnnotations);
  document.getElementById("title1").innerHTML = str;

    //all the markers in the markers array
    Array.from(videoAnnotations).forEach((element, index) => {
      element
        if(isElementInViewport(element)){
          number += 1;
          //if it's rgb, convert it to hex
          //color = rgb2hex(element.style.backgroundColor);
          //tempAnnotations.push(color);
          var y = $(element._icon).offset().top - $(window).scrollTop();
          var position = Math.round(y * 100) / 100;
          var color = element.options.mark_color;
          var id = element.options.mark_id;
          //str = str + ' ' + color; //up
          str = str + ' ' + id;
          tempAnnotations.push({id: element.options.mark_id, color: element.options.mark_color, position: position});
          document.getElementById("title").innerHTML = JSON.stringify(tempAnnotations);
          //document.getElementById("title1").innerHTML = str;
        }
    });

    if(!compare(annotations, tempAnnotations)){
      annotations = tempAnnotations.slice();
      //annotationsJSON = JSON.stringify(annotations);
      //add a if check okay signal
      socket.emit('ann color', annotations);
    }

  }

function rgb2hex(rgb) {
    if (/^#[0-9A-F]{6}$/i.test(rgb)) return rgb;

    rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
    function hex(x) {
        return ("0" + parseInt(x).toString(16)).slice(-2);
    }
    return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
}

function isElementInViewport(el) {
    var rect = el.getBoundingClientRect();

    return rect.bottom > 0 &&
           rect.right > 0 &&
           rect.left < (window.innerWidth || document.documentElement.clientWidth) /* or $(window).width()
           rect.top < (window.innerHeight || document.documentElement.clientHeight) /* or $(window).height() ;
}

searchInAnnotations();

//important TODO maybe send the update only when it's stable (so if it doesn't change for half a second?)
window.onscroll = function () {

  searchInAnnotations();
}

window.onresize = function () {
  searchInAnnotations();
}*/

//for a jump on the timeline
myPlayer.on('seeked', function () {
    //document.getElementById("title1").innerHTML = myPlayer.currentTime();
    checkAnnOnScreen(2);
});

//only send it when it's playing
$(function(){
 setInterval(updateTimeline, 300);
});

function updateTimeline() {
  var current = myPlayer.currentTime();
  socket.emit('videocurrenttime', current);

  if(!myPlayer.paused()){
    checkAnnOnScreen(1);
    //document.getElementById("title").innerHTML = myPlayer.currentTime();
  }
}

//every time the time is updated, no separate cases
myPlayer.on('timeupdate', function () {
  videoCanvases.forEach((item, i) => {
    var currentTime = myPlayer.currentTime();
    var startInterval = parseFloat(item.getAttribute("startInterval"));
    var interval = parseFloat(item.getAttribute("intervalTime"));
    var endInterval = startInterval + interval;
    if(startInterval <= currentTime &&  currentTime <= endInterval){
      item.style.display = 'block';
    } else {
      item.style.display = 'none';
    }
  });

  //checkAnnOnScreen(1);
    //document.getElementById("title").innerHTML = myPlayer.currentTime();
});

myPlayer.on('loadedmetadata', function () {
  //document.getElementById("title").innerHTML = myPlayer.currentTime();
});


/* predefined canvases*/

// Create a blank div where we are going to put the canvas into.
var canvasContainer1 = document.createElement('div');
// Add the div into the document
 document.getElementById('my-video').appendChild(canvasContainer1);
 canvasContainer1.classList.add("video-canvas");
 canvasContainer1.id = 1;
 canvasContainer1.setAttribute("startInterval", 9);
 canvasContainer1.setAttribute("intervalTime", 1);
 canvasContainer1.style.position="absolute";
 // it will have the dimensions of the video frame
 canvasContainer1.style.left="0px";
 canvasContainer1.style.top="0px";
 canvasContainer1.style.width=canvasContainer1.parentElement.clientWidth+"px";
 canvasContainer1.style.height=canvasContainer1.parentElement.clientHeight-30+"px";
 // Set to high index so that this is always above everything else
 // (might need to be increased if you have other element at higher index)
 canvasContainer1.style.zIndex="1000";
 videoCanvasContainer = document.getElementById('col-video');

 // Now we create the canvas
 myCanvas1 = document.createElement('canvas');
 myCanvas1.style.width = canvasContainer1.scrollWidth+"px";
 myCanvas1.style.height = canvasContainer1.scrollHeight+"px";
 // You must set this otherwise the canvas will be streethed to fit the container
 myCanvas1.width=canvasContainer1.scrollWidth;
 myCanvas1.height=canvasContainer1.scrollHeight;
 myCanvas1.style.overflow = 'visible';
 myCanvas1.style.position = 'absolute';

 var context1=myCanvas1.getContext('2d');
 context1.fillStyle = 'rgba(192,192,192,0.1)';
 context1.fillRect(0,0, myCanvas1.width, myCanvas1.height);

 // Add int into the container
 canvasContainer1.appendChild(myCanvas1);

 sketcher1 = new Sketchable(myCanvas1, {
   graphics: {
     lineWidth: 8,
     firstPointSize: 0,
     strokeStyle: "#1B9E77"
   }});

var contentSk1 = '{"options":{"interactive":true,"mouseupMovements":false,"relTimestamps":false,"multitouch":true,"cssCursors":true,"filterCoords":false,"events":{},"graphics":{"firstPointSize":0,"lineWidth":8,"strokeStyle":"#1B9E77","fillStyle":"#F0F","lineCap":"round","lineJoin":"round","miterLimit":10}},"strokes":[[[442,230,1630371209657,1,0],[441,230,1630371209809,1,0],[436,230,1630371209831,1,0],[433,230,1630371209833,1,0],[428,230,1630371209840,1,0],[423,230,1630371209855,1,0],[417,230,1630371209857,1,0],[410,230,1630371209866,1,0],[404,230,1630371209873,1,0],[398,230,1630371209880,1,0],[392,230,1630371209889,1,0],[386,230,1630371209897,1,0],[380,230,1630371209905,1,0],[374,230,1630371209913,1,0],[372,230,1630371209922,1,0],[368,230,1630371209930,1,0],[363,230,1630371209937,1,0],[357,230,1630371209946,1,0],[352,230,1630371209953,1,0],[347,232,1630371209963,1,0],[343,233,1630371209970,1,0],[338,236,1630371209980,1,0],[334,237,1630371209987,1,0],[329,239,1630371209997,1,0],[326,241,1630371210003,1,0],[321,243,1630371210014,1,0],[317,245,1630371210018,1,0],[310,248,1630371210030,1,0],[305,252,1630371210035,1,0],[301,254,1630371210045,1,0],[298,256,1630371210052,1,0],[293,259,1630371210061,1,0],[289,262,1630371210068,1,0],[284,265,1630371210075,1,0],[281,267,1630371210084,1,0],[277,271,1630371210091,1,0],[273,274,1630371210100,1,0],[270,276,1630371210107,1,0],[267,279,1630371210116,1,0],[264,281,1630371210124,1,0],[261,284,1630371210132,1,0],[258,286,1630371210145,1,0],[255,289,1630371210148,1,0],[253,292,1630371210157,1,0],[251,294,1630371210164,1,0],[249,297,1630371210172,1,0],[247,299,1630371210180,1,0],[246,302,1630371210189,1,0],[244,305,1630371210197,1,0],[242,307,1630371210204,1,0],[241,309,1630371210214,1,0],[240,313,1630371210222,1,0],[237,316,1630371210230,1,0],[236,319,1630371210237,1,0],[235,322,1630371210247,1,0],[232,325,1630371210253,1,0],[231,327,1630371210264,1,0],[230,330,1630371210269,1,0],[228,333,1630371210281,1,0],[227,335,1630371210286,1,0],[225,339,1630371210297,1,0],[224,342,1630371210303,1,0],[223,345,1630371210312,1,0],[222,347,1630371210319,1,0],[221,350,1630371210328,1,0],[221,353,1630371210334,1,0],[220,356,1630371210345,1,0],[219,358,1630371210351,1,0],[218,361,1630371210358,1,0],[218,363,1630371210367,1,0],[217,366,1630371210374,1,0],[217,368,1630371210383,1,0],[216,371,1630371210390,1,0],[216,374,1630371210399,1,0],[215,377,1630371210407,1,0],[215,381,1630371210414,1,0],[215,387,1630371210423,1,0],[215,390,1630371210430,1,0],[214,395,1630371210439,1,0],[214,399,1630371210447,1,0],[214,404,1630371210455,1,0],[214,409,1630371210464,1,0],[214,413,1630371210472,1,0],[214,416,1630371210480,1,0],[214,420,1630371210487,1,0],[214,424,1630371210497,1,0],[214,427,1630371210504,1,0],[214,430,1630371210514,1,0],[214,432,1630371210520,1,0],[214,435,1630371210531,1,0],[214,437,1630371210536,1,0],[214,439,1630371210547,1,0],[214,442,1630371210552,1,0],[214,444,1630371210564,1,0],[214,446,1630371210569,1,0],[214,449,1630371210579,1,0],[215,450,1630371210585,1,0],[216,452,1630371210596,1,0],[217,455,1630371210602,1,0],[218,457,1630371210609,1,0],[219,459,1630371210618,1,0],[220,462,1630371210625,1,0],[221,463,1630371210634,1,0],[222,465,1630371210641,1,0],[223,467,1630371210649,1,0],[224,470,1630371210657,1,0],[225,472,1630371210665,1,0],[227,474,1630371210673,1,0],[228,476,1630371210682,1,0],[229,477,1630371210690,1,0],[231,479,1630371210697,1,0],[232,481,1630371210706,1,0],[233,482,1630371210714,1,0],[235,484,1630371210722,1,0],[236,485,1630371210730,1,0],[238,486,1630371210739,1,0],[239,487,1630371210747,1,0],[241,489,1630371210754,1,0],[243,490,1630371210764,1,0],[244,492,1630371210770,1,0],[246,493,1630371210781,1,0],[248,494,1630371210787,1,0],[249,495,1630371210798,1,0],[251,496,1630371210804,1,0],[252,498,1630371210814,1,0],[254,499,1630371210819,1,0],[256,500,1630371210831,1,0],[258,501,1630371210836,1,0],[260,502,1630371210846,1,0],[261,503,1630371210853,1,0],[263,504,1630371210862,1,0],[264,504,1630371210869,1,0],[266,505,1630371210876,1,0],[268,507,1630371210893,1,0],[270,507,1630371210900,1,0],[272,508,1630371210908,1,0],[274,509,1630371210918,1,0],[276,509,1630371210924,1,0],[278,511,1630371210934,1,0],[280,511,1630371210940,1,0],[283,513,1630371210948,1,0],[286,514,1630371210956,1,0],[288,515,1630371210964,1,0],[290,516,1630371210973,1,0],[293,517,1630371210981,1,0],[297,519,1630371210989,1,0],[300,519,1630371210997,1,0],[302,521,1630371211005,1,0],[306,522,1630371211015,1,0],[309,524,1630371211021,1,0],[312,524,1630371211030,1,0],[316,526,1630371211037,1,0],[321,527,1630371211047,1,0],[325,529,1630371211053,1,0],[329,531,1630371211064,1,0],[333,531,1630371211070,1,0],[338,533,1630371211081,1,0],[344,535,1630371211086,1,0],[347,535,1630371211097,1,0],[350,537,1630371211103,1,0],[354,537,1630371211114,1,0],[358,539,1630371211119,1,0],[360,540,1630371211129,1,0],[363,541,1630371211135,1,0],[367,542,1630371211146,1,0],[369,542,1630371211151,1,0],[372,544,1630371211159,1,0],[375,544,1630371211168,1,0],[378,545,1630371211175,1,0],[382,547,1630371211183,1,0],[385,547,1630371211191,1,0],[389,548,1630371211199,1,0],[394,550,1630371211207,1,0],[397,550,1630371211215,1,0],[402,551,1630371211223,1,0],[406,552,1630371211231,1,0],[411,553,1630371211240,1,0],[414,554,1630371211247,1,0],[418,554,1630371211256,1,0],[422,555,1630371211264,1,0],[425,556,1630371211272,1,0],[430,557,1630371211282,1,0],[436,559,1630371211288,1,0],[442,560,1630371211297,1,0],[445,560,1630371211304,1,0],[453,562,1630371211314,1,0],[459,563,1630371211320,1,0],[465,564,1630371211331,1,0],[472,565,1630371211337,1,0],[480,566,1630371211347,1,0],[487,567,1630371211353,1,0],[494,568,1630371211364,1,0],[503,569,1630371211370,1,0],[511,570,1630371211381,1,0],[522,572,1630371211386,1,0],[529,573,1630371211396,1,0],[534,573,1630371211403,1,0],[541,574,1630371211413,1,0],[547,574,1630371211419,1,0],[554,575,1630371211426,1,0],[563,575,1630371211434,1,0],[569,576,1630371211442,1,0],[575,576,1630371211451,1,0],[582,576,1630371211458,1,0],[588,577,1630371211466,1,0],[595,577,1630371211476,1,0],[602,578,1630371211483,1,0],[610,579,1630371211491,1,0],[613,579,1630371211498,1,0],[619,579,1630371211506,1,0],[625,580,1630371211514,1,0],[631,580,1630371211524,1,0],[636,580,1630371211531,1,0],[640,580,1630371211539,1,0],[644,580,1630371211547,1,0],[647,580,1630371211555,1,0],[650,580,1630371211564,1,0],[652,580,1630371211571,1,0],[655,580,1630371211582,1,0],[658,580,1630371211587,1,0],[661,580,1630371211597,1,0],[663,580,1630371211603,1,0],[665,580,1630371211614,1,0],[668,580,1630371211620,1,0],[671,580,1630371211631,1,0],[673,580,1630371211636,1,0],[675,580,1630371211647,1,0],[677,580,1630371211653,1,0],[680,579,1630371211663,1,0],[682,578,1630371211669,1,0],[685,578,1630371211680,1,0],[687,577,1630371211686,1,0],[690,576,1630371211696,1,0],[693,576,1630371211702,1,0],[695,575,1630371211709,1,0],[698,574,1630371211718,1,0],[700,574,1630371211725,1,0],[702,573,1630371211734,1,0],[704,572,1630371211741,1,0],[706,572,1630371211749,1,0],[708,571,1630371211757,1,0],[709,570,1630371211765,1,0],[711,570,1630371211774,1,0],[712,569,1630371211782,1,0],[714,569,1630371211790,1,0],[716,567,1630371211799,1,0],[717,567,1630371211806,1,0],[719,566,1630371211816,1,0],[720,565,1630371211822,1,0],[722,564,1630371211833,1,0],[723,564,1630371211839,1,0],[725,563,1630371211848,1,0],[726,563,1630371211856,1,0],[727,562,1630371211865,1,0],[729,561,1630371211870,1,0],[730,561,1630371211884,1,0],[731,560,1630371211889,1,0],[733,558,1630371211898,1,0],[735,557,1630371211904,1,0],[736,556,1630371211914,1,0],[738,555,1630371211919,1,0],[739,554,1630371211930,1,0],[740,552,1630371211936,1,0],[741,551,1630371211959,1,0],[745,548,1630371211974,1,0],[746,547,1630371211981,1,0],[747,546,1630371211984,1,0],[748,545,1630371211992,1,0],[749,544,1630371212001,1,0],[749,543,1630371212008,1,0],[751,543,1630371212016,1,0],[751,542,1630371212024,1,0],[752,541,1630371212032,1,0],[753,540,1630371212040,1,0],[753,539,1630371212048,1,0],[754,539,1630371212057,1,0],[754,538,1630371212064,1,0],[755,536,1630371212074,1,0],[756,535,1630371212082,1,0],[757,535,1630371212090,1,0],[757,534,1630371212098,1,0],[758,532,1630371212107,1,0],[759,532,1630371212114,1,0],[760,531,1630371212121,1,0],[760,530,1630371212131,1,0],[761,529,1630371212137,1,0],[761,528,1630371212148,1,0],[762,527,1630371212154,1,0],[763,527,1630371212165,1,0],[763,526,1630371212170,1,0],[764,526,1630371212182,1,0],[764,525,1630371212186,1,0],[764,525,1630371212197,1,0],[764,524,1630371212203,1,0],[764,524,1630371212219,1,0],[764,523,1630371212231,1,0],[764,523,1630371212236,1,0],[764,522,1630371212244,1,0],[765,521,1630371212252,1,0],[766,521,1630371212258,1,0],[766,519,1630371212267,1,0],[767,518,1630371212275,1,0],[768,516,1630371212283,1,0],[770,515,1630371212292,1,0],[771,513,1630371212299,1,0],[772,512,1630371212307,1,0],[773,511,1630371212315,1,0],[774,510,1630371212324,1,0],[774,509,1630371212333,1,0],[775,509,1630371212342,1,0],[775,508,1630371212348,1,0],[775,508,1630371212357,1,0],[776,508,1630371212364,1,0],[776,507,1630371212372,1,0],[776,507,1630371212388,1,0],[776,506,1630371212398,1,0],[776,506,1630371212404,1,0],[777,505,1630371212414,1,0],[777,504,1630371212420,1,0],[778,502,1630371212431,1,0],[779,500,1630371212437,1,0],[780,499,1630371212448,1,0],[781,497,1630371212453,1,0],[782,495,1630371212465,1,0],[784,493,1630371212470,1,0],[784,491,1630371212481,1,0],[785,490,1630371212486,1,0],[787,489,1630371212496,1,0],[787,488,1630371212502,1,0],[788,487,1630371212510,1,0],[788,486,1630371212519,1,0],[789,485,1630371212525,1,0],[789,485,1630371212535,1,0],[790,484,1630371212550,1,0],[790,484,1630371212558,1,0],[790,483,1630371212566,1,0],[791,482,1630371212574,1,0],[791,481,1630371212582,1,0],[792,480,1630371212591,1,0],[793,479,1630371212598,1,0],[793,477,1630371212607,1,0],[795,474,1630371212614,1,0],[796,472,1630371212623,1,0],[796,470,1630371212631,1,0],[798,469,1630371212639,1,0],[798,467,1630371212649,1,0],[800,465,1630371212655,1,0],[800,463,1630371212665,1,0],[801,461,1630371212671,1,0],[801,459,1630371212681,1,0],[802,457,1630371212687,1,0],[803,455,1630371212698,1,0],[803,454,1630371212704,1,0],[803,452,1630371212715,1,0],[804,449,1630371212721,1,0],[804,446,1630371212731,1,0],[805,443,1630371212736,1,0],[805,439,1630371212747,1,0],[805,435,1630371212753,1,0],[806,431,1630371212763,1,0],[806,429,1630371212768,1,0],[807,425,1630371212780,1,0],[807,421,1630371212785,1,0],[807,419,1630371212792,1,0],[808,415,1630371212801,1,0],[808,413,1630371212808,1,0],[808,410,1630371212818,1,0],[808,408,1630371212825,1,0],[808,406,1630371212833,1,0],[808,404,1630371212841,1,0],[808,401,1630371212849,1,0],[808,399,1630371212857,1,0],[808,397,1630371212865,1,0],[808,396,1630371212874,1,0],[808,394,1630371212881,1,0],[809,392,1630371212890,1,0],[809,391,1630371212902,1,0],[809,389,1630371212906,1,0],[809,387,1630371212915,1,0],[809,384,1630371212922,1,0],[809,382,1630371212932,1,0],[809,380,1630371212938,1,0],[809,378,1630371212948,1,0],[809,377,1630371212954,1,0],[809,375,1630371212965,1,0],[809,374,1630371212971,1,0],[809,373,1630371212982,1,0],[809,372,1630371212987,1,0],[809,368,1630371213014,1,0],[809,367,1630371213019,1,0],[809,366,1630371213034,1,0],[809,364,1630371213037,1,0],[809,362,1630371213044,1,0],[809,361,1630371213052,1,0],[809,359,1630371213059,1,0],[809,358,1630371213068,1,0],[808,356,1630371213076,1,0],[808,355,1630371213085,1,0],[808,354,1630371213091,1,0],[807,353,1630371213100,1,0],[807,352,1630371213108,1,0],[806,351,1630371213116,1,0],[806,350,1630371213124,1,0],[805,349,1630371213132,1,0],[805,348,1630371213141,1,0],[804,347,1630371213148,1,0],[804,346,1630371213157,1,0],[803,344,1630371213165,1,0],[802,343,1630371213173,1,0],[801,341,1630371213181,1,0],[801,339,1630371213189,1,0],[799,337,1630371213198,1,0],[799,336,1630371213205,1,0],[798,335,1630371213215,1,0],[798,334,1630371213221,1,0],[797,332,1630371213232,1,0],[796,331,1630371213238,1,0],[795,329,1630371213249,1,0],[794,327,1630371213254,1,0],[794,326,1630371213265,1,0],[792,324,1630371213271,1,0],[791,322,1630371213281,1,0],[790,321,1630371213287,1,0],[789,319,1630371213297,1,0],[788,318,1630371213303,1,0],[787,316,1630371213311,1,0],[786,315,1630371213319,1,0],[785,314,1630371213326,1,0],[784,312,1630371213335,1,0],[783,311,1630371213342,1,0],[782,310,1630371213351,1,0],[781,309,1630371213358,1,0],[780,308,1630371213367,1,0],[779,307,1630371213375,1,0],[778,307,1630371213383,1,0],[777,306,1630371213391,1,0],[776,305,1630371213399,1,0],[775,304,1630371213408,1,0],[774,303,1630371213415,1,0],[773,302,1630371213424,1,0],[773,302,1630371213431,1,0],[772,300,1630371213440,1,0],[771,300,1630371213448,1,0],[770,299,1630371213456,1,0],[770,299,1630371213465,1,0],[769,298,1630371213472,1,0],[769,298,1630371213482,1,0],[768,298,1630371213488,1,0],[768,297,1630371213504,1,0],[767,297,1630371213515,1,0],[766,296,1630371213521,1,0],[766,295,1630371213532,1,0],[765,295,1630371213537,1,0],[763,293,1630371213550,1,0],[762,292,1630371213553,1,0],[761,292,1630371213564,1,0],[760,291,1630371213569,1,0],[759,289,1630371213581,1,0],[758,288,1630371213586,1,0],[757,287,1630371213594,1,0],[755,286,1630371213602,1,0],[754,285,1630371213610,1,0],[752,284,1630371213618,1,0],[751,283,1630371213625,1,0],[750,282,1630371213634,1,0],[748,281,1630371213642,1,0],[747,281,1630371213650,1,0],[745,279,1630371213658,1,0],[743,278,1630371213666,1,0],[742,277,1630371213674,1,0],[740,276,1630371213682,1,0],[738,275,1630371213691,1,0],[737,274,1630371213700,1,0],[735,273,1630371213707,1,0],[733,272,1630371213715,1,0],[731,271,1630371213723,1,0],[729,270,1630371213732,1,0],[727,269,1630371213738,1,0],[725,268,1630371213749,1,0],[723,266,1630371213755,1,0],[721,266,1630371213765,1,0],[720,264,1630371213771,1,0],[718,263,1630371213782,1,0],[717,263,1630371213788,1,0],[715,262,1630371213799,1,0],[714,261,1630371213804,1,0],[712,260,1630371213815,1,0],[711,260,1630371213820,1,0],[709,260,1630371213831,1,0],[708,259,1630371213836,1,0],[707,258,1630371213847,1,0],[706,258,1630371213853,1,0],[705,257,1630371213860,1,0],[703,257,1630371213869,1,0],[702,256,1630371213876,1,0],[701,256,1630371213885,1,0],[699,255,1630371213892,1,0],[697,255,1630371213901,1,0],[696,254,1630371213909,1,0],[694,254,1630371213917,1,0],[692,253,1630371213925,1,0],[690,252,1630371213934,1,0],[688,251,1630371213942,1,0],[685,250,1630371213949,1,0],[684,249,1630371213958,1,0],[681,249,1630371213966,1,0],[679,247,1630371213974,1,0],[677,247,1630371213983,1,0],[675,245,1630371213990,1,0],[672,245,1630371213999,1,0],[671,243,1630371214005,1,0],[669,243,1630371214015,1,0],[667,242,1630371214022,1,0],[665,242,1630371214032,1,0],[664,241,1630371214038,1,0],[662,240,1630371214049,1,0],[660,240,1630371214054,1,0],[656,239,1630371214081,1,0],[655,238,1630371214083,1,0],[653,237,1630371214087,1,0],[652,236,1630371214103,1,0],[650,236,1630371214105,1,0],[648,235,1630371214115,1,0],[647,234,1630371214120,1,0],[645,233,1630371214127,1,0],[643,232,1630371214136,1,0],[641,231,1630371214144,1,0],[639,231,1630371214152,1,0],[636,230,1630371214159,1,0],[634,229,1630371214169,1,0],[632,228,1630371214175,1,0],[629,227,1630371214184,1,0],[626,227,1630371214192,1,0],[623,226,1630371214199,1,0],[622,225,1630371214208,1,0],[619,225,1630371214216,1,0],[616,224,1630371214224,1,0],[613,223,1630371214232,1,0],[611,223,1630371214241,1,0],[609,223,1630371214249,1,0],[607,222,1630371214256,1,0],[605,221,1630371214267,1,0],[603,221,1630371214272,1,0],[601,221,1630371214283,1,0],[599,221,1630371214290,1,0],[597,220,1630371214299,1,0],[595,220,1630371214305,1,0],[593,219,1630371214315,1,0],[591,219,1630371214321,1,0],[589,219,1630371214332,1,0],[587,219,1630371214337,1,0],[586,219,1630371214349,1,0],[584,218,1630371214354,1,0],[583,218,1630371214365,1,0],[581,218,1630371214370,1,0],[580,218,1630371214381,1,0],[578,218,1630371214386,1,0],[577,218,1630371214394,1,0],[575,218,1630371214404,1,0],[573,218,1630371214410,1,0],[572,218,1630371214419,1,0],[570,218,1630371214426,1,0],[568,218,1630371214434,1,0],[566,218,1630371214442,1,0],[564,218,1630371214450,1,0],[562,218,1630371214458,1,0],[560,218,1630371214467,1,0],[557,218,1630371214475,1,0],[555,218,1630371214482,1,0],[553,218,1630371214491,1,0],[551,217,1630371214499,1,0],[548,217,1630371214507,1,0],[546,217,1630371214516,1,0],[543,217,1630371214523,1,0],[541,217,1630371214532,1,0],[538,217,1630371214539,1,0],[536,217,1630371214549,1,0],[533,217,1630371214555,1,0],[531,217,1630371214568,1,0],[529,217,1630371214572,1,0],[527,217,1630371214583,1,0],[525,217,1630371214588,1,0],[522,217,1630371214599,1,0],[520,217,1630371214604,1,0],[518,217,1630371214616,1,0],[516,217,1630371214621,1,0],[514,217,1630371214631,1,0],[512,217,1630371214637,1,0],[511,217,1630371214647,1,0],[509,217,1630371214654,1,0],[508,217,1630371214661,1,0],[507,217,1630371214669,1,0],[506,217,1630371214677,1,0],[504,217,1630371214686,1,0],[503,217,1630371214693,1,0],[502,217,1630371214702,1,0],[502,217,1630371214709,1,0],[500,217,1630371214717,1,0],[499,217,1630371214725,1,0],[498,217,1630371214733,1,0],[497,217,1630371214742,1,0],[496,217,1630371214749,1,0],[495,217,1630371214758,1,0],[494,217,1630371214766,1,0],[493,217,1630371214774,1,0],[491,217,1630371214782,1,0],[490,217,1630371214790,1,0],[489,217,1630371214799,1,0],[487,217,1630371214806,1,0],[486,217,1630371214816,1,0],[485,217,1630371214822,1,0],[484,217,1630371214832,1,0],[482,217,1630371214839,1,0],[481,218,1630371214849,1,0],[480,218,1630371214855,1,0],[479,218,1630371214866,1,0],[477,219,1630371214871,1,0],[476,219,1630371214884,1,0],[474,219,1630371214888,1,0],[473,220,1630371214898,1,0],[472,220,1630371214904,1,0],[470,221,1630371214914,1,0],[469,221,1630371214920,1,0],[468,222,1630371214928,1,0],[466,222,1630371214937,1,0],[465,222,1630371214944,1,0],[463,223,1630371214953,1,0],[462,223,1630371214960,1,0],[461,223,1630371214969,1,0],[460,223,1630371214976,1,0],[459,224,1630371214985,1,0],[458,224,1630371214992,1,0],[457,225,1630371215000,1,0],[456,225,1630371215008,1,0],[455,225,1630371215016,1,0],[454,225,1630371215025,1,0],[453,226,1630371215032,1,0],[452,226,1630371215041,1,0],[451,227,1630371215049,1,0],[450,227,1630371215058,1,0],[449,227,1630371215066,1,0],[448,228,1630371215073,1,0],[447,228,1630371215082,1,0],[446,228,1630371215089,1,0],[445,228,1630371215099,1,0],[444,229,1630371215105,1,0],[442,229,1630371215116,1,0],[442,229,1630371215122,1,0],[440,229,1630371215144,1,0],[440,229,1630371215152,1,0],[439,229,1630371215155,1,0],[439,229,1630371215171,1,0],[439,230,1630371215187,1,0],[438,230,1630371215204,1,0],[438,230,1630371215220,1,0],[437,231,1630371215236,1,0],[436,231,1630371215243,1,0],[435,231,1630371215252,1,0],[435,232,1630371215259,1,0],[433,232,1630371215267,1,0],[430,234,1630371215276,1,0]]],"actions":[{"property":"fillStyle","value":"#F0F"},{"property":"strokeStyle","value":"#1B9E77"},{"property":"lineWidth","value":8},{"property":"lineCap","value":"round"},{"property":"lineJoin","value":"round"},{"property":"miterLimit","value":10},{"method":"beginPath"},{"method":"moveTo","args":[442,230]},{"method":"lineTo","args":[441,230]},{"method":"stroke"},{"method":"moveTo","args":[441,230]},{"method":"lineTo","args":[436,230]},{"method":"stroke"},{"method":"moveTo","args":[436,230]},{"method":"lineTo","args":[433,230]},{"method":"stroke"},{"method":"moveTo","args":[433,230]},{"method":"lineTo","args":[428,230]},{"method":"stroke"},{"method":"moveTo","args":[428,230]},{"method":"lineTo","args":[423,230]},{"method":"stroke"},{"method":"moveTo","args":[423,230]},{"method":"lineTo","args":[417,230]},{"method":"stroke"},{"method":"moveTo","args":[417,230]},{"method":"lineTo","args":[410,230]},{"method":"stroke"},{"method":"moveTo","args":[410,230]},{"method":"lineTo","args":[404,230]},{"method":"stroke"},{"method":"moveTo","args":[404,230]},{"method":"lineTo","args":[398,230]},{"method":"stroke"},{"method":"moveTo","args":[398,230]},{"method":"lineTo","args":[392,230]},{"method":"stroke"},{"method":"moveTo","args":[392,230]},{"method":"lineTo","args":[386,230]},{"method":"stroke"},{"method":"moveTo","args":[386,230]},{"method":"lineTo","args":[380,230]},{"method":"stroke"},{"method":"moveTo","args":[380,230]},{"method":"lineTo","args":[374,230]},{"method":"stroke"},{"method":"moveTo","args":[374,230]},{"method":"lineTo","args":[372,230]},{"method":"stroke"},{"method":"moveTo","args":[372,230]},{"method":"lineTo","args":[368,230]},{"method":"stroke"},{"method":"moveTo","args":[368,230]},{"method":"lineTo","args":[363,230]},{"method":"stroke"},{"method":"moveTo","args":[363,230]},{"method":"lineTo","args":[357,230]},{"method":"stroke"},{"method":"moveTo","args":[357,230]},{"method":"lineTo","args":[352,230]},{"method":"stroke"},{"method":"moveTo","args":[352,230]},{"method":"lineTo","args":[347,232]},{"method":"stroke"},{"method":"moveTo","args":[347,232]},{"method":"lineTo","args":[343,233]},{"method":"stroke"},{"method":"moveTo","args":[343,233]},{"method":"lineTo","args":[338,236]},{"method":"stroke"},{"method":"moveTo","args":[338,236]},{"method":"lineTo","args":[334,237]},{"method":"stroke"},{"method":"moveTo","args":[334,237]},{"method":"lineTo","args":[329,239]},{"method":"stroke"},{"method":"moveTo","args":[329,239]},{"method":"lineTo","args":[326,241]},{"method":"stroke"},{"method":"moveTo","args":[326,241]},{"method":"lineTo","args":[321,243]},{"method":"stroke"},{"method":"moveTo","args":[321,243]},{"method":"lineTo","args":[317,245]},{"method":"stroke"},{"method":"moveTo","args":[317,245]},{"method":"lineTo","args":[310,248]},{"method":"stroke"},{"method":"moveTo","args":[310,248]},{"method":"lineTo","args":[305,252]},{"method":"stroke"},{"method":"moveTo","args":[305,252]},{"method":"lineTo","args":[301,254]},{"method":"stroke"},{"method":"moveTo","args":[301,254]},{"method":"lineTo","args":[298,256]},{"method":"stroke"},{"method":"moveTo","args":[298,256]},{"method":"lineTo","args":[293,259]},{"method":"stroke"},{"method":"moveTo","args":[293,259]},{"method":"lineTo","args":[289,262]},{"method":"stroke"},{"method":"moveTo","args":[289,262]},{"method":"lineTo","args":[284,265]},{"method":"stroke"},{"method":"moveTo","args":[284,265]},{"method":"lineTo","args":[281,267]},{"method":"stroke"},{"method":"moveTo","args":[281,267]},{"method":"lineTo","args":[277,271]},{"method":"stroke"},{"method":"moveTo","args":[277,271]},{"method":"lineTo","args":[273,274]},{"method":"stroke"},{"method":"moveTo","args":[273,274]},{"method":"lineTo","args":[270,276]},{"method":"stroke"},{"method":"moveTo","args":[270,276]},{"method":"lineTo","args":[267,279]},{"method":"stroke"},{"method":"moveTo","args":[267,279]},{"method":"lineTo","args":[264,281]},{"method":"stroke"},{"method":"moveTo","args":[264,281]},{"method":"lineTo","args":[261,284]},{"method":"stroke"},{"method":"moveTo","args":[261,284]},{"method":"lineTo","args":[258,286]},{"method":"stroke"},{"method":"moveTo","args":[258,286]},{"method":"lineTo","args":[255,289]},{"method":"stroke"},{"method":"moveTo","args":[255,289]},{"method":"lineTo","args":[253,292]},{"method":"stroke"},{"method":"moveTo","args":[253,292]},{"method":"lineTo","args":[251,294]},{"method":"stroke"},{"method":"moveTo","args":[251,294]},{"method":"lineTo","args":[249,297]},{"method":"stroke"},{"method":"moveTo","args":[249,297]},{"method":"lineTo","args":[247,299]},{"method":"stroke"},{"method":"moveTo","args":[247,299]},{"method":"lineTo","args":[246,302]},{"method":"stroke"},{"method":"moveTo","args":[246,302]},{"method":"lineTo","args":[244,305]},{"method":"stroke"},{"method":"moveTo","args":[244,305]},{"method":"lineTo","args":[242,307]},{"method":"stroke"},{"method":"moveTo","args":[242,307]},{"method":"lineTo","args":[241,309]},{"method":"stroke"},{"method":"moveTo","args":[241,309]},{"method":"lineTo","args":[240,313]},{"method":"stroke"},{"method":"moveTo","args":[240,313]},{"method":"lineTo","args":[237,316]},{"method":"stroke"},{"method":"moveTo","args":[237,316]},{"method":"lineTo","args":[236,319]},{"method":"stroke"},{"method":"moveTo","args":[236,319]},{"method":"lineTo","args":[235,322]},{"method":"stroke"},{"method":"moveTo","args":[235,322]},{"method":"lineTo","args":[232,325]},{"method":"stroke"},{"method":"moveTo","args":[232,325]},{"method":"lineTo","args":[231,327]},{"method":"stroke"},{"method":"moveTo","args":[231,327]},{"method":"lineTo","args":[230,330]},{"method":"stroke"},{"method":"moveTo","args":[230,330]},{"method":"lineTo","args":[228,333]},{"method":"stroke"},{"method":"moveTo","args":[228,333]},{"method":"lineTo","args":[227,335]},{"method":"stroke"},{"method":"moveTo","args":[227,335]},{"method":"lineTo","args":[225,339]},{"method":"stroke"},{"method":"moveTo","args":[225,339]},{"method":"lineTo","args":[224,342]},{"method":"stroke"},{"method":"moveTo","args":[224,342]},{"method":"lineTo","args":[223,345]},{"method":"stroke"},{"method":"moveTo","args":[223,345]},{"method":"lineTo","args":[222,347]},{"method":"stroke"},{"method":"moveTo","args":[222,347]},{"method":"lineTo","args":[221,350]},{"method":"stroke"},{"method":"moveTo","args":[221,350]},{"method":"lineTo","args":[221,353]},{"method":"stroke"},{"method":"moveTo","args":[221,353]},{"method":"lineTo","args":[220,356]},{"method":"stroke"},{"method":"moveTo","args":[220,356]},{"method":"lineTo","args":[219,358]},{"method":"stroke"},{"method":"moveTo","args":[219,358]},{"method":"lineTo","args":[218,361]},{"method":"stroke"},{"method":"moveTo","args":[218,361]},{"method":"lineTo","args":[218,363]},{"method":"stroke"},{"method":"moveTo","args":[218,363]},{"method":"lineTo","args":[217,366]},{"method":"stroke"},{"method":"moveTo","args":[217,366]},{"method":"lineTo","args":[217,368]},{"method":"stroke"},{"method":"moveTo","args":[217,368]},{"method":"lineTo","args":[216,371]},{"method":"stroke"},{"method":"moveTo","args":[216,371]},{"method":"lineTo","args":[216,374]},{"method":"stroke"},{"method":"moveTo","args":[216,374]},{"method":"lineTo","args":[215,377]},{"method":"stroke"},{"method":"moveTo","args":[215,377]},{"method":"lineTo","args":[215,381]},{"method":"stroke"},{"method":"moveTo","args":[215,381]},{"method":"lineTo","args":[215,387]},{"method":"stroke"},{"method":"moveTo","args":[215,387]},{"method":"lineTo","args":[215,390]},{"method":"stroke"},{"method":"moveTo","args":[215,390]},{"method":"lineTo","args":[214,395]},{"method":"stroke"},{"method":"moveTo","args":[214,395]},{"method":"lineTo","args":[214,399]},{"method":"stroke"},{"method":"moveTo","args":[214,399]},{"method":"lineTo","args":[214,404]},{"method":"stroke"},{"method":"moveTo","args":[214,404]},{"method":"lineTo","args":[214,409]},{"method":"stroke"},{"method":"moveTo","args":[214,409]},{"method":"lineTo","args":[214,413]},{"method":"stroke"},{"method":"moveTo","args":[214,413]},{"method":"lineTo","args":[214,416]},{"method":"stroke"},{"method":"moveTo","args":[214,416]},{"method":"lineTo","args":[214,420]},{"method":"stroke"},{"method":"moveTo","args":[214,420]},{"method":"lineTo","args":[214,424]},{"method":"stroke"},{"method":"moveTo","args":[214,424]},{"method":"lineTo","args":[214,427]},{"method":"stroke"},{"method":"moveTo","args":[214,427]},{"method":"lineTo","args":[214,430]},{"method":"stroke"},{"method":"moveTo","args":[214,430]},{"method":"lineTo","args":[214,432]},{"method":"stroke"},{"method":"moveTo","args":[214,432]},{"method":"lineTo","args":[214,435]},{"method":"stroke"},{"method":"moveTo","args":[214,435]},{"method":"lineTo","args":[214,437]},{"method":"stroke"},{"method":"moveTo","args":[214,437]},{"method":"lineTo","args":[214,439]},{"method":"stroke"},{"method":"moveTo","args":[214,439]},{"method":"lineTo","args":[214,442]},{"method":"stroke"},{"method":"moveTo","args":[214,442]},{"method":"lineTo","args":[214,444]},{"method":"stroke"},{"method":"moveTo","args":[214,444]},{"method":"lineTo","args":[214,446]},{"method":"stroke"},{"method":"moveTo","args":[214,446]},{"method":"lineTo","args":[214,449]},{"method":"stroke"},{"method":"moveTo","args":[214,449]},{"method":"lineTo","args":[215,450]},{"method":"stroke"},{"method":"moveTo","args":[215,450]},{"method":"lineTo","args":[216,452]},{"method":"stroke"},{"method":"moveTo","args":[216,452]},{"method":"lineTo","args":[217,455]},{"method":"stroke"},{"method":"moveTo","args":[217,455]},{"method":"lineTo","args":[218,457]},{"method":"stroke"},{"method":"moveTo","args":[218,457]},{"method":"lineTo","args":[219,459]},{"method":"stroke"},{"method":"moveTo","args":[219,459]},{"method":"lineTo","args":[220,462]},{"method":"stroke"},{"method":"moveTo","args":[220,462]},{"method":"lineTo","args":[221,463]},{"method":"stroke"},{"method":"moveTo","args":[221,463]},{"method":"lineTo","args":[222,465]},{"method":"stroke"},{"method":"moveTo","args":[222,465]},{"method":"lineTo","args":[223,467]},{"method":"stroke"},{"method":"moveTo","args":[223,467]},{"method":"lineTo","args":[224,470]},{"method":"stroke"},{"method":"moveTo","args":[224,470]},{"method":"lineTo","args":[225,472]},{"method":"stroke"},{"method":"moveTo","args":[225,472]},{"method":"lineTo","args":[227,474]},{"method":"stroke"},{"method":"moveTo","args":[227,474]},{"method":"lineTo","args":[228,476]},{"method":"stroke"},{"method":"moveTo","args":[228,476]},{"method":"lineTo","args":[229,477]},{"method":"stroke"},{"method":"moveTo","args":[229,477]},{"method":"lineTo","args":[231,479]},{"method":"stroke"},{"method":"moveTo","args":[231,479]},{"method":"lineTo","args":[232,481]},{"method":"stroke"},{"method":"moveTo","args":[232,481]},{"method":"lineTo","args":[233,482]},{"method":"stroke"},{"method":"moveTo","args":[233,482]},{"method":"lineTo","args":[235,484]},{"method":"stroke"},{"method":"moveTo","args":[235,484]},{"method":"lineTo","args":[236,485]},{"method":"stroke"},{"method":"moveTo","args":[236,485]},{"method":"lineTo","args":[238,486]},{"method":"stroke"},{"method":"moveTo","args":[238,486]},{"method":"lineTo","args":[239,487]},{"method":"stroke"},{"method":"moveTo","args":[239,487]},{"method":"lineTo","args":[241,489]},{"method":"stroke"},{"method":"moveTo","args":[241,489]},{"method":"lineTo","args":[243,490]},{"method":"stroke"},{"method":"moveTo","args":[243,490]},{"method":"lineTo","args":[244,492]},{"method":"stroke"},{"method":"moveTo","args":[244,492]},{"method":"lineTo","args":[246,493]},{"method":"stroke"},{"method":"moveTo","args":[246,493]},{"method":"lineTo","args":[248,494]},{"method":"stroke"},{"method":"moveTo","args":[248,494]},{"method":"lineTo","args":[249,495]},{"method":"stroke"},{"method":"moveTo","args":[249,495]},{"method":"lineTo","args":[251,496]},{"method":"stroke"},{"method":"moveTo","args":[251,496]},{"method":"lineTo","args":[252,498]},{"method":"stroke"},{"method":"moveTo","args":[252,498]},{"method":"lineTo","args":[254,499]},{"method":"stroke"},{"method":"moveTo","args":[254,499]},{"method":"lineTo","args":[256,500]},{"method":"stroke"},{"method":"moveTo","args":[256,500]},{"method":"lineTo","args":[258,501]},{"method":"stroke"},{"method":"moveTo","args":[258,501]},{"method":"lineTo","args":[260,502]},{"method":"stroke"},{"method":"moveTo","args":[260,502]},{"method":"lineTo","args":[261,503]},{"method":"stroke"},{"method":"moveTo","args":[261,503]},{"method":"lineTo","args":[263,504]},{"method":"stroke"},{"method":"moveTo","args":[263,504]},{"method":"lineTo","args":[264,504]},{"method":"stroke"},{"method":"moveTo","args":[264,504]},{"method":"lineTo","args":[266,505]},{"method":"stroke"},{"method":"moveTo","args":[266,505]},{"method":"lineTo","args":[268,507]},{"method":"stroke"},{"method":"moveTo","args":[268,507]},{"method":"lineTo","args":[270,507]},{"method":"stroke"},{"method":"moveTo","args":[270,507]},{"method":"lineTo","args":[272,508]},{"method":"stroke"},{"method":"moveTo","args":[272,508]},{"method":"lineTo","args":[274,509]},{"method":"stroke"},{"method":"moveTo","args":[274,509]},{"method":"lineTo","args":[276,509]},{"method":"stroke"},{"method":"moveTo","args":[276,509]},{"method":"lineTo","args":[278,511]},{"method":"stroke"},{"method":"moveTo","args":[278,511]},{"method":"lineTo","args":[280,511]},{"method":"stroke"},{"method":"moveTo","args":[280,511]},{"method":"lineTo","args":[283,513]},{"method":"stroke"},{"method":"moveTo","args":[283,513]},{"method":"lineTo","args":[286,514]},{"method":"stroke"},{"method":"moveTo","args":[286,514]},{"method":"lineTo","args":[288,515]},{"method":"stroke"},{"method":"moveTo","args":[288,515]},{"method":"lineTo","args":[290,516]},{"method":"stroke"},{"method":"moveTo","args":[290,516]},{"method":"lineTo","args":[293,517]},{"method":"stroke"},{"method":"moveTo","args":[293,517]},{"method":"lineTo","args":[297,519]},{"method":"stroke"},{"method":"moveTo","args":[297,519]},{"method":"lineTo","args":[300,519]},{"method":"stroke"},{"method":"moveTo","args":[300,519]},{"method":"lineTo","args":[302,521]},{"method":"stroke"},{"method":"moveTo","args":[302,521]},{"method":"lineTo","args":[306,522]},{"method":"stroke"},{"method":"moveTo","args":[306,522]},{"method":"lineTo","args":[309,524]},{"method":"stroke"},{"method":"moveTo","args":[309,524]},{"method":"lineTo","args":[312,524]},{"method":"stroke"},{"method":"moveTo","args":[312,524]},{"method":"lineTo","args":[316,526]},{"method":"stroke"},{"method":"moveTo","args":[316,526]},{"method":"lineTo","args":[321,527]},{"method":"stroke"},{"method":"moveTo","args":[321,527]},{"method":"lineTo","args":[325,529]},{"method":"stroke"},{"method":"moveTo","args":[325,529]},{"method":"lineTo","args":[329,531]},{"method":"stroke"},{"method":"moveTo","args":[329,531]},{"method":"lineTo","args":[333,531]},{"method":"stroke"},{"method":"moveTo","args":[333,531]},{"method":"lineTo","args":[338,533]},{"method":"stroke"},{"method":"moveTo","args":[338,533]},{"method":"lineTo","args":[344,535]},{"method":"stroke"},{"method":"moveTo","args":[344,535]},{"method":"lineTo","args":[347,535]},{"method":"stroke"},{"method":"moveTo","args":[347,535]},{"method":"lineTo","args":[350,537]},{"method":"stroke"},{"method":"moveTo","args":[350,537]},{"method":"lineTo","args":[354,537]},{"method":"stroke"},{"method":"moveTo","args":[354,537]},{"method":"lineTo","args":[358,539]},{"method":"stroke"},{"method":"moveTo","args":[358,539]},{"method":"lineTo","args":[360,540]},{"method":"stroke"},{"method":"moveTo","args":[360,540]},{"method":"lineTo","args":[363,541]},{"method":"stroke"},{"method":"moveTo","args":[363,541]},{"method":"lineTo","args":[367,542]},{"method":"stroke"},{"method":"moveTo","args":[367,542]},{"method":"lineTo","args":[369,542]},{"method":"stroke"},{"method":"moveTo","args":[369,542]},{"method":"lineTo","args":[372,544]},{"method":"stroke"},{"method":"moveTo","args":[372,544]},{"method":"lineTo","args":[375,544]},{"method":"stroke"},{"method":"moveTo","args":[375,544]},{"method":"lineTo","args":[378,545]},{"method":"stroke"},{"method":"moveTo","args":[378,545]},{"method":"lineTo","args":[382,547]},{"method":"stroke"},{"method":"moveTo","args":[382,547]},{"method":"lineTo","args":[385,547]},{"method":"stroke"},{"method":"moveTo","args":[385,547]},{"method":"lineTo","args":[389,548]},{"method":"stroke"},{"method":"moveTo","args":[389,548]},{"method":"lineTo","args":[394,550]},{"method":"stroke"},{"method":"moveTo","args":[394,550]},{"method":"lineTo","args":[397,550]},{"method":"stroke"},{"method":"moveTo","args":[397,550]},{"method":"lineTo","args":[402,551]},{"method":"stroke"},{"method":"moveTo","args":[402,551]},{"method":"lineTo","args":[406,552]},{"method":"stroke"},{"method":"moveTo","args":[406,552]},{"method":"lineTo","args":[411,553]},{"method":"stroke"},{"method":"moveTo","args":[411,553]},{"method":"lineTo","args":[414,554]},{"method":"stroke"},{"method":"moveTo","args":[414,554]},{"method":"lineTo","args":[418,554]},{"method":"stroke"},{"method":"moveTo","args":[418,554]},{"method":"lineTo","args":[422,555]},{"method":"stroke"},{"method":"moveTo","args":[422,555]},{"method":"lineTo","args":[425,556]},{"method":"stroke"},{"method":"moveTo","args":[425,556]},{"method":"lineTo","args":[430,557]},{"method":"stroke"},{"method":"moveTo","args":[430,557]},{"method":"lineTo","args":[436,559]},{"method":"stroke"},{"method":"moveTo","args":[436,559]},{"method":"lineTo","args":[442,560]},{"method":"stroke"},{"method":"moveTo","args":[442,560]},{"method":"lineTo","args":[445,560]},{"method":"stroke"},{"method":"moveTo","args":[445,560]},{"method":"lineTo","args":[453,562]},{"method":"stroke"},{"method":"moveTo","args":[453,562]},{"method":"lineTo","args":[459,563]},{"method":"stroke"},{"method":"moveTo","args":[459,563]},{"method":"lineTo","args":[465,564]},{"method":"stroke"},{"method":"moveTo","args":[465,564]},{"method":"lineTo","args":[472,565]},{"method":"stroke"},{"method":"moveTo","args":[472,565]},{"method":"lineTo","args":[480,566]},{"method":"stroke"},{"method":"moveTo","args":[480,566]},{"method":"lineTo","args":[487,567]},{"method":"stroke"},{"method":"moveTo","args":[487,567]},{"method":"lineTo","args":[494,568]},{"method":"stroke"},{"method":"moveTo","args":[494,568]},{"method":"lineTo","args":[503,569]},{"method":"stroke"},{"method":"moveTo","args":[503,569]},{"method":"lineTo","args":[511,570]},{"method":"stroke"},{"method":"moveTo","args":[511,570]},{"method":"lineTo","args":[522,572]},{"method":"stroke"},{"method":"moveTo","args":[522,572]},{"method":"lineTo","args":[529,573]},{"method":"stroke"},{"method":"moveTo","args":[529,573]},{"method":"lineTo","args":[534,573]},{"method":"stroke"},{"method":"moveTo","args":[534,573]},{"method":"lineTo","args":[541,574]},{"method":"stroke"},{"method":"moveTo","args":[541,574]},{"method":"lineTo","args":[547,574]},{"method":"stroke"},{"method":"moveTo","args":[547,574]},{"method":"lineTo","args":[554,575]},{"method":"stroke"},{"method":"moveTo","args":[554,575]},{"method":"lineTo","args":[563,575]},{"method":"stroke"},{"method":"moveTo","args":[563,575]},{"method":"lineTo","args":[569,576]},{"method":"stroke"},{"method":"moveTo","args":[569,576]},{"method":"lineTo","args":[575,576]},{"method":"stroke"},{"method":"moveTo","args":[575,576]},{"method":"lineTo","args":[582,576]},{"method":"stroke"},{"method":"moveTo","args":[582,576]},{"method":"lineTo","args":[588,577]},{"method":"stroke"},{"method":"moveTo","args":[588,577]},{"method":"lineTo","args":[595,577]},{"method":"stroke"},{"method":"moveTo","args":[595,577]},{"method":"lineTo","args":[602,578]},{"method":"stroke"},{"method":"moveTo","args":[602,578]},{"method":"lineTo","args":[610,579]},{"method":"stroke"},{"method":"moveTo","args":[610,579]},{"method":"lineTo","args":[613,579]},{"method":"stroke"},{"method":"moveTo","args":[613,579]},{"method":"lineTo","args":[619,579]},{"method":"stroke"},{"method":"moveTo","args":[619,579]},{"method":"lineTo","args":[625,580]},{"method":"stroke"},{"method":"moveTo","args":[625,580]},{"method":"lineTo","args":[631,580]},{"method":"stroke"},{"method":"moveTo","args":[631,580]},{"method":"lineTo","args":[636,580]},{"method":"stroke"},{"method":"moveTo","args":[636,580]},{"method":"lineTo","args":[640,580]},{"method":"stroke"},{"method":"moveTo","args":[640,580]},{"method":"lineTo","args":[644,580]},{"method":"stroke"},{"method":"moveTo","args":[644,580]},{"method":"lineTo","args":[647,580]},{"method":"stroke"},{"method":"moveTo","args":[647,580]},{"method":"lineTo","args":[650,580]},{"method":"stroke"},{"method":"moveTo","args":[650,580]},{"method":"lineTo","args":[652,580]},{"method":"stroke"},{"method":"moveTo","args":[652,580]},{"method":"lineTo","args":[655,580]},{"method":"stroke"},{"method":"moveTo","args":[655,580]},{"method":"lineTo","args":[658,580]},{"method":"stroke"},{"method":"moveTo","args":[658,580]},{"method":"lineTo","args":[661,580]},{"method":"stroke"},{"method":"moveTo","args":[661,580]},{"method":"lineTo","args":[663,580]},{"method":"stroke"},{"method":"moveTo","args":[663,580]},{"method":"lineTo","args":[665,580]},{"method":"stroke"},{"method":"moveTo","args":[665,580]},{"method":"lineTo","args":[668,580]},{"method":"stroke"},{"method":"moveTo","args":[668,580]},{"method":"lineTo","args":[671,580]},{"method":"stroke"},{"method":"moveTo","args":[671,580]},{"method":"lineTo","args":[673,580]},{"method":"stroke"},{"method":"moveTo","args":[673,580]},{"method":"lineTo","args":[675,580]},{"method":"stroke"},{"method":"moveTo","args":[675,580]},{"method":"lineTo","args":[677,580]},{"method":"stroke"},{"method":"moveTo","args":[677,580]},{"method":"lineTo","args":[680,579]},{"method":"stroke"},{"method":"moveTo","args":[680,579]},{"method":"lineTo","args":[682,578]},{"method":"stroke"},{"method":"moveTo","args":[682,578]},{"method":"lineTo","args":[685,578]},{"method":"stroke"},{"method":"moveTo","args":[685,578]},{"method":"lineTo","args":[687,577]},{"method":"stroke"},{"method":"moveTo","args":[687,577]},{"method":"lineTo","args":[690,576]},{"method":"stroke"},{"method":"moveTo","args":[690,576]},{"method":"lineTo","args":[693,576]},{"method":"stroke"},{"method":"moveTo","args":[693,576]},{"method":"lineTo","args":[695,575]},{"method":"stroke"},{"method":"moveTo","args":[695,575]},{"method":"lineTo","args":[698,574]},{"method":"stroke"},{"method":"moveTo","args":[698,574]},{"method":"lineTo","args":[700,574]},{"method":"stroke"},{"method":"moveTo","args":[700,574]},{"method":"lineTo","args":[702,573]},{"method":"stroke"},{"method":"moveTo","args":[702,573]},{"method":"lineTo","args":[704,572]},{"method":"stroke"},{"method":"moveTo","args":[704,572]},{"method":"lineTo","args":[706,572]},{"method":"stroke"},{"method":"moveTo","args":[706,572]},{"method":"lineTo","args":[708,571]},{"method":"stroke"},{"method":"moveTo","args":[708,571]},{"method":"lineTo","args":[709,570]},{"method":"stroke"},{"method":"moveTo","args":[709,570]},{"method":"lineTo","args":[711,570]},{"method":"stroke"},{"method":"moveTo","args":[711,570]},{"method":"lineTo","args":[712,569]},{"method":"stroke"},{"method":"moveTo","args":[712,569]},{"method":"lineTo","args":[714,569]},{"method":"stroke"},{"method":"moveTo","args":[714,569]},{"method":"lineTo","args":[716,567]},{"method":"stroke"},{"method":"moveTo","args":[716,567]},{"method":"lineTo","args":[717,567]},{"method":"stroke"},{"method":"moveTo","args":[717,567]},{"method":"lineTo","args":[719,566]},{"method":"stroke"},{"method":"moveTo","args":[719,566]},{"method":"lineTo","args":[720,565]},{"method":"stroke"},{"method":"moveTo","args":[720,565]},{"method":"lineTo","args":[722,564]},{"method":"stroke"},{"method":"moveTo","args":[722,564]},{"method":"lineTo","args":[723,564]},{"method":"stroke"},{"method":"moveTo","args":[723,564]},{"method":"lineTo","args":[725,563]},{"method":"stroke"},{"method":"moveTo","args":[725,563]},{"method":"lineTo","args":[726,563]},{"method":"stroke"},{"method":"moveTo","args":[726,563]},{"method":"lineTo","args":[727,562]},{"method":"stroke"},{"method":"moveTo","args":[727,562]},{"method":"lineTo","args":[729,561]},{"method":"stroke"},{"method":"moveTo","args":[729,561]},{"method":"lineTo","args":[730,561]},{"method":"stroke"},{"method":"moveTo","args":[730,561]},{"method":"lineTo","args":[731,560]},{"method":"stroke"},{"method":"moveTo","args":[731,560]},{"method":"lineTo","args":[733,558]},{"method":"stroke"},{"method":"moveTo","args":[733,558]},{"method":"lineTo","args":[735,557]},{"method":"stroke"},{"method":"moveTo","args":[735,557]},{"method":"lineTo","args":[736,556]},{"method":"stroke"},{"method":"moveTo","args":[736,556]},{"method":"lineTo","args":[738,555]},{"method":"stroke"},{"method":"moveTo","args":[738,555]},{"method":"lineTo","args":[739,554]},{"method":"stroke"},{"method":"moveTo","args":[739,554]},{"method":"lineTo","args":[740,552]},{"method":"stroke"},{"method":"moveTo","args":[740,552]},{"method":"lineTo","args":[741,551]},{"method":"stroke"},{"method":"moveTo","args":[741,551]},{"method":"lineTo","args":[745,548]},{"method":"stroke"},{"method":"moveTo","args":[745,548]},{"method":"lineTo","args":[746,547]},{"method":"stroke"},{"method":"moveTo","args":[746,547]},{"method":"lineTo","args":[747,546]},{"method":"stroke"},{"method":"moveTo","args":[747,546]},{"method":"lineTo","args":[748,545]},{"method":"stroke"},{"method":"moveTo","args":[748,545]},{"method":"lineTo","args":[749,544]},{"method":"stroke"},{"method":"moveTo","args":[749,544]},{"method":"lineTo","args":[749,543]},{"method":"stroke"},{"method":"moveTo","args":[749,543]},{"method":"lineTo","args":[751,543]},{"method":"stroke"},{"method":"moveTo","args":[751,543]},{"method":"lineTo","args":[751,542]},{"method":"stroke"},{"method":"moveTo","args":[751,542]},{"method":"lineTo","args":[752,541]},{"method":"stroke"},{"method":"moveTo","args":[752,541]},{"method":"lineTo","args":[753,540]},{"method":"stroke"},{"method":"moveTo","args":[753,540]},{"method":"lineTo","args":[753,539]},{"method":"stroke"},{"method":"moveTo","args":[753,539]},{"method":"lineTo","args":[754,539]},{"method":"stroke"},{"method":"moveTo","args":[754,539]},{"method":"lineTo","args":[754,538]},{"method":"stroke"},{"method":"moveTo","args":[754,538]},{"method":"lineTo","args":[755,536]},{"method":"stroke"},{"method":"moveTo","args":[755,536]},{"method":"lineTo","args":[756,535]},{"method":"stroke"},{"method":"moveTo","args":[756,535]},{"method":"lineTo","args":[757,535]},{"method":"stroke"},{"method":"moveTo","args":[757,535]},{"method":"lineTo","args":[757,534]},{"method":"stroke"},{"method":"moveTo","args":[757,534]},{"method":"lineTo","args":[758,532]},{"method":"stroke"},{"method":"moveTo","args":[758,532]},{"method":"lineTo","args":[759,532]},{"method":"stroke"},{"method":"moveTo","args":[759,532]},{"method":"lineTo","args":[760,531]},{"method":"stroke"},{"method":"moveTo","args":[760,531]},{"method":"lineTo","args":[760,530]},{"method":"stroke"},{"method":"moveTo","args":[760,530]},{"method":"lineTo","args":[761,529]},{"method":"stroke"},{"method":"moveTo","args":[761,529]},{"method":"lineTo","args":[761,528]},{"method":"stroke"},{"method":"moveTo","args":[761,528]},{"method":"lineTo","args":[762,527]},{"method":"stroke"},{"method":"moveTo","args":[762,527]},{"method":"lineTo","args":[763,527]},{"method":"stroke"},{"method":"moveTo","args":[763,527]},{"method":"lineTo","args":[763,526]},{"method":"stroke"},{"method":"moveTo","args":[763,526]},{"method":"lineTo","args":[764,526]},{"method":"stroke"},{"method":"moveTo","args":[764,526]},{"method":"lineTo","args":[764,525]},{"method":"stroke"},{"method":"moveTo","args":[764,525]},{"method":"lineTo","args":[764,525]},{"method":"stroke"},{"method":"moveTo","args":[764,525]},{"method":"lineTo","args":[764,524]},{"method":"stroke"},{"method":"moveTo","args":[764,524]},{"method":"lineTo","args":[764,524]},{"method":"stroke"},{"method":"moveTo","args":[764,524]},{"method":"lineTo","args":[764,523]},{"method":"stroke"},{"method":"moveTo","args":[764,523]},{"method":"lineTo","args":[764,523]},{"method":"stroke"},{"method":"moveTo","args":[764,523]},{"method":"lineTo","args":[764,522]},{"method":"stroke"},{"method":"moveTo","args":[764,522]},{"method":"lineTo","args":[765,521]},{"method":"stroke"},{"method":"moveTo","args":[765,521]},{"method":"lineTo","args":[766,521]},{"method":"stroke"},{"method":"moveTo","args":[766,521]},{"method":"lineTo","args":[766,519]},{"method":"stroke"},{"method":"moveTo","args":[766,519]},{"method":"lineTo","args":[767,518]},{"method":"stroke"},{"method":"moveTo","args":[767,518]},{"method":"lineTo","args":[768,516]},{"method":"stroke"},{"method":"moveTo","args":[768,516]},{"method":"lineTo","args":[770,515]},{"method":"stroke"},{"method":"moveTo","args":[770,515]},{"method":"lineTo","args":[771,513]},{"method":"stroke"},{"method":"moveTo","args":[771,513]},{"method":"lineTo","args":[772,512]},{"method":"stroke"},{"method":"moveTo","args":[772,512]},{"method":"lineTo","args":[773,511]},{"method":"stroke"},{"method":"moveTo","args":[773,511]},{"method":"lineTo","args":[774,510]},{"method":"stroke"},{"method":"moveTo","args":[774,510]},{"method":"lineTo","args":[774,509]},{"method":"stroke"},{"method":"moveTo","args":[774,509]},{"method":"lineTo","args":[775,509]},{"method":"stroke"},{"method":"moveTo","args":[775,509]},{"method":"lineTo","args":[775,508]},{"method":"stroke"},{"method":"moveTo","args":[775,508]},{"method":"lineTo","args":[775,508]},{"method":"stroke"},{"method":"moveTo","args":[775,508]},{"method":"lineTo","args":[776,508]},{"method":"stroke"},{"method":"moveTo","args":[776,508]},{"method":"lineTo","args":[776,507]},{"method":"stroke"},{"method":"moveTo","args":[776,507]},{"method":"lineTo","args":[776,507]},{"method":"stroke"},{"method":"moveTo","args":[776,507]},{"method":"lineTo","args":[776,506]},{"method":"stroke"},{"method":"moveTo","args":[776,506]},{"method":"lineTo","args":[776,506]},{"method":"stroke"},{"method":"moveTo","args":[776,506]},{"method":"lineTo","args":[777,505]},{"method":"stroke"},{"method":"moveTo","args":[777,505]},{"method":"lineTo","args":[777,504]},{"method":"stroke"},{"method":"moveTo","args":[777,504]},{"method":"lineTo","args":[778,502]},{"method":"stroke"},{"method":"moveTo","args":[778,502]},{"method":"lineTo","args":[779,500]},{"method":"stroke"},{"method":"moveTo","args":[779,500]},{"method":"lineTo","args":[780,499]},{"method":"stroke"},{"method":"moveTo","args":[780,499]},{"method":"lineTo","args":[781,497]},{"method":"stroke"},{"method":"moveTo","args":[781,497]},{"method":"lineTo","args":[782,495]},{"method":"stroke"},{"method":"moveTo","args":[782,495]},{"method":"lineTo","args":[784,493]},{"method":"stroke"},{"method":"moveTo","args":[784,493]},{"method":"lineTo","args":[784,491]},{"method":"stroke"},{"method":"moveTo","args":[784,491]},{"method":"lineTo","args":[785,490]},{"method":"stroke"},{"method":"moveTo","args":[785,490]},{"method":"lineTo","args":[787,489]},{"method":"stroke"},{"method":"moveTo","args":[787,489]},{"method":"lineTo","args":[787,488]},{"method":"stroke"},{"method":"moveTo","args":[787,488]},{"method":"lineTo","args":[788,487]},{"method":"stroke"},{"method":"moveTo","args":[788,487]},{"method":"lineTo","args":[788,486]},{"method":"stroke"},{"method":"moveTo","args":[788,486]},{"method":"lineTo","args":[789,485]},{"method":"stroke"},{"method":"moveTo","args":[789,485]},{"method":"lineTo","args":[789,485]},{"method":"stroke"},{"method":"moveTo","args":[789,485]},{"method":"lineTo","args":[790,484]},{"method":"stroke"},{"method":"moveTo","args":[790,484]},{"method":"lineTo","args":[790,484]},{"method":"stroke"},{"method":"moveTo","args":[790,484]},{"method":"lineTo","args":[790,483]},{"method":"stroke"},{"method":"moveTo","args":[790,483]},{"method":"lineTo","args":[791,482]},{"method":"stroke"},{"method":"moveTo","args":[791,482]},{"method":"lineTo","args":[791,481]},{"method":"stroke"},{"method":"moveTo","args":[791,481]},{"method":"lineTo","args":[792,480]},{"method":"stroke"},{"method":"moveTo","args":[792,480]},{"method":"lineTo","args":[793,479]},{"method":"stroke"},{"method":"moveTo","args":[793,479]},{"method":"lineTo","args":[793,477]},{"method":"stroke"},{"method":"moveTo","args":[793,477]},{"method":"lineTo","args":[795,474]},{"method":"stroke"},{"method":"moveTo","args":[795,474]},{"method":"lineTo","args":[796,472]},{"method":"stroke"},{"method":"moveTo","args":[796,472]},{"method":"lineTo","args":[796,470]},{"method":"stroke"},{"method":"moveTo","args":[796,470]},{"method":"lineTo","args":[798,469]},{"method":"stroke"},{"method":"moveTo","args":[798,469]},{"method":"lineTo","args":[798,467]},{"method":"stroke"},{"method":"moveTo","args":[798,467]},{"method":"lineTo","args":[800,465]},{"method":"stroke"},{"method":"moveTo","args":[800,465]},{"method":"lineTo","args":[800,463]},{"method":"stroke"},{"method":"moveTo","args":[800,463]},{"method":"lineTo","args":[801,461]},{"method":"stroke"},{"method":"moveTo","args":[801,461]},{"method":"lineTo","args":[801,459]},{"method":"stroke"},{"method":"moveTo","args":[801,459]},{"method":"lineTo","args":[802,457]},{"method":"stroke"},{"method":"moveTo","args":[802,457]},{"method":"lineTo","args":[803,455]},{"method":"stroke"},{"method":"moveTo","args":[803,455]},{"method":"lineTo","args":[803,454]},{"method":"stroke"},{"method":"moveTo","args":[803,454]},{"method":"lineTo","args":[803,452]},{"method":"stroke"},{"method":"moveTo","args":[803,452]},{"method":"lineTo","args":[804,449]},{"method":"stroke"},{"method":"moveTo","args":[804,449]},{"method":"lineTo","args":[804,446]},{"method":"stroke"},{"method":"moveTo","args":[804,446]},{"method":"lineTo","args":[805,443]},{"method":"stroke"},{"method":"moveTo","args":[805,443]},{"method":"lineTo","args":[805,439]},{"method":"stroke"},{"method":"moveTo","args":[805,439]},{"method":"lineTo","args":[805,435]},{"method":"stroke"},{"method":"moveTo","args":[805,435]},{"method":"lineTo","args":[806,431]},{"method":"stroke"},{"method":"moveTo","args":[806,431]},{"method":"lineTo","args":[806,429]},{"method":"stroke"},{"method":"moveTo","args":[806,429]},{"method":"lineTo","args":[807,425]},{"method":"stroke"},{"method":"moveTo","args":[807,425]},{"method":"lineTo","args":[807,421]},{"method":"stroke"},{"method":"moveTo","args":[807,421]},{"method":"lineTo","args":[807,419]},{"method":"stroke"},{"method":"moveTo","args":[807,419]},{"method":"lineTo","args":[808,415]},{"method":"stroke"},{"method":"moveTo","args":[808,415]},{"method":"lineTo","args":[808,413]},{"method":"stroke"},{"method":"moveTo","args":[808,413]},{"method":"lineTo","args":[808,410]},{"method":"stroke"},{"method":"moveTo","args":[808,410]},{"method":"lineTo","args":[808,408]},{"method":"stroke"},{"method":"moveTo","args":[808,408]},{"method":"lineTo","args":[808,406]},{"method":"stroke"},{"method":"moveTo","args":[808,406]},{"method":"lineTo","args":[808,404]},{"method":"stroke"},{"method":"moveTo","args":[808,404]},{"method":"lineTo","args":[808,401]},{"method":"stroke"},{"method":"moveTo","args":[808,401]},{"method":"lineTo","args":[808,399]},{"method":"stroke"},{"method":"moveTo","args":[808,399]},{"method":"lineTo","args":[808,397]},{"method":"stroke"},{"method":"moveTo","args":[808,397]},{"method":"lineTo","args":[808,396]},{"method":"stroke"},{"method":"moveTo","args":[808,396]},{"method":"lineTo","args":[808,394]},{"method":"stroke"},{"method":"moveTo","args":[808,394]},{"method":"lineTo","args":[809,392]},{"method":"stroke"},{"method":"moveTo","args":[809,392]},{"method":"lineTo","args":[809,391]},{"method":"stroke"},{"method":"moveTo","args":[809,391]},{"method":"lineTo","args":[809,389]},{"method":"stroke"},{"method":"moveTo","args":[809,389]},{"method":"lineTo","args":[809,387]},{"method":"stroke"},{"method":"moveTo","args":[809,387]},{"method":"lineTo","args":[809,384]},{"method":"stroke"},{"method":"moveTo","args":[809,384]},{"method":"lineTo","args":[809,382]},{"method":"stroke"},{"method":"moveTo","args":[809,382]},{"method":"lineTo","args":[809,380]},{"method":"stroke"},{"method":"moveTo","args":[809,380]},{"method":"lineTo","args":[809,378]},{"method":"stroke"},{"method":"moveTo","args":[809,378]},{"method":"lineTo","args":[809,377]},{"method":"stroke"},{"method":"moveTo","args":[809,377]},{"method":"lineTo","args":[809,375]},{"method":"stroke"},{"method":"moveTo","args":[809,375]},{"method":"lineTo","args":[809,374]},{"method":"stroke"},{"method":"moveTo","args":[809,374]},{"method":"lineTo","args":[809,373]},{"method":"stroke"},{"method":"moveTo","args":[809,373]},{"method":"lineTo","args":[809,372]},{"method":"stroke"},{"method":"moveTo","args":[809,372]},{"method":"lineTo","args":[809,368]},{"method":"stroke"},{"method":"moveTo","args":[809,368]},{"method":"lineTo","args":[809,367]},{"method":"stroke"},{"method":"moveTo","args":[809,367]},{"method":"lineTo","args":[809,366]},{"method":"stroke"},{"method":"moveTo","args":[809,366]},{"method":"lineTo","args":[809,364]},{"method":"stroke"},{"method":"moveTo","args":[809,364]},{"method":"lineTo","args":[809,362]},{"method":"stroke"},{"method":"moveTo","args":[809,362]},{"method":"lineTo","args":[809,361]},{"method":"stroke"},{"method":"moveTo","args":[809,361]},{"method":"lineTo","args":[809,359]},{"method":"stroke"},{"method":"moveTo","args":[809,359]},{"method":"lineTo","args":[809,358]},{"method":"stroke"},{"method":"moveTo","args":[809,358]},{"method":"lineTo","args":[808,356]},{"method":"stroke"},{"method":"moveTo","args":[808,356]},{"method":"lineTo","args":[808,355]},{"method":"stroke"},{"method":"moveTo","args":[808,355]},{"method":"lineTo","args":[808,354]},{"method":"stroke"},{"method":"moveTo","args":[808,354]},{"method":"lineTo","args":[807,353]},{"method":"stroke"},{"method":"moveTo","args":[807,353]},{"method":"lineTo","args":[807,352]},{"method":"stroke"},{"method":"moveTo","args":[807,352]},{"method":"lineTo","args":[806,351]},{"method":"stroke"},{"method":"moveTo","args":[806,351]},{"method":"lineTo","args":[806,350]},{"method":"stroke"},{"method":"moveTo","args":[806,350]},{"method":"lineTo","args":[805,349]},{"method":"stroke"},{"method":"moveTo","args":[805,349]},{"method":"lineTo","args":[805,348]},{"method":"stroke"},{"method":"moveTo","args":[805,348]},{"method":"lineTo","args":[804,347]},{"method":"stroke"},{"method":"moveTo","args":[804,347]},{"method":"lineTo","args":[804,346]},{"method":"stroke"},{"method":"moveTo","args":[804,346]},{"method":"lineTo","args":[803,344]},{"method":"stroke"},{"method":"moveTo","args":[803,344]},{"method":"lineTo","args":[802,343]},{"method":"stroke"},{"method":"moveTo","args":[802,343]},{"method":"lineTo","args":[801,341]},{"method":"stroke"},{"method":"moveTo","args":[801,341]},{"method":"lineTo","args":[801,339]},{"method":"stroke"},{"method":"moveTo","args":[801,339]},{"method":"lineTo","args":[799,337]},{"method":"stroke"},{"method":"moveTo","args":[799,337]},{"method":"lineTo","args":[799,336]},{"method":"stroke"},{"method":"moveTo","args":[799,336]},{"method":"lineTo","args":[798,335]},{"method":"stroke"},{"method":"moveTo","args":[798,335]},{"method":"lineTo","args":[798,334]},{"method":"stroke"},{"method":"moveTo","args":[798,334]},{"method":"lineTo","args":[797,332]},{"method":"stroke"},{"method":"moveTo","args":[797,332]},{"method":"lineTo","args":[796,331]},{"method":"stroke"},{"method":"moveTo","args":[796,331]},{"method":"lineTo","args":[795,329]},{"method":"stroke"},{"method":"moveTo","args":[795,329]},{"method":"lineTo","args":[794,327]},{"method":"stroke"},{"method":"moveTo","args":[794,327]},{"method":"lineTo","args":[794,326]},{"method":"stroke"},{"method":"moveTo","args":[794,326]},{"method":"lineTo","args":[792,324]},{"method":"stroke"},{"method":"moveTo","args":[792,324]},{"method":"lineTo","args":[791,322]},{"method":"stroke"},{"method":"moveTo","args":[791,322]},{"method":"lineTo","args":[790,321]},{"method":"stroke"},{"method":"moveTo","args":[790,321]},{"method":"lineTo","args":[789,319]},{"method":"stroke"},{"method":"moveTo","args":[789,319]},{"method":"lineTo","args":[788,318]},{"method":"stroke"},{"method":"moveTo","args":[788,318]},{"method":"lineTo","args":[787,316]},{"method":"stroke"},{"method":"moveTo","args":[787,316]},{"method":"lineTo","args":[786,315]},{"method":"stroke"},{"method":"moveTo","args":[786,315]},{"method":"lineTo","args":[785,314]},{"method":"stroke"},{"method":"moveTo","args":[785,314]},{"method":"lineTo","args":[784,312]},{"method":"stroke"},{"method":"moveTo","args":[784,312]},{"method":"lineTo","args":[783,311]},{"method":"stroke"},{"method":"moveTo","args":[783,311]},{"method":"lineTo","args":[782,310]},{"method":"stroke"},{"method":"moveTo","args":[782,310]},{"method":"lineTo","args":[781,309]},{"method":"stroke"},{"method":"moveTo","args":[781,309]},{"method":"lineTo","args":[780,308]},{"method":"stroke"},{"method":"moveTo","args":[780,308]},{"method":"lineTo","args":[779,307]},{"method":"stroke"},{"method":"moveTo","args":[779,307]},{"method":"lineTo","args":[778,307]},{"method":"stroke"},{"method":"moveTo","args":[778,307]},{"method":"lineTo","args":[777,306]},{"method":"stroke"},{"method":"moveTo","args":[777,306]},{"method":"lineTo","args":[776,305]},{"method":"stroke"},{"method":"moveTo","args":[776,305]},{"method":"lineTo","args":[775,304]},{"method":"stroke"},{"method":"moveTo","args":[775,304]},{"method":"lineTo","args":[774,303]},{"method":"stroke"},{"method":"moveTo","args":[774,303]},{"method":"lineTo","args":[773,302]},{"method":"stroke"},{"method":"moveTo","args":[773,302]},{"method":"lineTo","args":[773,302]},{"method":"stroke"},{"method":"moveTo","args":[773,302]},{"method":"lineTo","args":[772,300]},{"method":"stroke"},{"method":"moveTo","args":[772,300]},{"method":"lineTo","args":[771,300]},{"method":"stroke"},{"method":"moveTo","args":[771,300]},{"method":"lineTo","args":[770,299]},{"method":"stroke"},{"method":"moveTo","args":[770,299]},{"method":"lineTo","args":[770,299]},{"method":"stroke"},{"method":"moveTo","args":[770,299]},{"method":"lineTo","args":[769,298]},{"method":"stroke"},{"method":"moveTo","args":[769,298]},{"method":"lineTo","args":[769,298]},{"method":"stroke"},{"method":"moveTo","args":[769,298]},{"method":"lineTo","args":[768,298]},{"method":"stroke"},{"method":"moveTo","args":[768,298]},{"method":"lineTo","args":[768,297]},{"method":"stroke"},{"method":"moveTo","args":[768,297]},{"method":"lineTo","args":[767,297]},{"method":"stroke"},{"method":"moveTo","args":[767,297]},{"method":"lineTo","args":[766,296]},{"method":"stroke"},{"method":"moveTo","args":[766,296]},{"method":"lineTo","args":[766,295]},{"method":"stroke"},{"method":"moveTo","args":[766,295]},{"method":"lineTo","args":[765,295]},{"method":"stroke"},{"method":"moveTo","args":[765,295]},{"method":"lineTo","args":[763,293]},{"method":"stroke"},{"method":"moveTo","args":[763,293]},{"method":"lineTo","args":[762,292]},{"method":"stroke"},{"method":"moveTo","args":[762,292]},{"method":"lineTo","args":[761,292]},{"method":"stroke"},{"method":"moveTo","args":[761,292]},{"method":"lineTo","args":[760,291]},{"method":"stroke"},{"method":"moveTo","args":[760,291]},{"method":"lineTo","args":[759,289]},{"method":"stroke"},{"method":"moveTo","args":[759,289]},{"method":"lineTo","args":[758,288]},{"method":"stroke"},{"method":"moveTo","args":[758,288]},{"method":"lineTo","args":[757,287]},{"method":"stroke"},{"method":"moveTo","args":[757,287]},{"method":"lineTo","args":[755,286]},{"method":"stroke"},{"method":"moveTo","args":[755,286]},{"method":"lineTo","args":[754,285]},{"method":"stroke"},{"method":"moveTo","args":[754,285]},{"method":"lineTo","args":[752,284]},{"method":"stroke"},{"method":"moveTo","args":[752,284]},{"method":"lineTo","args":[751,283]},{"method":"stroke"},{"method":"moveTo","args":[751,283]},{"method":"lineTo","args":[750,282]},{"method":"stroke"},{"method":"moveTo","args":[750,282]},{"method":"lineTo","args":[748,281]},{"method":"stroke"},{"method":"moveTo","args":[748,281]},{"method":"lineTo","args":[747,281]},{"method":"stroke"},{"method":"moveTo","args":[747,281]},{"method":"lineTo","args":[745,279]},{"method":"stroke"},{"method":"moveTo","args":[745,279]},{"method":"lineTo","args":[743,278]},{"method":"stroke"},{"method":"moveTo","args":[743,278]},{"method":"lineTo","args":[742,277]},{"method":"stroke"},{"method":"moveTo","args":[742,277]},{"method":"lineTo","args":[740,276]},{"method":"stroke"},{"method":"moveTo","args":[740,276]},{"method":"lineTo","args":[738,275]},{"method":"stroke"},{"method":"moveTo","args":[738,275]},{"method":"lineTo","args":[737,274]},{"method":"stroke"},{"method":"moveTo","args":[737,274]},{"method":"lineTo","args":[735,273]},{"method":"stroke"},{"method":"moveTo","args":[735,273]},{"method":"lineTo","args":[733,272]},{"method":"stroke"},{"method":"moveTo","args":[733,272]},{"method":"lineTo","args":[731,271]},{"method":"stroke"},{"method":"moveTo","args":[731,271]},{"method":"lineTo","args":[729,270]},{"method":"stroke"},{"method":"moveTo","args":[729,270]},{"method":"lineTo","args":[727,269]},{"method":"stroke"},{"method":"moveTo","args":[727,269]},{"method":"lineTo","args":[725,268]},{"method":"stroke"},{"method":"moveTo","args":[725,268]},{"method":"lineTo","args":[723,266]},{"method":"stroke"},{"method":"moveTo","args":[723,266]},{"method":"lineTo","args":[721,266]},{"method":"stroke"},{"method":"moveTo","args":[721,266]},{"method":"lineTo","args":[720,264]},{"method":"stroke"},{"method":"moveTo","args":[720,264]},{"method":"lineTo","args":[718,263]},{"method":"stroke"},{"method":"moveTo","args":[718,263]},{"method":"lineTo","args":[717,263]},{"method":"stroke"},{"method":"moveTo","args":[717,263]},{"method":"lineTo","args":[715,262]},{"method":"stroke"},{"method":"moveTo","args":[715,262]},{"method":"lineTo","args":[714,261]},{"method":"stroke"},{"method":"moveTo","args":[714,261]},{"method":"lineTo","args":[712,260]},{"method":"stroke"},{"method":"moveTo","args":[712,260]},{"method":"lineTo","args":[711,260]},{"method":"stroke"},{"method":"moveTo","args":[711,260]},{"method":"lineTo","args":[709,260]},{"method":"stroke"},{"method":"moveTo","args":[709,260]},{"method":"lineTo","args":[708,259]},{"method":"stroke"},{"method":"moveTo","args":[708,259]},{"method":"lineTo","args":[707,258]},{"method":"stroke"},{"method":"moveTo","args":[707,258]},{"method":"lineTo","args":[706,258]},{"method":"stroke"},{"method":"moveTo","args":[706,258]},{"method":"lineTo","args":[705,257]},{"method":"stroke"},{"method":"moveTo","args":[705,257]},{"method":"lineTo","args":[703,257]},{"method":"stroke"},{"method":"moveTo","args":[703,257]},{"method":"lineTo","args":[702,256]},{"method":"stroke"},{"method":"moveTo","args":[702,256]},{"method":"lineTo","args":[701,256]},{"method":"stroke"},{"method":"moveTo","args":[701,256]},{"method":"lineTo","args":[699,255]},{"method":"stroke"},{"method":"moveTo","args":[699,255]},{"method":"lineTo","args":[697,255]},{"method":"stroke"},{"method":"moveTo","args":[697,255]},{"method":"lineTo","args":[696,254]},{"method":"stroke"},{"method":"moveTo","args":[696,254]},{"method":"lineTo","args":[694,254]},{"method":"stroke"},{"method":"moveTo","args":[694,254]},{"method":"lineTo","args":[692,253]},{"method":"stroke"},{"method":"moveTo","args":[692,253]},{"method":"lineTo","args":[690,252]},{"method":"stroke"},{"method":"moveTo","args":[690,252]},{"method":"lineTo","args":[688,251]},{"method":"stroke"},{"method":"moveTo","args":[688,251]},{"method":"lineTo","args":[685,250]},{"method":"stroke"},{"method":"moveTo","args":[685,250]},{"method":"lineTo","args":[684,249]},{"method":"stroke"},{"method":"moveTo","args":[684,249]},{"method":"lineTo","args":[681,249]},{"method":"stroke"},{"method":"moveTo","args":[681,249]},{"method":"lineTo","args":[679,247]},{"method":"stroke"},{"method":"moveTo","args":[679,247]},{"method":"lineTo","args":[677,247]},{"method":"stroke"},{"method":"moveTo","args":[677,247]},{"method":"lineTo","args":[675,245]},{"method":"stroke"},{"method":"moveTo","args":[675,245]},{"method":"lineTo","args":[672,245]},{"method":"stroke"},{"method":"moveTo","args":[672,245]},{"method":"lineTo","args":[671,243]},{"method":"stroke"},{"method":"moveTo","args":[671,243]},{"method":"lineTo","args":[669,243]},{"method":"stroke"},{"method":"moveTo","args":[669,243]},{"method":"lineTo","args":[667,242]},{"method":"stroke"},{"method":"moveTo","args":[667,242]},{"method":"lineTo","args":[665,242]},{"method":"stroke"},{"method":"moveTo","args":[665,242]},{"method":"lineTo","args":[664,241]},{"method":"stroke"},{"method":"moveTo","args":[664,241]},{"method":"lineTo","args":[662,240]},{"method":"stroke"},{"method":"moveTo","args":[662,240]},{"method":"lineTo","args":[660,240]},{"method":"stroke"},{"method":"moveTo","args":[660,240]},{"method":"lineTo","args":[656,239]},{"method":"stroke"},{"method":"moveTo","args":[656,239]},{"method":"lineTo","args":[655,238]},{"method":"stroke"},{"method":"moveTo","args":[655,238]},{"method":"lineTo","args":[653,237]},{"method":"stroke"},{"method":"moveTo","args":[653,237]},{"method":"lineTo","args":[652,236]},{"method":"stroke"},{"method":"moveTo","args":[652,236]},{"method":"lineTo","args":[650,236]},{"method":"stroke"},{"method":"moveTo","args":[650,236]},{"method":"lineTo","args":[648,235]},{"method":"stroke"},{"method":"moveTo","args":[648,235]},{"method":"lineTo","args":[647,234]},{"method":"stroke"},{"method":"moveTo","args":[647,234]},{"method":"lineTo","args":[645,233]},{"method":"stroke"},{"method":"moveTo","args":[645,233]},{"method":"lineTo","args":[643,232]},{"method":"stroke"},{"method":"moveTo","args":[643,232]},{"method":"lineTo","args":[641,231]},{"method":"stroke"},{"method":"moveTo","args":[641,231]},{"method":"lineTo","args":[639,231]},{"method":"stroke"},{"method":"moveTo","args":[639,231]},{"method":"lineTo","args":[636,230]},{"method":"stroke"},{"method":"moveTo","args":[636,230]},{"method":"lineTo","args":[634,229]},{"method":"stroke"},{"method":"moveTo","args":[634,229]},{"method":"lineTo","args":[632,228]},{"method":"stroke"},{"method":"moveTo","args":[632,228]},{"method":"lineTo","args":[629,227]},{"method":"stroke"},{"method":"moveTo","args":[629,227]},{"method":"lineTo","args":[626,227]},{"method":"stroke"},{"method":"moveTo","args":[626,227]},{"method":"lineTo","args":[623,226]},{"method":"stroke"},{"method":"moveTo","args":[623,226]},{"method":"lineTo","args":[622,225]},{"method":"stroke"},{"method":"moveTo","args":[622,225]},{"method":"lineTo","args":[619,225]},{"method":"stroke"},{"method":"moveTo","args":[619,225]},{"method":"lineTo","args":[616,224]},{"method":"stroke"},{"method":"moveTo","args":[616,224]},{"method":"lineTo","args":[613,223]},{"method":"stroke"},{"method":"moveTo","args":[613,223]},{"method":"lineTo","args":[611,223]},{"method":"stroke"},{"method":"moveTo","args":[611,223]},{"method":"lineTo","args":[609,223]},{"method":"stroke"},{"method":"moveTo","args":[609,223]},{"method":"lineTo","args":[607,222]},{"method":"stroke"},{"method":"moveTo","args":[607,222]},{"method":"lineTo","args":[605,221]},{"method":"stroke"},{"method":"moveTo","args":[605,221]},{"method":"lineTo","args":[603,221]},{"method":"stroke"},{"method":"moveTo","args":[603,221]},{"method":"lineTo","args":[601,221]},{"method":"stroke"},{"method":"moveTo","args":[601,221]},{"method":"lineTo","args":[599,221]},{"method":"stroke"},{"method":"moveTo","args":[599,221]},{"method":"lineTo","args":[597,220]},{"method":"stroke"},{"method":"moveTo","args":[597,220]},{"method":"lineTo","args":[595,220]},{"method":"stroke"},{"method":"moveTo","args":[595,220]},{"method":"lineTo","args":[593,219]},{"method":"stroke"},{"method":"moveTo","args":[593,219]},{"method":"lineTo","args":[591,219]},{"method":"stroke"},{"method":"moveTo","args":[591,219]},{"method":"lineTo","args":[589,219]},{"method":"stroke"},{"method":"moveTo","args":[589,219]},{"method":"lineTo","args":[587,219]},{"method":"stroke"},{"method":"moveTo","args":[587,219]},{"method":"lineTo","args":[586,219]},{"method":"stroke"},{"method":"moveTo","args":[586,219]},{"method":"lineTo","args":[584,218]},{"method":"stroke"},{"method":"moveTo","args":[584,218]},{"method":"lineTo","args":[583,218]},{"method":"stroke"},{"method":"moveTo","args":[583,218]},{"method":"lineTo","args":[581,218]},{"method":"stroke"},{"method":"moveTo","args":[581,218]},{"method":"lineTo","args":[580,218]},{"method":"stroke"},{"method":"moveTo","args":[580,218]},{"method":"lineTo","args":[578,218]},{"method":"stroke"},{"method":"moveTo","args":[578,218]},{"method":"lineTo","args":[577,218]},{"method":"stroke"},{"method":"moveTo","args":[577,218]},{"method":"lineTo","args":[575,218]},{"method":"stroke"},{"method":"moveTo","args":[575,218]},{"method":"lineTo","args":[573,218]},{"method":"stroke"},{"method":"moveTo","args":[573,218]},{"method":"lineTo","args":[572,218]},{"method":"stroke"},{"method":"moveTo","args":[572,218]},{"method":"lineTo","args":[570,218]},{"method":"stroke"},{"method":"moveTo","args":[570,218]},{"method":"lineTo","args":[568,218]},{"method":"stroke"},{"method":"moveTo","args":[568,218]},{"method":"lineTo","args":[566,218]},{"method":"stroke"},{"method":"moveTo","args":[566,218]},{"method":"lineTo","args":[564,218]},{"method":"stroke"},{"method":"moveTo","args":[564,218]},{"method":"lineTo","args":[562,218]},{"method":"stroke"},{"method":"moveTo","args":[562,218]},{"method":"lineTo","args":[560,218]},{"method":"stroke"},{"method":"moveTo","args":[560,218]},{"method":"lineTo","args":[557,218]},{"method":"stroke"},{"method":"moveTo","args":[557,218]},{"method":"lineTo","args":[555,218]},{"method":"stroke"},{"method":"moveTo","args":[555,218]},{"method":"lineTo","args":[553,218]},{"method":"stroke"},{"method":"moveTo","args":[553,218]},{"method":"lineTo","args":[551,217]},{"method":"stroke"},{"method":"moveTo","args":[551,217]},{"method":"lineTo","args":[548,217]},{"method":"stroke"},{"method":"moveTo","args":[548,217]},{"method":"lineTo","args":[546,217]},{"method":"stroke"},{"method":"moveTo","args":[546,217]},{"method":"lineTo","args":[543,217]},{"method":"stroke"},{"method":"moveTo","args":[543,217]},{"method":"lineTo","args":[541,217]},{"method":"stroke"},{"method":"moveTo","args":[541,217]},{"method":"lineTo","args":[538,217]},{"method":"stroke"},{"method":"moveTo","args":[538,217]},{"method":"lineTo","args":[536,217]},{"method":"stroke"},{"method":"moveTo","args":[536,217]},{"method":"lineTo","args":[533,217]},{"method":"stroke"},{"method":"moveTo","args":[533,217]},{"method":"lineTo","args":[531,217]},{"method":"stroke"},{"method":"moveTo","args":[531,217]},{"method":"lineTo","args":[529,217]},{"method":"stroke"},{"method":"moveTo","args":[529,217]},{"method":"lineTo","args":[527,217]},{"method":"stroke"},{"method":"moveTo","args":[527,217]},{"method":"lineTo","args":[525,217]},{"method":"stroke"},{"method":"moveTo","args":[525,217]},{"method":"lineTo","args":[522,217]},{"method":"stroke"},{"method":"moveTo","args":[522,217]},{"method":"lineTo","args":[520,217]},{"method":"stroke"},{"method":"moveTo","args":[520,217]},{"method":"lineTo","args":[518,217]},{"method":"stroke"},{"method":"moveTo","args":[518,217]},{"method":"lineTo","args":[516,217]},{"method":"stroke"},{"method":"moveTo","args":[516,217]},{"method":"lineTo","args":[514,217]},{"method":"stroke"},{"method":"moveTo","args":[514,217]},{"method":"lineTo","args":[512,217]},{"method":"stroke"},{"method":"moveTo","args":[512,217]},{"method":"lineTo","args":[511,217]},{"method":"stroke"},{"method":"moveTo","args":[511,217]},{"method":"lineTo","args":[509,217]},{"method":"stroke"},{"method":"moveTo","args":[509,217]},{"method":"lineTo","args":[508,217]},{"method":"stroke"},{"method":"moveTo","args":[508,217]},{"method":"lineTo","args":[507,217]},{"method":"stroke"},{"method":"moveTo","args":[507,217]},{"method":"lineTo","args":[506,217]},{"method":"stroke"},{"method":"moveTo","args":[506,217]},{"method":"lineTo","args":[504,217]},{"method":"stroke"},{"method":"moveTo","args":[504,217]},{"method":"lineTo","args":[503,217]},{"method":"stroke"},{"method":"moveTo","args":[503,217]},{"method":"lineTo","args":[502,217]},{"method":"stroke"},{"method":"moveTo","args":[502,217]},{"method":"lineTo","args":[502,217]},{"method":"stroke"},{"method":"moveTo","args":[502,217]},{"method":"lineTo","args":[500,217]},{"method":"stroke"},{"method":"moveTo","args":[500,217]},{"method":"lineTo","args":[499,217]},{"method":"stroke"},{"method":"moveTo","args":[499,217]},{"method":"lineTo","args":[498,217]},{"method":"stroke"},{"method":"moveTo","args":[498,217]},{"method":"lineTo","args":[497,217]},{"method":"stroke"},{"method":"moveTo","args":[497,217]},{"method":"lineTo","args":[496,217]},{"method":"stroke"},{"method":"moveTo","args":[496,217]},{"method":"lineTo","args":[495,217]},{"method":"stroke"},{"method":"moveTo","args":[495,217]},{"method":"lineTo","args":[494,217]},{"method":"stroke"},{"method":"moveTo","args":[494,217]},{"method":"lineTo","args":[493,217]},{"method":"stroke"},{"method":"moveTo","args":[493,217]},{"method":"lineTo","args":[491,217]},{"method":"stroke"},{"method":"moveTo","args":[491,217]},{"method":"lineTo","args":[490,217]},{"method":"stroke"},{"method":"moveTo","args":[490,217]},{"method":"lineTo","args":[489,217]},{"method":"stroke"},{"method":"moveTo","args":[489,217]},{"method":"lineTo","args":[487,217]},{"method":"stroke"},{"method":"moveTo","args":[487,217]},{"method":"lineTo","args":[486,217]},{"method":"stroke"},{"method":"moveTo","args":[486,217]},{"method":"lineTo","args":[485,217]},{"method":"stroke"},{"method":"moveTo","args":[485,217]},{"method":"lineTo","args":[484,217]},{"method":"stroke"},{"method":"moveTo","args":[484,217]},{"method":"lineTo","args":[482,217]},{"method":"stroke"},{"method":"moveTo","args":[482,217]},{"method":"lineTo","args":[481,218]},{"method":"stroke"},{"method":"moveTo","args":[481,218]},{"method":"lineTo","args":[480,218]},{"method":"stroke"},{"method":"moveTo","args":[480,218]},{"method":"lineTo","args":[479,218]},{"method":"stroke"},{"method":"moveTo","args":[479,218]},{"method":"lineTo","args":[477,219]},{"method":"stroke"},{"method":"moveTo","args":[477,219]},{"method":"lineTo","args":[476,219]},{"method":"stroke"},{"method":"moveTo","args":[476,219]},{"method":"lineTo","args":[474,219]},{"method":"stroke"},{"method":"moveTo","args":[474,219]},{"method":"lineTo","args":[473,220]},{"method":"stroke"},{"method":"moveTo","args":[473,220]},{"method":"lineTo","args":[472,220]},{"method":"stroke"},{"method":"moveTo","args":[472,220]},{"method":"lineTo","args":[470,221]},{"method":"stroke"},{"method":"moveTo","args":[470,221]},{"method":"lineTo","args":[469,221]},{"method":"stroke"},{"method":"moveTo","args":[469,221]},{"method":"lineTo","args":[468,222]},{"method":"stroke"},{"method":"moveTo","args":[468,222]},{"method":"lineTo","args":[466,222]},{"method":"stroke"},{"method":"moveTo","args":[466,222]},{"method":"lineTo","args":[465,222]},{"method":"stroke"},{"method":"moveTo","args":[465,222]},{"method":"lineTo","args":[463,223]},{"method":"stroke"},{"method":"moveTo","args":[463,223]},{"method":"lineTo","args":[462,223]},{"method":"stroke"},{"method":"moveTo","args":[462,223]},{"method":"lineTo","args":[461,223]},{"method":"stroke"},{"method":"moveTo","args":[461,223]},{"method":"lineTo","args":[460,223]},{"method":"stroke"},{"method":"moveTo","args":[460,223]},{"method":"lineTo","args":[459,224]},{"method":"stroke"},{"method":"moveTo","args":[459,224]},{"method":"lineTo","args":[458,224]},{"method":"stroke"},{"method":"moveTo","args":[458,224]},{"method":"lineTo","args":[457,225]},{"method":"stroke"},{"method":"moveTo","args":[457,225]},{"method":"lineTo","args":[456,225]},{"method":"stroke"},{"method":"moveTo","args":[456,225]},{"method":"lineTo","args":[455,225]},{"method":"stroke"},{"method":"moveTo","args":[455,225]},{"method":"lineTo","args":[454,225]},{"method":"stroke"},{"method":"moveTo","args":[454,225]},{"method":"lineTo","args":[453,226]},{"method":"stroke"},{"method":"moveTo","args":[453,226]},{"method":"lineTo","args":[452,226]},{"method":"stroke"},{"method":"moveTo","args":[452,226]},{"method":"lineTo","args":[451,227]},{"method":"stroke"},{"method":"moveTo","args":[451,227]},{"method":"lineTo","args":[450,227]},{"method":"stroke"},{"method":"moveTo","args":[450,227]},{"method":"lineTo","args":[449,227]},{"method":"stroke"},{"method":"moveTo","args":[449,227]},{"method":"lineTo","args":[448,228]},{"method":"stroke"},{"method":"moveTo","args":[448,228]},{"method":"lineTo","args":[447,228]},{"method":"stroke"},{"method":"moveTo","args":[447,228]},{"method":"lineTo","args":[446,228]},{"method":"stroke"},{"method":"moveTo","args":[446,228]},{"method":"lineTo","args":[445,228]},{"method":"stroke"},{"method":"moveTo","args":[445,228]},{"method":"lineTo","args":[444,229]},{"method":"stroke"},{"method":"moveTo","args":[444,229]},{"method":"lineTo","args":[442,229]},{"method":"stroke"},{"method":"moveTo","args":[442,229]},{"method":"lineTo","args":[442,229]},{"method":"stroke"},{"method":"moveTo","args":[442,229]},{"method":"lineTo","args":[440,229]},{"method":"stroke"},{"method":"moveTo","args":[440,229]},{"method":"lineTo","args":[440,229]},{"method":"stroke"},{"method":"moveTo","args":[440,229]},{"method":"lineTo","args":[439,229]},{"method":"stroke"},{"method":"moveTo","args":[439,229]},{"method":"lineTo","args":[439,229]},{"method":"stroke"},{"method":"moveTo","args":[439,229]},{"method":"lineTo","args":[439,230]},{"method":"stroke"},{"method":"moveTo","args":[439,230]},{"method":"lineTo","args":[438,230]},{"method":"stroke"},{"method":"moveTo","args":[438,230]},{"method":"lineTo","args":[438,230]},{"method":"stroke"},{"method":"moveTo","args":[438,230]},{"method":"lineTo","args":[437,231]},{"method":"stroke"},{"method":"moveTo","args":[437,231]},{"method":"lineTo","args":[436,231]},{"method":"stroke"},{"method":"moveTo","args":[436,231]},{"method":"lineTo","args":[435,231]},{"method":"stroke"},{"method":"moveTo","args":[435,231]},{"method":"lineTo","args":[435,232]},{"method":"stroke"},{"method":"moveTo","args":[435,232]},{"method":"lineTo","args":[433,232]},{"method":"stroke"},{"method":"moveTo","args":[433,232]},{"method":"lineTo","args":[430,234]},{"method":"stroke"},{"method":"closePath"}]}';
sketcher1.serializer.load(contentSk1);
canvasContainer1.style.pointerEvents = "none";
myCanvas1.style.pointerEvents = "none";
canvasContainer1.style.display = 'none';

videoCanvases.push(canvasContainer1);

annot1 = {
"id": 1,
"time": 9,
"interval": 1,
"color": "#1B9E77",
"active": 0
}
videoAnnotations.push(annot1);

// Create a blank div where we are going to put the canvas into.
var canvasContainer2 = document.createElement('div');
// Add the div into the document
 document.getElementById('my-video').appendChild(canvasContainer2);
 canvasContainer2.classList.add("video-canvas");
 canvasContainer2.id = 2;
 canvasContainer2.setAttribute("startInterval", 30);
 canvasContainer2.setAttribute("intervalTime", 1);
 canvasContainer2.style.position="absolute";
 // it will have the dimensions of the video frame
 canvasContainer2.style.left="0px";
 canvasContainer2.style.top="0px";
 canvasContainer2.style.width=canvasContainer2.parentElement.clientWidth+"px";
 canvasContainer2.style.height=canvasContainer2.parentElement.clientHeight-30+"px";
 // Set to high index so that this is always above everything else
 // (might need to be increased if you have other element at higher index)
 canvasContainer2.style.zIndex="1000";
 videoCanvasContainer = document.getElementById('col-video');

 // Now we create the canvas
 myCanvas2 = document.createElement('canvas');
 myCanvas2.style.width = canvasContainer2.scrollWidth+"px";
 myCanvas2.style.height = canvasContainer2.scrollHeight+"px";
 // You must set this otherwise the canvas will be streethed to fit the container
 myCanvas2.width=canvasContainer2.scrollWidth;
 myCanvas2.height=canvasContainer2.scrollHeight;
 myCanvas2.style.overflow = 'visible';
 myCanvas2.style.position = 'absolute';

 var context2=myCanvas2.getContext('2d');
 context2.fillStyle = 'rgba(192,192,192,0.1)';
 context2.fillRect(0,0, myCanvas2.width, myCanvas2.height);

 // Add int into the container
 canvasContainer2.appendChild(myCanvas2);

 sketcher2 = new Sketchable(myCanvas2, {
   graphics: {
     lineWidth: 8,
     firstPointSize: 0,
     strokeStyle: "#D95F02"
   }});

var contentSk2 = '{"options":{"interactive":true,"mouseupMovements":false,"relTimestamps":false,"multitouch":true,"cssCursors":true,"filterCoords":false,"events":{},"graphics":{"firstPointSize":0,"lineWidth":8,"strokeStyle":"#1B9E77","fillStyle":"#F0F","lineCap":"round","lineJoin":"round","miterLimit":10}},"strokes":[[[469,215,1630371669516,1,0],[468,215,1630371669660,1,0],[466,215,1630371669668,1,0],[462,215,1630371669677,1,0],[459,215,1630371669683,1,0],[453,215,1630371669695,1,0],[447,215,1630371669700,1,0],[441,215,1630371669707,1,0],[435,215,1630371669716,1,0],[427,215,1630371669723,1,0],[420,215,1630371669733,1,0],[412,217,1630371669739,1,0],[404,218,1630371669748,1,0],[397,220,1630371669756,1,0],[391,222,1630371669766,1,0],[385,225,1630371669772,1,0],[379,227,1630371669782,1,0],[377,228,1630371669788,1,0],[371,231,1630371669799,1,0],[367,233,1630371669805,1,0],[362,236,1630371669815,1,0],[358,239,1630371669821,1,0],[355,242,1630371669833,1,0],[351,244,1630371669837,1,0],[347,246,1630371669849,1,0],[344,248,1630371669854,1,0],[340,251,1630371669866,1,0],[337,254,1630371669870,1,0],[333,256,1630371669883,1,0],[330,258,1630371669886,1,0],[327,260,1630371669898,1,0],[324,263,1630371669901,1,0],[321,265,1630371669914,1,0],[318,268,1630371669918,1,0],[315,271,1630371669930,1,0],[312,274,1630371669934,1,0],[309,277,1630371669942,1,0],[305,282,1630371669950,1,0],[302,286,1630371669958,1,0],[300,289,1630371669966,1,0],[298,293,1630371669974,1,0],[296,297,1630371669982,1,0],[292,301,1630371669990,1,0],[291,306,1630371669999,1,0],[289,310,1630371670007,1,0],[287,313,1630371670015,1,0],[286,317,1630371670023,1,0],[285,320,1630371670031,1,0],[284,324,1630371670040,1,0],[284,326,1630371670049,1,0],[284,329,1630371670056,1,0],[283,332,1630371670064,1,0],[283,335,1630371670072,1,0],[283,337,1630371670083,1,0],[283,339,1630371670088,1,0],[283,342,1630371670099,1,0],[283,344,1630371670105,1,0],[283,346,1630371670116,1,0],[283,348,1630371670120,1,0],[283,350,1630371670134,1,0],[283,353,1630371670136,1,0],[283,355,1630371670151,1,0],[284,357,1630371670153,1,0],[286,359,1630371670165,1,0],[287,361,1630371670168,1,0],[288,363,1630371670182,1,0],[290,365,1630371670184,1,0],[291,367,1630371670197,1,0],[293,369,1630371670201,1,0],[295,371,1630371670209,1,0],[298,373,1630371670217,1,0],[299,375,1630371670225,1,0],[304,378,1630371670250,1,0],[307,379,1630371670252,1,0],[310,381,1630371670257,1,0],[312,382,1630371670269,1,0],[316,384,1630371670273,1,0],[320,385,1630371670283,1,0],[325,387,1630371670290,1,0],[328,388,1630371670299,1,0],[333,389,1630371670306,1,0],[342,392,1630371670316,1,0],[346,393,1630371670322,1,0],[352,395,1630371670333,1,0],[356,396,1630371670338,1,0],[363,398,1630371670350,1,0],[367,400,1630371670354,1,0],[372,402,1630371670366,1,0],[378,403,1630371670371,1,0],[382,404,1630371670383,1,0],[388,406,1630371670387,1,0],[396,408,1630371670399,1,0],[399,408,1630371670403,1,0],[404,409,1630371670417,1,0],[409,410,1630371670420,1,0],[414,411,1630371670432,1,0],[420,412,1630371670436,1,0],[424,412,1630371670448,1,0],[432,413,1630371670452,1,0],[434,414,1630371670464,1,0],[439,415,1630371670467,1,0],[445,416,1630371670476,1,0],[452,416,1630371670484,1,0],[455,417,1630371670492,1,0],[461,417,1630371670500,1,0],[466,418,1630371670508,1,0],[474,419,1630371670517,1,0],[477,419,1630371670524,1,0],[482,419,1630371670533,1,0],[487,420,1630371670540,1,0],[491,420,1630371670549,1,0],[496,420,1630371670557,1,0],[501,420,1630371670565,1,0],[506,420,1630371670573,1,0],[510,420,1630371670583,1,0],[514,420,1630371670589,1,0],[519,420,1630371670599,1,0],[526,420,1630371670606,1,0],[528,420,1630371670616,1,0],[533,420,1630371670621,1,0],[538,420,1630371670633,1,0],[542,420,1630371670638,1,0],[547,420,1630371670650,1,0],[551,420,1630371670654,1,0],[555,420,1630371670667,1,0],[559,420,1630371670671,1,0],[563,420,1630371670683,1,0],[568,420,1630371670686,1,0],[571,420,1630371670699,1,0],[575,419,1630371670703,1,0],[579,419,1630371670714,1,0],[583,418,1630371670718,1,0],[587,417,1630371670731,1,0],[592,416,1630371670734,1,0],[597,415,1630371670743,1,0],[601,414,1630371670751,1,0],[605,414,1630371670759,1,0],[610,413,1630371670767,1,0],[615,413,1630371670775,1,0],[619,412,1630371670783,1,0],[624,411,1630371670791,1,0],[629,411,1630371670799,1,0],[634,410,1630371670807,1,0],[638,410,1630371670816,1,0],[643,410,1630371670823,1,0],[648,409,1630371670833,1,0],[653,408,1630371670839,1,0],[656,408,1630371670848,1,0],[661,407,1630371670857,1,0],[666,406,1630371670866,1,0],[671,406,1630371670872,1,0],[674,405,1630371670884,1,0],[678,404,1630371670888,1,0],[682,403,1630371670899,1,0],[686,403,1630371670905,1,0],[689,402,1630371670917,1,0],[693,401,1630371670921,1,0],[696,400,1630371670933,1,0],[699,400,1630371670937,1,0],[702,398,1630371670950,1,0],[706,398,1630371670953,1,0],[708,397,1630371670967,1,0],[712,395,1630371670969,1,0],[715,395,1630371670982,1,0],[718,394,1630371670985,1,0],[721,393,1630371670997,1,0],[724,392,1630371671001,1,0],[728,390,1630371671010,1,0],[730,390,1630371671017,1,0],[733,388,1630371671026,1,0],[737,388,1630371671034,1,0],[740,386,1630371671042,1,0],[743,385,1630371671051,1,0],[746,384,1630371671058,1,0],[749,382,1630371671066,1,0],[752,381,1630371671074,1,0],[754,380,1630371671083,1,0],[757,379,1630371671090,1,0],[760,378,1630371671099,1,0],[762,377,1630371671107,1,0],[766,375,1630371671116,1,0],[769,374,1630371671122,1,0],[772,372,1630371671133,1,0],[775,370,1630371671139,1,0],[777,369,1630371671150,1,0],[780,368,1630371671155,1,0],[783,366,1630371671167,1,0],[786,364,1630371671172,1,0],[788,363,1630371671183,1,0],[791,361,1630371671188,1,0],[794,359,1630371671201,1,0],[797,358,1630371671204,1,0],[800,355,1630371671217,1,0],[802,353,1630371671220,1,0],[805,352,1630371671233,1,0],[807,350,1630371671237,1,0],[810,348,1630371671251,1,0],[811,346,1630371671253,1,0],[813,345,1630371671265,1,0],[814,343,1630371671268,1,0],[816,341,1630371671291,1,0],[817,340,1630371671293,1,0],[818,338,1630371671300,1,0],[819,337,1630371671302,1,0],[820,336,1630371671309,1,0],[821,335,1630371671318,1,0],[822,334,1630371671325,1,0],[823,333,1630371671333,1,0],[823,332,1630371671341,1,0],[825,330,1630371671349,1,0],[825,330,1630371671357,1,0],[826,329,1630371671366,1,0],[827,328,1630371671373,1,0],[828,327,1630371671383,1,0],[828,326,1630371671392,1,0],[830,325,1630371671399,1,0],[830,324,1630371671407,1,0],[831,323,1630371671415,1,0],[832,322,1630371671422,1,0],[833,321,1630371671433,1,0],[834,320,1630371671439,1,0],[835,318,1630371671450,1,0],[836,317,1630371671455,1,0],[836,316,1630371671466,1,0],[837,315,1630371671471,1,0],[838,314,1630371671483,1,0],[838,313,1630371671487,1,0],[839,312,1630371671507,1,0],[839,310,1630371671508,1,0],[840,308,1630371671517,1,0],[841,307,1630371671520,1,0],[841,304,1630371671531,1,0],[842,301,1630371671535,1,0],[843,298,1630371671548,1,0],[843,295,1630371671551,1,0],[844,292,1630371671560,1,0],[845,288,1630371671567,1,0],[845,286,1630371671576,1,0],[846,284,1630371671584,1,0],[846,282,1630371671592,1,0],[846,280,1630371671600,1,0],[846,278,1630371671608,1,0],[846,276,1630371671616,1,0],[846,273,1630371671624,1,0],[846,272,1630371671633,1,0],[846,269,1630371671640,1,0],[846,266,1630371671650,1,0],[846,263,1630371671656,1,0],[846,261,1630371671666,1,0],[846,259,1630371671673,1,0],[846,255,1630371671683,1,0],[846,252,1630371671689,1,0],[846,250,1630371671699,1,0],[846,246,1630371671706,1,0],[846,242,1630371671716,1,0],[846,239,1630371671722,1,0],[846,236,1630371671734,1,0],[846,233,1630371671738,1,0],[846,229,1630371671751,1,0],[846,227,1630371671754,1,0],[845,224,1630371671768,1,0],[845,221,1630371671771,1,0],[845,219,1630371671783,1,0],[845,217,1630371671786,1,0],[844,215,1630371671800,1,0],[844,213,1630371671803,1,0],[844,212,1630371671815,1,0],[843,210,1630371671818,1,0],[842,209,1630371671832,1,0],[842,208,1630371671835,1,0],[842,208,1630371671843,1,0],[841,207,1630371671850,1,0],[841,207,1630371671859,1,0],[841,207,1630371671867,1,0],[840,207,1630371671875,1,0],[840,206,1630371671883,1,0],[839,206,1630371671891,1,0],[838,205,1630371671900,1,0],[837,204,1630371671907,1,0],[836,203,1630371671916,1,0],[834,201,1630371671925,1,0],[832,201,1630371671933,1,0],[830,200,1630371671940,1,0],[828,198,1630371671949,1,0],[826,197,1630371671956,1,0],[825,196,1630371671967,1,0],[823,195,1630371671972,1,0],[821,195,1630371671984,1,0],[819,193,1630371671988,1,0],[816,193,1630371672001,1,0],[813,192,1630371672005,1,0],[811,191,1630371672017,1,0],[808,189,1630371672021,1,0],[805,188,1630371672034,1,0],[802,187,1630371672037,1,0],[797,185,1630371672050,1,0],[793,184,1630371672053,1,0],[788,182,1630371672066,1,0],[785,182,1630371672069,1,0],[780,181,1630371672083,1,0],[776,179,1630371672086,1,0],[773,179,1630371672098,1,0],[768,178,1630371672101,1,0],[764,178,1630371672109,1,0],[761,177,1630371672118,1,0],[758,176,1630371672125,1,0],[754,176,1630371672133,1,0],[750,176,1630371672142,1,0],[747,175,1630371672150,1,0],[743,175,1630371672158,1,0],[739,174,1630371672167,1,0],[736,174,1630371672174,1,0],[732,174,1630371672183,1,0],[728,174,1630371672190,1,0],[724,174,1630371672199,1,0],[721,174,1630371672206,1,0],[718,174,1630371672217,1,0],[714,174,1630371672223,1,0],[712,174,1630371672233,1,0],[709,174,1630371672240,1,0],[706,174,1630371672251,1,0],[702,174,1630371672255,1,0],[700,174,1630371672269,1,0],[697,174,1630371672272,1,0],[694,174,1630371672284,1,0],[691,174,1630371672288,1,0],[687,174,1630371672302,1,0],[684,174,1630371672304,1,0],[679,174,1630371672316,1,0],[676,174,1630371672320,1,0],[667,175,1630371672344,1,0],[663,176,1630371672352,1,0],[658,177,1630371672353,1,0],[653,177,1630371672368,1,0],[649,178,1630371672370,1,0],[645,179,1630371672376,1,0],[641,180,1630371672384,1,0],[637,180,1630371672392,1,0],[633,181,1630371672401,1,0],[630,182,1630371672409,1,0],[626,183,1630371672417,1,0],[622,184,1630371672425,1,0],[619,186,1630371672433,1,0],[615,186,1630371672441,1,0],[611,187,1630371672450,1,0],[608,189,1630371672458,1,0],[604,189,1630371672467,1,0],[600,190,1630371672473,1,0],[597,191,1630371672483,1,0],[594,191,1630371672490,1,0],[590,192,1630371672499,1,0],[586,193,1630371672506,1,0],[584,194,1630371672516,1,0],[580,194,1630371672522,1,0],[577,195,1630371672534,1,0],[575,196,1630371672538,1,0],[571,196,1630371672551,1,0],[568,198,1630371672555,1,0],[565,199,1630371672567,1,0],[562,199,1630371672571,1,0],[559,200,1630371672585,1,0],[556,201,1630371672588,1,0],[553,201,1630371672598,1,0],[549,202,1630371672603,1,0],[546,203,1630371672616,1,0],[541,204,1630371672619,1,0],[538,205,1630371672627,1,0],[534,206,1630371672635,1,0],[530,207,1630371672644,1,0],[527,208,1630371672651,1,0],[523,209,1630371672659,1,0],[519,210,1630371672668,1,0],[517,211,1630371672675,1,0],[513,212,1630371672684,1,0],[510,213,1630371672692,1,0],[507,214,1630371672700,1,0],[505,215,1630371672708,1,0],[502,216,1630371672716,1,0],[499,216,1630371672724,1,0],[496,217,1630371672733,1,0],[494,218,1630371672740,1,0],[492,218,1630371672750,1,0],[489,218,1630371672757,1,0],[487,218,1630371672766,1,0],[484,218,1630371672773,1,0],[483,218,1630371672783,1,0],[481,218,1630371672790,1,0],[478,218,1630371672800,1,0],[476,218,1630371672806,1,0],[474,218,1630371672817,1,0],[472,218,1630371672822,1,0],[470,218,1630371672834,1,0],[469,218,1630371672839,1,0],[467,218,1630371672849,1,0],[466,218,1630371672854,1,0],[464,218,1630371672867,1,0],[463,218,1630371672869,1,0],[462,218,1630371672882,1,0],[461,218,1630371672886,1,0],[460,218,1630371672899,1,0],[459,218,1630371672902,1,0],[458,218,1630371672910,1,0],[456,217,1630371672918,1,0],[455,217,1630371672927,1,0],[453,217,1630371672934,1,0],[451,216,1630371672942,1,0],[450,216,1630371672951,1,0],[448,215,1630371672959,1,0],[447,215,1630371672967,1,0],[445,215,1630371672975,1,0],[443,214,1630371672983,1,0],[442,214,1630371672991,1,0],[440,214,1630371673000,1,0],[439,214,1630371673008,1,0],[437,214,1630371673017,1,0],[436,214,1630371673023,1,0],[434,214,1630371673033,1,0],[433,214,1630371673041,1,0],[431,214,1630371673050,1,0],[430,214,1630371673056,1,0],[430,214,1630371673066,1,0],[429,214,1630371673072,1,0]]],"actions":[{"property":"fillStyle","value":"#F0F"},{"property":"strokeStyle","value":"#1B9E77"},{"property":"lineWidth","value":8},{"property":"lineCap","value":"round"},{"property":"lineJoin","value":"round"},{"property":"miterLimit","value":10},{"method":"beginPath"},{"method":"moveTo","args":[469,215]},{"method":"lineTo","args":[468,215]},{"method":"stroke"},{"method":"moveTo","args":[468,215]},{"method":"lineTo","args":[466,215]},{"method":"stroke"},{"method":"moveTo","args":[466,215]},{"method":"lineTo","args":[462,215]},{"method":"stroke"},{"method":"moveTo","args":[462,215]},{"method":"lineTo","args":[459,215]},{"method":"stroke"},{"method":"moveTo","args":[459,215]},{"method":"lineTo","args":[453,215]},{"method":"stroke"},{"method":"moveTo","args":[453,215]},{"method":"lineTo","args":[447,215]},{"method":"stroke"},{"method":"moveTo","args":[447,215]},{"method":"lineTo","args":[441,215]},{"method":"stroke"},{"method":"moveTo","args":[441,215]},{"method":"lineTo","args":[435,215]},{"method":"stroke"},{"method":"moveTo","args":[435,215]},{"method":"lineTo","args":[427,215]},{"method":"stroke"},{"method":"moveTo","args":[427,215]},{"method":"lineTo","args":[420,215]},{"method":"stroke"},{"method":"moveTo","args":[420,215]},{"method":"lineTo","args":[412,217]},{"method":"stroke"},{"method":"moveTo","args":[412,217]},{"method":"lineTo","args":[404,218]},{"method":"stroke"},{"method":"moveTo","args":[404,218]},{"method":"lineTo","args":[397,220]},{"method":"stroke"},{"method":"moveTo","args":[397,220]},{"method":"lineTo","args":[391,222]},{"method":"stroke"},{"method":"moveTo","args":[391,222]},{"method":"lineTo","args":[385,225]},{"method":"stroke"},{"method":"moveTo","args":[385,225]},{"method":"lineTo","args":[379,227]},{"method":"stroke"},{"method":"moveTo","args":[379,227]},{"method":"lineTo","args":[377,228]},{"method":"stroke"},{"method":"moveTo","args":[377,228]},{"method":"lineTo","args":[371,231]},{"method":"stroke"},{"method":"moveTo","args":[371,231]},{"method":"lineTo","args":[367,233]},{"method":"stroke"},{"method":"moveTo","args":[367,233]},{"method":"lineTo","args":[362,236]},{"method":"stroke"},{"method":"moveTo","args":[362,236]},{"method":"lineTo","args":[358,239]},{"method":"stroke"},{"method":"moveTo","args":[358,239]},{"method":"lineTo","args":[355,242]},{"method":"stroke"},{"method":"moveTo","args":[355,242]},{"method":"lineTo","args":[351,244]},{"method":"stroke"},{"method":"moveTo","args":[351,244]},{"method":"lineTo","args":[347,246]},{"method":"stroke"},{"method":"moveTo","args":[347,246]},{"method":"lineTo","args":[344,248]},{"method":"stroke"},{"method":"moveTo","args":[344,248]},{"method":"lineTo","args":[340,251]},{"method":"stroke"},{"method":"moveTo","args":[340,251]},{"method":"lineTo","args":[337,254]},{"method":"stroke"},{"method":"moveTo","args":[337,254]},{"method":"lineTo","args":[333,256]},{"method":"stroke"},{"method":"moveTo","args":[333,256]},{"method":"lineTo","args":[330,258]},{"method":"stroke"},{"method":"moveTo","args":[330,258]},{"method":"lineTo","args":[327,260]},{"method":"stroke"},{"method":"moveTo","args":[327,260]},{"method":"lineTo","args":[324,263]},{"method":"stroke"},{"method":"moveTo","args":[324,263]},{"method":"lineTo","args":[321,265]},{"method":"stroke"},{"method":"moveTo","args":[321,265]},{"method":"lineTo","args":[318,268]},{"method":"stroke"},{"method":"moveTo","args":[318,268]},{"method":"lineTo","args":[315,271]},{"method":"stroke"},{"method":"moveTo","args":[315,271]},{"method":"lineTo","args":[312,274]},{"method":"stroke"},{"method":"moveTo","args":[312,274]},{"method":"lineTo","args":[309,277]},{"method":"stroke"},{"method":"moveTo","args":[309,277]},{"method":"lineTo","args":[305,282]},{"method":"stroke"},{"method":"moveTo","args":[305,282]},{"method":"lineTo","args":[302,286]},{"method":"stroke"},{"method":"moveTo","args":[302,286]},{"method":"lineTo","args":[300,289]},{"method":"stroke"},{"method":"moveTo","args":[300,289]},{"method":"lineTo","args":[298,293]},{"method":"stroke"},{"method":"moveTo","args":[298,293]},{"method":"lineTo","args":[296,297]},{"method":"stroke"},{"method":"moveTo","args":[296,297]},{"method":"lineTo","args":[292,301]},{"method":"stroke"},{"method":"moveTo","args":[292,301]},{"method":"lineTo","args":[291,306]},{"method":"stroke"},{"method":"moveTo","args":[291,306]},{"method":"lineTo","args":[289,310]},{"method":"stroke"},{"method":"moveTo","args":[289,310]},{"method":"lineTo","args":[287,313]},{"method":"stroke"},{"method":"moveTo","args":[287,313]},{"method":"lineTo","args":[286,317]},{"method":"stroke"},{"method":"moveTo","args":[286,317]},{"method":"lineTo","args":[285,320]},{"method":"stroke"},{"method":"moveTo","args":[285,320]},{"method":"lineTo","args":[284,324]},{"method":"stroke"},{"method":"moveTo","args":[284,324]},{"method":"lineTo","args":[284,326]},{"method":"stroke"},{"method":"moveTo","args":[284,326]},{"method":"lineTo","args":[284,329]},{"method":"stroke"},{"method":"moveTo","args":[284,329]},{"method":"lineTo","args":[283,332]},{"method":"stroke"},{"method":"moveTo","args":[283,332]},{"method":"lineTo","args":[283,335]},{"method":"stroke"},{"method":"moveTo","args":[283,335]},{"method":"lineTo","args":[283,337]},{"method":"stroke"},{"method":"moveTo","args":[283,337]},{"method":"lineTo","args":[283,339]},{"method":"stroke"},{"method":"moveTo","args":[283,339]},{"method":"lineTo","args":[283,342]},{"method":"stroke"},{"method":"moveTo","args":[283,342]},{"method":"lineTo","args":[283,344]},{"method":"stroke"},{"method":"moveTo","args":[283,344]},{"method":"lineTo","args":[283,346]},{"method":"stroke"},{"method":"moveTo","args":[283,346]},{"method":"lineTo","args":[283,348]},{"method":"stroke"},{"method":"moveTo","args":[283,348]},{"method":"lineTo","args":[283,350]},{"method":"stroke"},{"method":"moveTo","args":[283,350]},{"method":"lineTo","args":[283,353]},{"method":"stroke"},{"method":"moveTo","args":[283,353]},{"method":"lineTo","args":[283,355]},{"method":"stroke"},{"method":"moveTo","args":[283,355]},{"method":"lineTo","args":[284,357]},{"method":"stroke"},{"method":"moveTo","args":[284,357]},{"method":"lineTo","args":[286,359]},{"method":"stroke"},{"method":"moveTo","args":[286,359]},{"method":"lineTo","args":[287,361]},{"method":"stroke"},{"method":"moveTo","args":[287,361]},{"method":"lineTo","args":[288,363]},{"method":"stroke"},{"method":"moveTo","args":[288,363]},{"method":"lineTo","args":[290,365]},{"method":"stroke"},{"method":"moveTo","args":[290,365]},{"method":"lineTo","args":[291,367]},{"method":"stroke"},{"method":"moveTo","args":[291,367]},{"method":"lineTo","args":[293,369]},{"method":"stroke"},{"method":"moveTo","args":[293,369]},{"method":"lineTo","args":[295,371]},{"method":"stroke"},{"method":"moveTo","args":[295,371]},{"method":"lineTo","args":[298,373]},{"method":"stroke"},{"method":"moveTo","args":[298,373]},{"method":"lineTo","args":[299,375]},{"method":"stroke"},{"method":"moveTo","args":[299,375]},{"method":"lineTo","args":[304,378]},{"method":"stroke"},{"method":"moveTo","args":[304,378]},{"method":"lineTo","args":[307,379]},{"method":"stroke"},{"method":"moveTo","args":[307,379]},{"method":"lineTo","args":[310,381]},{"method":"stroke"},{"method":"moveTo","args":[310,381]},{"method":"lineTo","args":[312,382]},{"method":"stroke"},{"method":"moveTo","args":[312,382]},{"method":"lineTo","args":[316,384]},{"method":"stroke"},{"method":"moveTo","args":[316,384]},{"method":"lineTo","args":[320,385]},{"method":"stroke"},{"method":"moveTo","args":[320,385]},{"method":"lineTo","args":[325,387]},{"method":"stroke"},{"method":"moveTo","args":[325,387]},{"method":"lineTo","args":[328,388]},{"method":"stroke"},{"method":"moveTo","args":[328,388]},{"method":"lineTo","args":[333,389]},{"method":"stroke"},{"method":"moveTo","args":[333,389]},{"method":"lineTo","args":[342,392]},{"method":"stroke"},{"method":"moveTo","args":[342,392]},{"method":"lineTo","args":[346,393]},{"method":"stroke"},{"method":"moveTo","args":[346,393]},{"method":"lineTo","args":[352,395]},{"method":"stroke"},{"method":"moveTo","args":[352,395]},{"method":"lineTo","args":[356,396]},{"method":"stroke"},{"method":"moveTo","args":[356,396]},{"method":"lineTo","args":[363,398]},{"method":"stroke"},{"method":"moveTo","args":[363,398]},{"method":"lineTo","args":[367,400]},{"method":"stroke"},{"method":"moveTo","args":[367,400]},{"method":"lineTo","args":[372,402]},{"method":"stroke"},{"method":"moveTo","args":[372,402]},{"method":"lineTo","args":[378,403]},{"method":"stroke"},{"method":"moveTo","args":[378,403]},{"method":"lineTo","args":[382,404]},{"method":"stroke"},{"method":"moveTo","args":[382,404]},{"method":"lineTo","args":[388,406]},{"method":"stroke"},{"method":"moveTo","args":[388,406]},{"method":"lineTo","args":[396,408]},{"method":"stroke"},{"method":"moveTo","args":[396,408]},{"method":"lineTo","args":[399,408]},{"method":"stroke"},{"method":"moveTo","args":[399,408]},{"method":"lineTo","args":[404,409]},{"method":"stroke"},{"method":"moveTo","args":[404,409]},{"method":"lineTo","args":[409,410]},{"method":"stroke"},{"method":"moveTo","args":[409,410]},{"method":"lineTo","args":[414,411]},{"method":"stroke"},{"method":"moveTo","args":[414,411]},{"method":"lineTo","args":[420,412]},{"method":"stroke"},{"method":"moveTo","args":[420,412]},{"method":"lineTo","args":[424,412]},{"method":"stroke"},{"method":"moveTo","args":[424,412]},{"method":"lineTo","args":[432,413]},{"method":"stroke"},{"method":"moveTo","args":[432,413]},{"method":"lineTo","args":[434,414]},{"method":"stroke"},{"method":"moveTo","args":[434,414]},{"method":"lineTo","args":[439,415]},{"method":"stroke"},{"method":"moveTo","args":[439,415]},{"method":"lineTo","args":[445,416]},{"method":"stroke"},{"method":"moveTo","args":[445,416]},{"method":"lineTo","args":[452,416]},{"method":"stroke"},{"method":"moveTo","args":[452,416]},{"method":"lineTo","args":[455,417]},{"method":"stroke"},{"method":"moveTo","args":[455,417]},{"method":"lineTo","args":[461,417]},{"method":"stroke"},{"method":"moveTo","args":[461,417]},{"method":"lineTo","args":[466,418]},{"method":"stroke"},{"method":"moveTo","args":[466,418]},{"method":"lineTo","args":[474,419]},{"method":"stroke"},{"method":"moveTo","args":[474,419]},{"method":"lineTo","args":[477,419]},{"method":"stroke"},{"method":"moveTo","args":[477,419]},{"method":"lineTo","args":[482,419]},{"method":"stroke"},{"method":"moveTo","args":[482,419]},{"method":"lineTo","args":[487,420]},{"method":"stroke"},{"method":"moveTo","args":[487,420]},{"method":"lineTo","args":[491,420]},{"method":"stroke"},{"method":"moveTo","args":[491,420]},{"method":"lineTo","args":[496,420]},{"method":"stroke"},{"method":"moveTo","args":[496,420]},{"method":"lineTo","args":[501,420]},{"method":"stroke"},{"method":"moveTo","args":[501,420]},{"method":"lineTo","args":[506,420]},{"method":"stroke"},{"method":"moveTo","args":[506,420]},{"method":"lineTo","args":[510,420]},{"method":"stroke"},{"method":"moveTo","args":[510,420]},{"method":"lineTo","args":[514,420]},{"method":"stroke"},{"method":"moveTo","args":[514,420]},{"method":"lineTo","args":[519,420]},{"method":"stroke"},{"method":"moveTo","args":[519,420]},{"method":"lineTo","args":[526,420]},{"method":"stroke"},{"method":"moveTo","args":[526,420]},{"method":"lineTo","args":[528,420]},{"method":"stroke"},{"method":"moveTo","args":[528,420]},{"method":"lineTo","args":[533,420]},{"method":"stroke"},{"method":"moveTo","args":[533,420]},{"method":"lineTo","args":[538,420]},{"method":"stroke"},{"method":"moveTo","args":[538,420]},{"method":"lineTo","args":[542,420]},{"method":"stroke"},{"method":"moveTo","args":[542,420]},{"method":"lineTo","args":[547,420]},{"method":"stroke"},{"method":"moveTo","args":[547,420]},{"method":"lineTo","args":[551,420]},{"method":"stroke"},{"method":"moveTo","args":[551,420]},{"method":"lineTo","args":[555,420]},{"method":"stroke"},{"method":"moveTo","args":[555,420]},{"method":"lineTo","args":[559,420]},{"method":"stroke"},{"method":"moveTo","args":[559,420]},{"method":"lineTo","args":[563,420]},{"method":"stroke"},{"method":"moveTo","args":[563,420]},{"method":"lineTo","args":[568,420]},{"method":"stroke"},{"method":"moveTo","args":[568,420]},{"method":"lineTo","args":[571,420]},{"method":"stroke"},{"method":"moveTo","args":[571,420]},{"method":"lineTo","args":[575,419]},{"method":"stroke"},{"method":"moveTo","args":[575,419]},{"method":"lineTo","args":[579,419]},{"method":"stroke"},{"method":"moveTo","args":[579,419]},{"method":"lineTo","args":[583,418]},{"method":"stroke"},{"method":"moveTo","args":[583,418]},{"method":"lineTo","args":[587,417]},{"method":"stroke"},{"method":"moveTo","args":[587,417]},{"method":"lineTo","args":[592,416]},{"method":"stroke"},{"method":"moveTo","args":[592,416]},{"method":"lineTo","args":[597,415]},{"method":"stroke"},{"method":"moveTo","args":[597,415]},{"method":"lineTo","args":[601,414]},{"method":"stroke"},{"method":"moveTo","args":[601,414]},{"method":"lineTo","args":[605,414]},{"method":"stroke"},{"method":"moveTo","args":[605,414]},{"method":"lineTo","args":[610,413]},{"method":"stroke"},{"method":"moveTo","args":[610,413]},{"method":"lineTo","args":[615,413]},{"method":"stroke"},{"method":"moveTo","args":[615,413]},{"method":"lineTo","args":[619,412]},{"method":"stroke"},{"method":"moveTo","args":[619,412]},{"method":"lineTo","args":[624,411]},{"method":"stroke"},{"method":"moveTo","args":[624,411]},{"method":"lineTo","args":[629,411]},{"method":"stroke"},{"method":"moveTo","args":[629,411]},{"method":"lineTo","args":[634,410]},{"method":"stroke"},{"method":"moveTo","args":[634,410]},{"method":"lineTo","args":[638,410]},{"method":"stroke"},{"method":"moveTo","args":[638,410]},{"method":"lineTo","args":[643,410]},{"method":"stroke"},{"method":"moveTo","args":[643,410]},{"method":"lineTo","args":[648,409]},{"method":"stroke"},{"method":"moveTo","args":[648,409]},{"method":"lineTo","args":[653,408]},{"method":"stroke"},{"method":"moveTo","args":[653,408]},{"method":"lineTo","args":[656,408]},{"method":"stroke"},{"method":"moveTo","args":[656,408]},{"method":"lineTo","args":[661,407]},{"method":"stroke"},{"method":"moveTo","args":[661,407]},{"method":"lineTo","args":[666,406]},{"method":"stroke"},{"method":"moveTo","args":[666,406]},{"method":"lineTo","args":[671,406]},{"method":"stroke"},{"method":"moveTo","args":[671,406]},{"method":"lineTo","args":[674,405]},{"method":"stroke"},{"method":"moveTo","args":[674,405]},{"method":"lineTo","args":[678,404]},{"method":"stroke"},{"method":"moveTo","args":[678,404]},{"method":"lineTo","args":[682,403]},{"method":"stroke"},{"method":"moveTo","args":[682,403]},{"method":"lineTo","args":[686,403]},{"method":"stroke"},{"method":"moveTo","args":[686,403]},{"method":"lineTo","args":[689,402]},{"method":"stroke"},{"method":"moveTo","args":[689,402]},{"method":"lineTo","args":[693,401]},{"method":"stroke"},{"method":"moveTo","args":[693,401]},{"method":"lineTo","args":[696,400]},{"method":"stroke"},{"method":"moveTo","args":[696,400]},{"method":"lineTo","args":[699,400]},{"method":"stroke"},{"method":"moveTo","args":[699,400]},{"method":"lineTo","args":[702,398]},{"method":"stroke"},{"method":"moveTo","args":[702,398]},{"method":"lineTo","args":[706,398]},{"method":"stroke"},{"method":"moveTo","args":[706,398]},{"method":"lineTo","args":[708,397]},{"method":"stroke"},{"method":"moveTo","args":[708,397]},{"method":"lineTo","args":[712,395]},{"method":"stroke"},{"method":"moveTo","args":[712,395]},{"method":"lineTo","args":[715,395]},{"method":"stroke"},{"method":"moveTo","args":[715,395]},{"method":"lineTo","args":[718,394]},{"method":"stroke"},{"method":"moveTo","args":[718,394]},{"method":"lineTo","args":[721,393]},{"method":"stroke"},{"method":"moveTo","args":[721,393]},{"method":"lineTo","args":[724,392]},{"method":"stroke"},{"method":"moveTo","args":[724,392]},{"method":"lineTo","args":[728,390]},{"method":"stroke"},{"method":"moveTo","args":[728,390]},{"method":"lineTo","args":[730,390]},{"method":"stroke"},{"method":"moveTo","args":[730,390]},{"method":"lineTo","args":[733,388]},{"method":"stroke"},{"method":"moveTo","args":[733,388]},{"method":"lineTo","args":[737,388]},{"method":"stroke"},{"method":"moveTo","args":[737,388]},{"method":"lineTo","args":[740,386]},{"method":"stroke"},{"method":"moveTo","args":[740,386]},{"method":"lineTo","args":[743,385]},{"method":"stroke"},{"method":"moveTo","args":[743,385]},{"method":"lineTo","args":[746,384]},{"method":"stroke"},{"method":"moveTo","args":[746,384]},{"method":"lineTo","args":[749,382]},{"method":"stroke"},{"method":"moveTo","args":[749,382]},{"method":"lineTo","args":[752,381]},{"method":"stroke"},{"method":"moveTo","args":[752,381]},{"method":"lineTo","args":[754,380]},{"method":"stroke"},{"method":"moveTo","args":[754,380]},{"method":"lineTo","args":[757,379]},{"method":"stroke"},{"method":"moveTo","args":[757,379]},{"method":"lineTo","args":[760,378]},{"method":"stroke"},{"method":"moveTo","args":[760,378]},{"method":"lineTo","args":[762,377]},{"method":"stroke"},{"method":"moveTo","args":[762,377]},{"method":"lineTo","args":[766,375]},{"method":"stroke"},{"method":"moveTo","args":[766,375]},{"method":"lineTo","args":[769,374]},{"method":"stroke"},{"method":"moveTo","args":[769,374]},{"method":"lineTo","args":[772,372]},{"method":"stroke"},{"method":"moveTo","args":[772,372]},{"method":"lineTo","args":[775,370]},{"method":"stroke"},{"method":"moveTo","args":[775,370]},{"method":"lineTo","args":[777,369]},{"method":"stroke"},{"method":"moveTo","args":[777,369]},{"method":"lineTo","args":[780,368]},{"method":"stroke"},{"method":"moveTo","args":[780,368]},{"method":"lineTo","args":[783,366]},{"method":"stroke"},{"method":"moveTo","args":[783,366]},{"method":"lineTo","args":[786,364]},{"method":"stroke"},{"method":"moveTo","args":[786,364]},{"method":"lineTo","args":[788,363]},{"method":"stroke"},{"method":"moveTo","args":[788,363]},{"method":"lineTo","args":[791,361]},{"method":"stroke"},{"method":"moveTo","args":[791,361]},{"method":"lineTo","args":[794,359]},{"method":"stroke"},{"method":"moveTo","args":[794,359]},{"method":"lineTo","args":[797,358]},{"method":"stroke"},{"method":"moveTo","args":[797,358]},{"method":"lineTo","args":[800,355]},{"method":"stroke"},{"method":"moveTo","args":[800,355]},{"method":"lineTo","args":[802,353]},{"method":"stroke"},{"method":"moveTo","args":[802,353]},{"method":"lineTo","args":[805,352]},{"method":"stroke"},{"method":"moveTo","args":[805,352]},{"method":"lineTo","args":[807,350]},{"method":"stroke"},{"method":"moveTo","args":[807,350]},{"method":"lineTo","args":[810,348]},{"method":"stroke"},{"method":"moveTo","args":[810,348]},{"method":"lineTo","args":[811,346]},{"method":"stroke"},{"method":"moveTo","args":[811,346]},{"method":"lineTo","args":[813,345]},{"method":"stroke"},{"method":"moveTo","args":[813,345]},{"method":"lineTo","args":[814,343]},{"method":"stroke"},{"method":"moveTo","args":[814,343]},{"method":"lineTo","args":[816,341]},{"method":"stroke"},{"method":"moveTo","args":[816,341]},{"method":"lineTo","args":[817,340]},{"method":"stroke"},{"method":"moveTo","args":[817,340]},{"method":"lineTo","args":[818,338]},{"method":"stroke"},{"method":"moveTo","args":[818,338]},{"method":"lineTo","args":[819,337]},{"method":"stroke"},{"method":"moveTo","args":[819,337]},{"method":"lineTo","args":[820,336]},{"method":"stroke"},{"method":"moveTo","args":[820,336]},{"method":"lineTo","args":[821,335]},{"method":"stroke"},{"method":"moveTo","args":[821,335]},{"method":"lineTo","args":[822,334]},{"method":"stroke"},{"method":"moveTo","args":[822,334]},{"method":"lineTo","args":[823,333]},{"method":"stroke"},{"method":"moveTo","args":[823,333]},{"method":"lineTo","args":[823,332]},{"method":"stroke"},{"method":"moveTo","args":[823,332]},{"method":"lineTo","args":[825,330]},{"method":"stroke"},{"method":"moveTo","args":[825,330]},{"method":"lineTo","args":[825,330]},{"method":"stroke"},{"method":"moveTo","args":[825,330]},{"method":"lineTo","args":[826,329]},{"method":"stroke"},{"method":"moveTo","args":[826,329]},{"method":"lineTo","args":[827,328]},{"method":"stroke"},{"method":"moveTo","args":[827,328]},{"method":"lineTo","args":[828,327]},{"method":"stroke"},{"method":"moveTo","args":[828,327]},{"method":"lineTo","args":[828,326]},{"method":"stroke"},{"method":"moveTo","args":[828,326]},{"method":"lineTo","args":[830,325]},{"method":"stroke"},{"method":"moveTo","args":[830,325]},{"method":"lineTo","args":[830,324]},{"method":"stroke"},{"method":"moveTo","args":[830,324]},{"method":"lineTo","args":[831,323]},{"method":"stroke"},{"method":"moveTo","args":[831,323]},{"method":"lineTo","args":[832,322]},{"method":"stroke"},{"method":"moveTo","args":[832,322]},{"method":"lineTo","args":[833,321]},{"method":"stroke"},{"method":"moveTo","args":[833,321]},{"method":"lineTo","args":[834,320]},{"method":"stroke"},{"method":"moveTo","args":[834,320]},{"method":"lineTo","args":[835,318]},{"method":"stroke"},{"method":"moveTo","args":[835,318]},{"method":"lineTo","args":[836,317]},{"method":"stroke"},{"method":"moveTo","args":[836,317]},{"method":"lineTo","args":[836,316]},{"method":"stroke"},{"method":"moveTo","args":[836,316]},{"method":"lineTo","args":[837,315]},{"method":"stroke"},{"method":"moveTo","args":[837,315]},{"method":"lineTo","args":[838,314]},{"method":"stroke"},{"method":"moveTo","args":[838,314]},{"method":"lineTo","args":[838,313]},{"method":"stroke"},{"method":"moveTo","args":[838,313]},{"method":"lineTo","args":[839,312]},{"method":"stroke"},{"method":"moveTo","args":[839,312]},{"method":"lineTo","args":[839,310]},{"method":"stroke"},{"method":"moveTo","args":[839,310]},{"method":"lineTo","args":[840,308]},{"method":"stroke"},{"method":"moveTo","args":[840,308]},{"method":"lineTo","args":[841,307]},{"method":"stroke"},{"method":"moveTo","args":[841,307]},{"method":"lineTo","args":[841,304]},{"method":"stroke"},{"method":"moveTo","args":[841,304]},{"method":"lineTo","args":[842,301]},{"method":"stroke"},{"method":"moveTo","args":[842,301]},{"method":"lineTo","args":[843,298]},{"method":"stroke"},{"method":"moveTo","args":[843,298]},{"method":"lineTo","args":[843,295]},{"method":"stroke"},{"method":"moveTo","args":[843,295]},{"method":"lineTo","args":[844,292]},{"method":"stroke"},{"method":"moveTo","args":[844,292]},{"method":"lineTo","args":[845,288]},{"method":"stroke"},{"method":"moveTo","args":[845,288]},{"method":"lineTo","args":[845,286]},{"method":"stroke"},{"method":"moveTo","args":[845,286]},{"method":"lineTo","args":[846,284]},{"method":"stroke"},{"method":"moveTo","args":[846,284]},{"method":"lineTo","args":[846,282]},{"method":"stroke"},{"method":"moveTo","args":[846,282]},{"method":"lineTo","args":[846,280]},{"method":"stroke"},{"method":"moveTo","args":[846,280]},{"method":"lineTo","args":[846,278]},{"method":"stroke"},{"method":"moveTo","args":[846,278]},{"method":"lineTo","args":[846,276]},{"method":"stroke"},{"method":"moveTo","args":[846,276]},{"method":"lineTo","args":[846,273]},{"method":"stroke"},{"method":"moveTo","args":[846,273]},{"method":"lineTo","args":[846,272]},{"method":"stroke"},{"method":"moveTo","args":[846,272]},{"method":"lineTo","args":[846,269]},{"method":"stroke"},{"method":"moveTo","args":[846,269]},{"method":"lineTo","args":[846,266]},{"method":"stroke"},{"method":"moveTo","args":[846,266]},{"method":"lineTo","args":[846,263]},{"method":"stroke"},{"method":"moveTo","args":[846,263]},{"method":"lineTo","args":[846,261]},{"method":"stroke"},{"method":"moveTo","args":[846,261]},{"method":"lineTo","args":[846,259]},{"method":"stroke"},{"method":"moveTo","args":[846,259]},{"method":"lineTo","args":[846,255]},{"method":"stroke"},{"method":"moveTo","args":[846,255]},{"method":"lineTo","args":[846,252]},{"method":"stroke"},{"method":"moveTo","args":[846,252]},{"method":"lineTo","args":[846,250]},{"method":"stroke"},{"method":"moveTo","args":[846,250]},{"method":"lineTo","args":[846,246]},{"method":"stroke"},{"method":"moveTo","args":[846,246]},{"method":"lineTo","args":[846,242]},{"method":"stroke"},{"method":"moveTo","args":[846,242]},{"method":"lineTo","args":[846,239]},{"method":"stroke"},{"method":"moveTo","args":[846,239]},{"method":"lineTo","args":[846,236]},{"method":"stroke"},{"method":"moveTo","args":[846,236]},{"method":"lineTo","args":[846,233]},{"method":"stroke"},{"method":"moveTo","args":[846,233]},{"method":"lineTo","args":[846,229]},{"method":"stroke"},{"method":"moveTo","args":[846,229]},{"method":"lineTo","args":[846,227]},{"method":"stroke"},{"method":"moveTo","args":[846,227]},{"method":"lineTo","args":[845,224]},{"method":"stroke"},{"method":"moveTo","args":[845,224]},{"method":"lineTo","args":[845,221]},{"method":"stroke"},{"method":"moveTo","args":[845,221]},{"method":"lineTo","args":[845,219]},{"method":"stroke"},{"method":"moveTo","args":[845,219]},{"method":"lineTo","args":[845,217]},{"method":"stroke"},{"method":"moveTo","args":[845,217]},{"method":"lineTo","args":[844,215]},{"method":"stroke"},{"method":"moveTo","args":[844,215]},{"method":"lineTo","args":[844,213]},{"method":"stroke"},{"method":"moveTo","args":[844,213]},{"method":"lineTo","args":[844,212]},{"method":"stroke"},{"method":"moveTo","args":[844,212]},{"method":"lineTo","args":[843,210]},{"method":"stroke"},{"method":"moveTo","args":[843,210]},{"method":"lineTo","args":[842,209]},{"method":"stroke"},{"method":"moveTo","args":[842,209]},{"method":"lineTo","args":[842,208]},{"method":"stroke"},{"method":"moveTo","args":[842,208]},{"method":"lineTo","args":[842,208]},{"method":"stroke"},{"method":"moveTo","args":[842,208]},{"method":"lineTo","args":[841,207]},{"method":"stroke"},{"method":"moveTo","args":[841,207]},{"method":"lineTo","args":[841,207]},{"method":"stroke"},{"method":"moveTo","args":[841,207]},{"method":"lineTo","args":[841,207]},{"method":"stroke"},{"method":"moveTo","args":[841,207]},{"method":"lineTo","args":[840,207]},{"method":"stroke"},{"method":"moveTo","args":[840,207]},{"method":"lineTo","args":[840,206]},{"method":"stroke"},{"method":"moveTo","args":[840,206]},{"method":"lineTo","args":[839,206]},{"method":"stroke"},{"method":"moveTo","args":[839,206]},{"method":"lineTo","args":[838,205]},{"method":"stroke"},{"method":"moveTo","args":[838,205]},{"method":"lineTo","args":[837,204]},{"method":"stroke"},{"method":"moveTo","args":[837,204]},{"method":"lineTo","args":[836,203]},{"method":"stroke"},{"method":"moveTo","args":[836,203]},{"method":"lineTo","args":[834,201]},{"method":"stroke"},{"method":"moveTo","args":[834,201]},{"method":"lineTo","args":[832,201]},{"method":"stroke"},{"method":"moveTo","args":[832,201]},{"method":"lineTo","args":[830,200]},{"method":"stroke"},{"method":"moveTo","args":[830,200]},{"method":"lineTo","args":[828,198]},{"method":"stroke"},{"method":"moveTo","args":[828,198]},{"method":"lineTo","args":[826,197]},{"method":"stroke"},{"method":"moveTo","args":[826,197]},{"method":"lineTo","args":[825,196]},{"method":"stroke"},{"method":"moveTo","args":[825,196]},{"method":"lineTo","args":[823,195]},{"method":"stroke"},{"method":"moveTo","args":[823,195]},{"method":"lineTo","args":[821,195]},{"method":"stroke"},{"method":"moveTo","args":[821,195]},{"method":"lineTo","args":[819,193]},{"method":"stroke"},{"method":"moveTo","args":[819,193]},{"method":"lineTo","args":[816,193]},{"method":"stroke"},{"method":"moveTo","args":[816,193]},{"method":"lineTo","args":[813,192]},{"method":"stroke"},{"method":"moveTo","args":[813,192]},{"method":"lineTo","args":[811,191]},{"method":"stroke"},{"method":"moveTo","args":[811,191]},{"method":"lineTo","args":[808,189]},{"method":"stroke"},{"method":"moveTo","args":[808,189]},{"method":"lineTo","args":[805,188]},{"method":"stroke"},{"method":"moveTo","args":[805,188]},{"method":"lineTo","args":[802,187]},{"method":"stroke"},{"method":"moveTo","args":[802,187]},{"method":"lineTo","args":[797,185]},{"method":"stroke"},{"method":"moveTo","args":[797,185]},{"method":"lineTo","args":[793,184]},{"method":"stroke"},{"method":"moveTo","args":[793,184]},{"method":"lineTo","args":[788,182]},{"method":"stroke"},{"method":"moveTo","args":[788,182]},{"method":"lineTo","args":[785,182]},{"method":"stroke"},{"method":"moveTo","args":[785,182]},{"method":"lineTo","args":[780,181]},{"method":"stroke"},{"method":"moveTo","args":[780,181]},{"method":"lineTo","args":[776,179]},{"method":"stroke"},{"method":"moveTo","args":[776,179]},{"method":"lineTo","args":[773,179]},{"method":"stroke"},{"method":"moveTo","args":[773,179]},{"method":"lineTo","args":[768,178]},{"method":"stroke"},{"method":"moveTo","args":[768,178]},{"method":"lineTo","args":[764,178]},{"method":"stroke"},{"method":"moveTo","args":[764,178]},{"method":"lineTo","args":[761,177]},{"method":"stroke"},{"method":"moveTo","args":[761,177]},{"method":"lineTo","args":[758,176]},{"method":"stroke"},{"method":"moveTo","args":[758,176]},{"method":"lineTo","args":[754,176]},{"method":"stroke"},{"method":"moveTo","args":[754,176]},{"method":"lineTo","args":[750,176]},{"method":"stroke"},{"method":"moveTo","args":[750,176]},{"method":"lineTo","args":[747,175]},{"method":"stroke"},{"method":"moveTo","args":[747,175]},{"method":"lineTo","args":[743,175]},{"method":"stroke"},{"method":"moveTo","args":[743,175]},{"method":"lineTo","args":[739,174]},{"method":"stroke"},{"method":"moveTo","args":[739,174]},{"method":"lineTo","args":[736,174]},{"method":"stroke"},{"method":"moveTo","args":[736,174]},{"method":"lineTo","args":[732,174]},{"method":"stroke"},{"method":"moveTo","args":[732,174]},{"method":"lineTo","args":[728,174]},{"method":"stroke"},{"method":"moveTo","args":[728,174]},{"method":"lineTo","args":[724,174]},{"method":"stroke"},{"method":"moveTo","args":[724,174]},{"method":"lineTo","args":[721,174]},{"method":"stroke"},{"method":"moveTo","args":[721,174]},{"method":"lineTo","args":[718,174]},{"method":"stroke"},{"method":"moveTo","args":[718,174]},{"method":"lineTo","args":[714,174]},{"method":"stroke"},{"method":"moveTo","args":[714,174]},{"method":"lineTo","args":[712,174]},{"method":"stroke"},{"method":"moveTo","args":[712,174]},{"method":"lineTo","args":[709,174]},{"method":"stroke"},{"method":"moveTo","args":[709,174]},{"method":"lineTo","args":[706,174]},{"method":"stroke"},{"method":"moveTo","args":[706,174]},{"method":"lineTo","args":[702,174]},{"method":"stroke"},{"method":"moveTo","args":[702,174]},{"method":"lineTo","args":[700,174]},{"method":"stroke"},{"method":"moveTo","args":[700,174]},{"method":"lineTo","args":[697,174]},{"method":"stroke"},{"method":"moveTo","args":[697,174]},{"method":"lineTo","args":[694,174]},{"method":"stroke"},{"method":"moveTo","args":[694,174]},{"method":"lineTo","args":[691,174]},{"method":"stroke"},{"method":"moveTo","args":[691,174]},{"method":"lineTo","args":[687,174]},{"method":"stroke"},{"method":"moveTo","args":[687,174]},{"method":"lineTo","args":[684,174]},{"method":"stroke"},{"method":"moveTo","args":[684,174]},{"method":"lineTo","args":[679,174]},{"method":"stroke"},{"method":"moveTo","args":[679,174]},{"method":"lineTo","args":[676,174]},{"method":"stroke"},{"method":"moveTo","args":[676,174]},{"method":"lineTo","args":[667,175]},{"method":"stroke"},{"method":"moveTo","args":[667,175]},{"method":"lineTo","args":[663,176]},{"method":"stroke"},{"method":"moveTo","args":[663,176]},{"method":"lineTo","args":[658,177]},{"method":"stroke"},{"method":"moveTo","args":[658,177]},{"method":"lineTo","args":[653,177]},{"method":"stroke"},{"method":"moveTo","args":[653,177]},{"method":"lineTo","args":[649,178]},{"method":"stroke"},{"method":"moveTo","args":[649,178]},{"method":"lineTo","args":[645,179]},{"method":"stroke"},{"method":"moveTo","args":[645,179]},{"method":"lineTo","args":[641,180]},{"method":"stroke"},{"method":"moveTo","args":[641,180]},{"method":"lineTo","args":[637,180]},{"method":"stroke"},{"method":"moveTo","args":[637,180]},{"method":"lineTo","args":[633,181]},{"method":"stroke"},{"method":"moveTo","args":[633,181]},{"method":"lineTo","args":[630,182]},{"method":"stroke"},{"method":"moveTo","args":[630,182]},{"method":"lineTo","args":[626,183]},{"method":"stroke"},{"method":"moveTo","args":[626,183]},{"method":"lineTo","args":[622,184]},{"method":"stroke"},{"method":"moveTo","args":[622,184]},{"method":"lineTo","args":[619,186]},{"method":"stroke"},{"method":"moveTo","args":[619,186]},{"method":"lineTo","args":[615,186]},{"method":"stroke"},{"method":"moveTo","args":[615,186]},{"method":"lineTo","args":[611,187]},{"method":"stroke"},{"method":"moveTo","args":[611,187]},{"method":"lineTo","args":[608,189]},{"method":"stroke"},{"method":"moveTo","args":[608,189]},{"method":"lineTo","args":[604,189]},{"method":"stroke"},{"method":"moveTo","args":[604,189]},{"method":"lineTo","args":[600,190]},{"method":"stroke"},{"method":"moveTo","args":[600,190]},{"method":"lineTo","args":[597,191]},{"method":"stroke"},{"method":"moveTo","args":[597,191]},{"method":"lineTo","args":[594,191]},{"method":"stroke"},{"method":"moveTo","args":[594,191]},{"method":"lineTo","args":[590,192]},{"method":"stroke"},{"method":"moveTo","args":[590,192]},{"method":"lineTo","args":[586,193]},{"method":"stroke"},{"method":"moveTo","args":[586,193]},{"method":"lineTo","args":[584,194]},{"method":"stroke"},{"method":"moveTo","args":[584,194]},{"method":"lineTo","args":[580,194]},{"method":"stroke"},{"method":"moveTo","args":[580,194]},{"method":"lineTo","args":[577,195]},{"method":"stroke"},{"method":"moveTo","args":[577,195]},{"method":"lineTo","args":[575,196]},{"method":"stroke"},{"method":"moveTo","args":[575,196]},{"method":"lineTo","args":[571,196]},{"method":"stroke"},{"method":"moveTo","args":[571,196]},{"method":"lineTo","args":[568,198]},{"method":"stroke"},{"method":"moveTo","args":[568,198]},{"method":"lineTo","args":[565,199]},{"method":"stroke"},{"method":"moveTo","args":[565,199]},{"method":"lineTo","args":[562,199]},{"method":"stroke"},{"method":"moveTo","args":[562,199]},{"method":"lineTo","args":[559,200]},{"method":"stroke"},{"method":"moveTo","args":[559,200]},{"method":"lineTo","args":[556,201]},{"method":"stroke"},{"method":"moveTo","args":[556,201]},{"method":"lineTo","args":[553,201]},{"method":"stroke"},{"method":"moveTo","args":[553,201]},{"method":"lineTo","args":[549,202]},{"method":"stroke"},{"method":"moveTo","args":[549,202]},{"method":"lineTo","args":[546,203]},{"method":"stroke"},{"method":"moveTo","args":[546,203]},{"method":"lineTo","args":[541,204]},{"method":"stroke"},{"method":"moveTo","args":[541,204]},{"method":"lineTo","args":[538,205]},{"method":"stroke"},{"method":"moveTo","args":[538,205]},{"method":"lineTo","args":[534,206]},{"method":"stroke"},{"method":"moveTo","args":[534,206]},{"method":"lineTo","args":[530,207]},{"method":"stroke"},{"method":"moveTo","args":[530,207]},{"method":"lineTo","args":[527,208]},{"method":"stroke"},{"method":"moveTo","args":[527,208]},{"method":"lineTo","args":[523,209]},{"method":"stroke"},{"method":"moveTo","args":[523,209]},{"method":"lineTo","args":[519,210]},{"method":"stroke"},{"method":"moveTo","args":[519,210]},{"method":"lineTo","args":[517,211]},{"method":"stroke"},{"method":"moveTo","args":[517,211]},{"method":"lineTo","args":[513,212]},{"method":"stroke"},{"method":"moveTo","args":[513,212]},{"method":"lineTo","args":[510,213]},{"method":"stroke"},{"method":"moveTo","args":[510,213]},{"method":"lineTo","args":[507,214]},{"method":"stroke"},{"method":"moveTo","args":[507,214]},{"method":"lineTo","args":[505,215]},{"method":"stroke"},{"method":"moveTo","args":[505,215]},{"method":"lineTo","args":[502,216]},{"method":"stroke"},{"method":"moveTo","args":[502,216]},{"method":"lineTo","args":[499,216]},{"method":"stroke"},{"method":"moveTo","args":[499,216]},{"method":"lineTo","args":[496,217]},{"method":"stroke"},{"method":"moveTo","args":[496,217]},{"method":"lineTo","args":[494,218]},{"method":"stroke"},{"method":"moveTo","args":[494,218]},{"method":"lineTo","args":[492,218]},{"method":"stroke"},{"method":"moveTo","args":[492,218]},{"method":"lineTo","args":[489,218]},{"method":"stroke"},{"method":"moveTo","args":[489,218]},{"method":"lineTo","args":[487,218]},{"method":"stroke"},{"method":"moveTo","args":[487,218]},{"method":"lineTo","args":[484,218]},{"method":"stroke"},{"method":"moveTo","args":[484,218]},{"method":"lineTo","args":[483,218]},{"method":"stroke"},{"method":"moveTo","args":[483,218]},{"method":"lineTo","args":[481,218]},{"method":"stroke"},{"method":"moveTo","args":[481,218]},{"method":"lineTo","args":[478,218]},{"method":"stroke"},{"method":"moveTo","args":[478,218]},{"method":"lineTo","args":[476,218]},{"method":"stroke"},{"method":"moveTo","args":[476,218]},{"method":"lineTo","args":[474,218]},{"method":"stroke"},{"method":"moveTo","args":[474,218]},{"method":"lineTo","args":[472,218]},{"method":"stroke"},{"method":"moveTo","args":[472,218]},{"method":"lineTo","args":[470,218]},{"method":"stroke"},{"method":"moveTo","args":[470,218]},{"method":"lineTo","args":[469,218]},{"method":"stroke"},{"method":"moveTo","args":[469,218]},{"method":"lineTo","args":[467,218]},{"method":"stroke"},{"method":"moveTo","args":[467,218]},{"method":"lineTo","args":[466,218]},{"method":"stroke"},{"method":"moveTo","args":[466,218]},{"method":"lineTo","args":[464,218]},{"method":"stroke"},{"method":"moveTo","args":[464,218]},{"method":"lineTo","args":[463,218]},{"method":"stroke"},{"method":"moveTo","args":[463,218]},{"method":"lineTo","args":[462,218]},{"method":"stroke"},{"method":"moveTo","args":[462,218]},{"method":"lineTo","args":[461,218]},{"method":"stroke"},{"method":"moveTo","args":[461,218]},{"method":"lineTo","args":[460,218]},{"method":"stroke"},{"method":"moveTo","args":[460,218]},{"method":"lineTo","args":[459,218]},{"method":"stroke"},{"method":"moveTo","args":[459,218]},{"method":"lineTo","args":[458,218]},{"method":"stroke"},{"method":"moveTo","args":[458,218]},{"method":"lineTo","args":[456,217]},{"method":"stroke"},{"method":"moveTo","args":[456,217]},{"method":"lineTo","args":[455,217]},{"method":"stroke"},{"method":"moveTo","args":[455,217]},{"method":"lineTo","args":[453,217]},{"method":"stroke"},{"method":"moveTo","args":[453,217]},{"method":"lineTo","args":[451,216]},{"method":"stroke"},{"method":"moveTo","args":[451,216]},{"method":"lineTo","args":[450,216]},{"method":"stroke"},{"method":"moveTo","args":[450,216]},{"method":"lineTo","args":[448,215]},{"method":"stroke"},{"method":"moveTo","args":[448,215]},{"method":"lineTo","args":[447,215]},{"method":"stroke"},{"method":"moveTo","args":[447,215]},{"method":"lineTo","args":[445,215]},{"method":"stroke"},{"method":"moveTo","args":[445,215]},{"method":"lineTo","args":[443,214]},{"method":"stroke"},{"method":"moveTo","args":[443,214]},{"method":"lineTo","args":[442,214]},{"method":"stroke"},{"method":"moveTo","args":[442,214]},{"method":"lineTo","args":[440,214]},{"method":"stroke"},{"method":"moveTo","args":[440,214]},{"method":"lineTo","args":[439,214]},{"method":"stroke"},{"method":"moveTo","args":[439,214]},{"method":"lineTo","args":[437,214]},{"method":"stroke"},{"method":"moveTo","args":[437,214]},{"method":"lineTo","args":[436,214]},{"method":"stroke"},{"method":"moveTo","args":[436,214]},{"method":"lineTo","args":[434,214]},{"method":"stroke"},{"method":"moveTo","args":[434,214]},{"method":"lineTo","args":[433,214]},{"method":"stroke"},{"method":"moveTo","args":[433,214]},{"method":"lineTo","args":[431,214]},{"method":"stroke"},{"method":"moveTo","args":[431,214]},{"method":"lineTo","args":[430,214]},{"method":"stroke"},{"method":"moveTo","args":[430,214]},{"method":"lineTo","args":[430,214]},{"method":"stroke"},{"method":"moveTo","args":[430,214]},{"method":"lineTo","args":[429,214]},{"method":"stroke"},{"method":"closePath"}]}';
sketcher2.serializer.load(contentSk2);
canvasContainer2.style.pointerEvents = "none";
myCanvas2.style.pointerEvents = "none";
canvasContainer2.style.display = 'none';

videoCanvases.push(canvasContainer2);

annot2 = {
"id": 2,
"time": 30,
"interval": 1,
"color": "#D95F02",
"active": 0
}
videoAnnotations.push(annot2);

annot3 = {
"id": 3,
"time": 37,
"interval": 2,
"color": "#7570B3",
"active": 0
}
videoAnnotations.push(annot3);

annot4 = {
"id": 4,
"time": 3,
"interval": 4,
"color": "#E7298A",
"active": 0
}
videoAnnotations.push(annot4);

videoAnnotations.sort((a, b) => parseFloat(a.time) - parseFloat(b.time));
