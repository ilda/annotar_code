﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Microsoft.MixedReality.Toolkit.UI;

public class PressableAnnotationText : MonoBehaviour
{
    private Vector3 scale4 = new Vector3(4f, 4f, 4f);
    private Vector3 scale1 = new Vector3(1.5f, 1.5f, 1.5f);
    bool fullscreen = false;
    private GameObject[] annotations;

    public void scaleAnnotation()
    {
        annotations = GameObject.FindGameObjectsWithTag("Annotation");
        GameObject newAnnotation = this.transform.GetChild(0).gameObject;
        GameObject videoPanel = newAnnotation.transform.GetChild(1).gameObject;


        if (fullscreen)
        {
            foreach(GameObject annotation in annotations)
            {
                //reenable interact with them
            }
            this.gameObject.transform.localScale = scale1;
            this.GetComponent<AnnotationExtension>().isOpen = false;
            videoPanel.SetActive(false);
            HerokuTextTablet.received = true;
            fullscreen = false;
        } else
        {
            foreach (GameObject annotation in annotations)
            {

                if (annotation.name != this.name)
                {
                    annotation.GetComponent<Interactable>().enabled = false;
                }
                //disable interaction with them
            }
            // this enable interacting
            this.gameObject.transform.localScale = scale4;

            //condition if the video is not null
            videoPanel.SetActive(true);

            //z position should be higher
            //enable video player and start loop
            this.GetComponent<AnnotationExtension>().isOpen = true;
            HerokuTextTablet.received = true;
            fullscreen = true;
        }

    }


}
