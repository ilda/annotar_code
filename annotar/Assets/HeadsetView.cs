﻿using UnityEngine;

public class HeadsetView : MonoBehaviour//, IMixedRealityPointerHandler, IStateHandler
{
    public GameObject camera;
    private Transform cameraTransform;
    private RectTransform rectTransform;
    private Transform thisTransform;
    public float distanceToCamera = 1f;
    //public Material material;
    //public Collider collider;
    //public Transform current_deaugmentedGameObjectsRoot;
    //public static Transform deaugmentedGameObjectsRoot;


    // Start is called before the first frame update
    void Start()
    {
        //deaugmentedGameObjectsRoot = current_deaugmentedGameObjectsRoot;
        //CoreServices.InputSystem?.RegisterHandler<IMixedRealityPointerHandler>(this);
        cameraTransform = camera.transform;
        thisTransform = this.transform;
        //rectTransform = this.GetComponent<RectTransform>();
    }

    void OnDestroy()
    {
        //CoreServices.InputSystem?.UnregisterHandler<IMixedRealityPointerHandler>(this);
    }

    // Update is called once per frame
    void Update()
    {
        //Position
        this.gameObject.transform.position = cameraTransform.position + distanceToCamera * cameraTransform.forward;

        //Orientation
        transform.LookAt(Camera.main.transform, Camera.main.transform.up);
        
        //Size
        float x = rectTransform.sizeDelta.x;
        float y = x / Camera.main.aspect;
        rectTransform.sizeDelta = new Vector2(x, y);

        //Scale
        float canvasY = 2f * distanceToCamera * Mathf.Tan((Camera.main.fieldOfView * Mathf.PI / 180f) / 2f);
        transform.localScale = Vector3.one * (canvasY / y);
    }

    private float movementSpeed = 20f; //per second
    private float opacitySpeed = 20f; //per second
    private float maxDistanceToCamera = 0.6f;
    private float minDistanceToCamera = 0.3f;
    private float maxTransparency = 0.3f;
    private float minTransparency = 0.0f;
    private bool IsMoving = false;
    private bool IsVisible = true;

    /*public void MoveToCamera()
    {
        if(IsVisible)
        {
            Debug.Log("Move to camera");
            StartCoroutine("MoveToCameraCoroutine");
            IsVisible = false;
        }
    }

    public IEnumerator MoveToCameraCoroutine()
    {
        if(!IsMoving)
        {
            IsMoving = true;

            while (distanceToCamera > minDistanceToCamera)
            {
                distanceToCamera = distanceToCamera - movementSpeed * Time.deltaTime;
                if (material.color.a > minTransparency)
                {
                    material.color = new Color(1f, 1f, 1f, material.color.a - opacitySpeed * Time.deltaTime);
                }
                yield return new WaitForSeconds(0.1f);
            }
            distanceToCamera = minDistanceToCamera;
            material.color = new Color(1f, 1f, 1f, minTransparency);
            collider.enabled = false;
        }

        IsMoving = false;
    }

    public void MoveToWorld()
    {
        if (!IsVisible)
        {
            Debug.Log("Move to world");
            StartCoroutine("MoveToWorldCoroutine");
            IsVisible = true;
        }
    }

    public IEnumerator MoveToWorldCoroutine()
    {
        if (!IsMoving)
        {
            IsMoving = true;
            while (distanceToCamera < maxDistanceToCamera)
            {
                distanceToCamera = distanceToCamera + movementSpeed * Time.deltaTime;
                if(material.color.a < maxTransparency)
                {
                    material.color = new Color(1f, 1f, 1f, material.color.a + opacitySpeed * Time.deltaTime);
                }

                yield return new WaitForSeconds(0.1f);
            }
            distanceToCamera = maxDistanceToCamera;
            material.color = new Color(1f, 1f, 1f, maxTransparency);
            collider.enabled = true;
        }

        IsMoving = false;
    }

    #region HeadsetView Listeners

    private static List<IHeadsetViewListener> listeners = new List<IHeadsetViewListener>();

    public static void RegisterListener(IHeadsetViewListener listener)
    {
        listeners.Add(listener);
    }

    public static void UnregisterListener(IHeadsetViewListener listener)
    {
        listeners.Remove(listener);
    }

    public static void UnregisterAllListeners()
    {
        listeners.Clear();
    }

    public static void NotifyListeners(Vector3 position)
    {
        foreach(IHeadsetViewListener listener in listeners)
        {
            listener.PointerClicked(position);
        }
    }
    
    #endregion HeadsetView Listeners

    #region IMixedRealityPointerHandler

    public void OnPointerDown(MixedRealityPointerEventData eventData)
    {
        NotifyListeners(eventData.Pointer.Result.Details.Point);
    }

    public void OnPointerDragged(MixedRealityPointerEventData eventData) { }

    public void OnPointerUp(MixedRealityPointerEventData eventData)
    {
        NotifyListeners(eventData.Pointer.Result.Details.Point);
    }

    public void OnPointerClicked(MixedRealityPointerEventData eventData) { }

    #endregion IMixedRealityPointerHandler

    #region IStateHandler

    public void HandleState(StateEvent stateEvent)
    {*/
        /*if(stateEvent.state != stateEvent.oldState && stateEvent.state != Settings.State.Normal)
        {
            MoveToWorld();
        }
        else if (stateEvent.state != stateEvent.oldState && stateEvent.state == Settings.State.Normal)
        {
            MoveToCamera();
        }*/
    /*}

    #endregion IStateHandler*/
}
