﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Microsoft.MixedReality.Toolkit.UI;

public class PressableAnnotationMap : MonoBehaviour
{
    private Vector3 scale3 = new Vector3(3f, 3f, 3f);
    private Vector3 scale1 = new Vector3(1.5f, 1.5f, 1.5f);
    bool fullscreen = false;
    private GameObject[] annotations;

    public void scaleAnnotation()
    {
        annotations = GameObject.FindGameObjectsWithTag("Annotation");

        if (fullscreen)
        {
            foreach(GameObject annotation in annotations)
            {
                //reenable interact with them
            }
            this.gameObject.transform.localScale = scale1;
            this.GetComponent<AnnotationExtension>().isOpen = false;
            HerokuTextTablet.received = true;
            fullscreen = false;
        } else
        {
            foreach (GameObject annotation in annotations)
            {

                if (annotation.name != this.name)
                {
                    annotation.GetComponent<Interactable>().enabled = false;
                }
                //disable interaction with them
            }
            // this enable interacting
            this.gameObject.transform.localScale = scale3;
            //z position should be higher
            //enable video player and start loop
            this.GetComponent<AnnotationExtension>().isOpen = true;
            HerokuNodeTest.received = true;
            fullscreen = true;
        }

    }


}
