﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestScript : MonoBehaviour
{
    private GameObject[] annotations;

    // Start is called before the first frame update
    void Start()
    {
        if (annotations == null)
            annotations = GameObject.FindGameObjectsWithTag("Annotation");

        annotations[0].GetComponent<AnnotationExtension>().isLeft = true;
        annotations[1].GetComponent<AnnotationExtension>().isLeft = false;
        annotations[2].GetComponent<AnnotationExtension>().isLeft = true;
        annotations[3].GetComponent<AnnotationExtension>().isLeft = false;
        annotations[4].GetComponent<AnnotationExtension>().isLeft = false;
        annotations[5].GetComponent<AnnotationExtension>().isLeft = true;

        foreach (GameObject annotation in annotations)
        {
            Debug.Log(annotation.GetComponent<AnnotationExtension>().isLeft);
        }

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
