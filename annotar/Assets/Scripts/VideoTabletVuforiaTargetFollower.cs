﻿using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class VideoTabletVuforiaTargetFollower : MonoBehaviour
{
    //Markers are aligns (Position of the device relative to each marker)
    //- Left: X+0.07m Y+0.0m Z-0.16m
    //- Middle: X+0.0m Y+0.0m Z-0.16m
    //- Right: X-0.07m Y+0.0m Z-0.16m

    private ImageTargetBehaviour imageTargetLeftBehaviour;
    private ImageTargetBehaviour imageTargetMiddleBehaviour;
    private ImageTargetBehaviour imageTargetRightBehaviour;

    private Transform imageTargetLeftTransform;
    private Transform imageTargetMiddleTransform;
    private Transform imageTargetRightTransform;

    public GameObject imageTargetLeft;
    public GameObject imageTargetMiddle;
    public GameObject imageTargetRight;

    public Vector3 offsetImageTargetLeft = new Vector3(0.07f, 0.0f, -0.163f);
    public Vector3 offsetImageTargetMiddle = new Vector3(0.0f, 0.0f, -0.163f);
    public Vector3 offsetImageTargetRight = new Vector3(-0.07f, 0.0f, -0.163f);
    //private Vector3 offsetImageTargetLeft = new Vector3(0.07f, 0.0f, -0.11f);
    //private Vector3 offsetImageTargetMiddle = new Vector3(0.0f, 0.0f, -0.11f);
    //private Vector3 offsetImageTargetRight = new Vector3(-0.07f, 0.0f, -0.11f);

    private Vector3 devicePosition = Vector3.zero;
    private Quaternion deviceRotation = Quaternion.identity;

    void Start()
    {
        imageTargetLeftBehaviour = imageTargetLeft.GetComponent<ImageTargetBehaviour>();
        imageTargetMiddleBehaviour = imageTargetMiddle.GetComponent<ImageTargetBehaviour>();
        imageTargetRightBehaviour = imageTargetRight.GetComponent<ImageTargetBehaviour>();

        imageTargetLeftTransform = imageTargetLeft.transform;
        imageTargetMiddleTransform = imageTargetMiddle.transform;
        imageTargetRightTransform = imageTargetRight.transform;
    }

    void Update()
    {
        List<Vector3> listDevicePositions = new List<Vector3>();
        List<Quaternion> listDeviceRotations = new List<Quaternion>();

        //If the left image is tracked, we get its rotation and the hypotetical position of the phone
        if (imageTargetLeftBehaviour.CurrentStatus == ImageTargetBehaviour.Status.TRACKED)
        {
            listDevicePositions.Add(imageTargetLeftTransform.position + offsetImageTargetLeft.x * imageTargetLeftTransform.right + offsetImageTargetLeft.y * imageTargetLeftTransform.up); //+ offsetImageTargetLeft.z * imageTargetLeftTransform.forward);
            listDeviceRotations.Add(imageTargetLeftTransform.rotation);
        }

        //If the middle image is tracked, we get its rotation and the hypotetical position of the phone
        if (imageTargetMiddleBehaviour.CurrentStatus == ImageTargetBehaviour.Status.TRACKED)
        {
            listDevicePositions.Add(imageTargetMiddleTransform.position + offsetImageTargetMiddle.x * imageTargetMiddleTransform.right + offsetImageTargetMiddle.y * imageTargetMiddleTransform.up); //+ offsetImageTargetMiddle.z * imageTargetMiddleTransform.forward);
            listDeviceRotations.Add(imageTargetMiddleTransform.rotation);
        }

        //If the right image is tracked, we get its rotation and the hypotetical position of the phone
        if (imageTargetRightBehaviour.CurrentStatus == ImageTargetBehaviour.Status.TRACKED)
        {
            listDevicePositions.Add(imageTargetRightTransform.position + offsetImageTargetRight.x * imageTargetRightTransform.right + offsetImageTargetRight.y * imageTargetRightTransform.up); //+ offsetImageTargetRight.z * imageTargetRightTransform.forward);
            listDeviceRotations.Add(imageTargetRightTransform.rotation);
        }

        //If one image, at least, is tracked
        if(listDevicePositions .Count != 0)
        {
            devicePosition = listDevicePositions[0];
            deviceRotation = listDeviceRotations[0];

            //We calculate the mean value of their rotation and the hypotetical positions of the phone
            for (int i = 1; i < listDevicePositions.Count; i++)
            {
                devicePosition = Vector3.Lerp(listDevicePositions[i], devicePosition, 1 / (i + 1));
                deviceRotation = Quaternion.Lerp(listDeviceRotations[i], deviceRotation, 1 / (i + 1));
            }
        }

        //We place the phone
        this.transform.position = devicePosition;
        this.transform.rotation = deviceRotation;
        this.transform.Translate(Vector3.forward * offsetImageTargetMiddle.z);
    }
}
