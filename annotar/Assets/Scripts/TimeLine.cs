﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeLine : MonoBehaviour
{
    public Slider slider;

    public void SetTime(float time)
    {
        slider.value = time;
    }

    public void SetMaxValue(float time)
    {
        slider.maxValue = time;
    }
}
