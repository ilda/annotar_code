﻿using UnityEngine;
using System.Collections;
using System.Linq;
using UnityEngine.UI;
using Vuforia;
using UnityEngine.Windows.WebCam;

public class PhotoCaptureHandler : MonoBehaviour
{
    public GameObject camera;
    public static GameObject pressableAnnotation;
    //public GameObject annotation;
    // GameObject imageHolder;
    public GameObject dialogConfirm;
    public GameObject dialogTakingPicture;
    private PhotoCapture photoCaptureObject = null;
    Texture2D targetTexture = null;
    private float waitTime;

    public void StartPhotoCapture()
    {
        InvokeRepeating("BlinkDialogTakingPicture", 0, 0.3f);
        camera.GetComponent<VuforiaBehaviour>().enabled = false;
        PhotoCapture.CreateAsync(false, OnPhotoCaptureCreated);
    }

    void OnPhotoCaptureCreated(PhotoCapture captureObject)
    {
        photoCaptureObject = captureObject;

        Resolution cameraResolution = PhotoCapture.SupportedResolutions.OrderByDescending((res) => res.width * res.height).First();

        CameraParameters c = new CameraParameters();
        c.hologramOpacity = 0.0f;
        c.cameraResolutionWidth = cameraResolution.width;
        c.cameraResolutionHeight = cameraResolution.height;
        c.pixelFormat = CapturePixelFormat.BGRA32;

        // Activate the camera
        photoCaptureObject.StartPhotoModeAsync(c, delegate(PhotoCapture.PhotoCaptureResult result)
        {
            // Take a picture
            photoCaptureObject.TakePhotoAsync(OnCapturedPhotoToMemory);
        });
    }

    void OnCapturedPhotoToMemory(PhotoCapture.PhotoCaptureResult result, PhotoCaptureFrame photoCaptureFrame)
    {
        if (result.success)
        {
            CancelInvoke();
            if (dialogTakingPicture.activeSelf)
                dialogTakingPicture.SetActive(false);

            StartCoroutine(ShowAndHide(dialogConfirm, 3.0f)); // 1 second
            Debug.Log("photo taken!");
            // Create our Texture2D for use and set the correct resolution
            Resolution cameraResolution = PhotoCapture.SupportedResolutions.OrderByDescending((res) => res.width * res.height).First();
            Texture2D targetTexture = new Texture2D(cameraResolution.width, cameraResolution.height);
            // Copy the raw image data into our target texture
            photoCaptureFrame.UploadImageDataToTexture(targetTexture);
            // Do as we wish with the texture such as apply it to a material, etc.

            Sprite spriteImage = Sprite.Create(targetTexture, new Rect(0.0f, 0.0f, targetTexture.width, targetTexture.height), new Vector2(0.5f, 0.5f), 100.0f);
            GameObject annotation = pressableAnnotation.transform.GetChild(0).gameObject;

            GameObject shade = annotation.transform.GetChild(0).gameObject;
            GameObject imageHolder = shade.transform.GetChild(0).gameObject;
            imageHolder.GetComponent<UnityEngine.UI.Image>().sprite = spriteImage;
            pressableAnnotation.SetActive(true);
            HerokuTextTablet.captureTaken = true;
        }
        // Clean up
        photoCaptureObject.StopPhotoModeAsync(OnStoppedPhotoMode);
        //camera.GetComponent<VuforiaBehaviour>().enabled = true;

    }

    void OnStoppedPhotoMode(PhotoCapture.PhotoCaptureResult result)
    {
        photoCaptureObject.Dispose();
        photoCaptureObject = null;
    }

    IEnumerator ShowAndHide(GameObject go, float delay)
    {
        go.SetActive(true);
        yield return new WaitForSeconds(delay);
        go.SetActive(false);
    }

    void BlinkDialogTakingPicture()
    {
        if (dialogTakingPicture.activeSelf)
            dialogTakingPicture.SetActive(false);
        else
            dialogTakingPicture.SetActive(true);
    }

}
