﻿using UnityEngine;
using System.Collections;
using System.Linq;
using UnityEngine.UI;
using Vuforia;
using UnityEngine.Windows.WebCam;

public class FramePhotoCaptureHandler : MonoBehaviour
{
    public GameObject camera;
    public static GameObject pressableAnnotation;
    //public GameObject annotation;
    // GameObject imageHolder;
    private PhotoCapture photoCaptureObject = null;
    Texture2D targetTexture = null;

    public void StartPhotoCapture()
    {
        camera.GetComponent<VuforiaBehaviour>().enabled = false;
        PhotoCapture.CreateAsync(false, OnPhotoCaptureCreated);
    }

    void OnPhotoCaptureCreated(PhotoCapture captureObject)
    {
        photoCaptureObject = captureObject;

        Resolution cameraResolution = PhotoCapture.SupportedResolutions.OrderByDescending((res) => res.width * res.height).First();

        CameraParameters c = new CameraParameters();
        c.hologramOpacity = 0.0f;
        c.cameraResolutionWidth = cameraResolution.width;
        c.cameraResolutionHeight = cameraResolution.height;
        c.pixelFormat = CapturePixelFormat.BGRA32;

        // Activate the camera
        photoCaptureObject.StartPhotoModeAsync(c, delegate (PhotoCapture.PhotoCaptureResult result)
        {
            // Take a picture
            photoCaptureObject.TakePhotoAsync(OnCapturedPhotoToMemory);
        });
    }

    void OnCapturedPhotoToMemory(PhotoCapture.PhotoCaptureResult result, PhotoCaptureFrame photoCaptureFrame)
    {
        if (result.success)
        {
            Debug.Log("photo taken!");
            // Create our Texture2D for use and set the correct resolution
            Resolution cameraResolution = PhotoCapture.SupportedResolutions.OrderByDescending((res) => res.width * res.height).First();
            Texture2D targetTexture = new Texture2D(cameraResolution.width, cameraResolution.height);
            // Copy the raw image data into our target texture
            photoCaptureFrame.UploadImageDataToTexture(targetTexture);
            // Do as we wish with the texture such as apply it to a material, etc.

            Sprite spriteImage = Sprite.Create(targetTexture, new Rect(0.0f, 0.0f, targetTexture.width, targetTexture.height), new Vector2(0.5f, 0.5f), 100.0f);
            GameObject annotation = pressableAnnotation.transform.GetChild(0).gameObject;
            GameObject shade = annotation.transform.GetChild(0).gameObject;
            GameObject imageHolder = shade.transform.GetChild(0).gameObject;
            GameObject playHolder = imageHolder.transform.GetChild(0).gameObject;

            imageHolder.GetComponent<UnityEngine.UI.Image>().sprite = spriteImage;
            playHolder.SetActive(true);
            VideoCaptureHandler.previewAnnotation = pressableAnnotation;
            //annotation.SetActive(true);
            //HerokuTextTablet.captureTaken = true;

        }
        // Clean up
        photoCaptureObject.StopPhotoModeAsync(OnStoppedPhotoMode);
        //camera.GetComponent<VuforiaBehaviour>().enabled = true;

    }

    void OnStoppedPhotoMode(PhotoCapture.PhotoCaptureResult result)
    {
        photoCaptureObject.Dispose();
        photoCaptureObject = null;
    }
}
