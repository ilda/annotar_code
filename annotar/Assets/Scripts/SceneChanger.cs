﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class SceneChanger : MonoBehaviour
{
    public void Text()
    {
        SceneManager.LoadScene("TextTabletScene");
    }
    public void Map()
    {
        SceneManager.LoadScene("TextMapScene");
    }
    public void Video()
    {
        SceneManager.LoadScene("VideoTabletScene");
    }
}