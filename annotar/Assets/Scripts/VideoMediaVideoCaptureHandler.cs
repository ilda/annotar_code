﻿using UnityEngine;
using System.Collections;
using System.Linq;
using UnityEngine.UI;
using Vuforia;
using UnityEngine.Windows.WebCam;

public class VideoMediaVideoCaptureHandler : MonoBehaviour
{
    public GameObject camera;
    public GameObject imageHolder;
    public GameObject pictureButton;
    public GameObject startButton;
    public GameObject stopButton;
    public GameObject dialogRec;
    public static GameObject previewAnnotation;
    public static GameObject annotationBar;


    VideoCapture m_VideoCapture = null;

    public void StartRecordingVideo()
    {
        camera.GetComponent<VuforiaBehaviour>().enabled = false;
        startButton.active = false;
        stopButton.active = true;
        VideoCapture.CreateAsync(false, OnVideoCaptureCreated);
    }

    public void StopRecordingVideo()
    {
        stopButton.active = false;
        startButton.active = true;
        m_VideoCapture.StopRecordingAsync(OnStoppedRecordingVideo);
        camera.GetComponent<VuforiaBehaviour>().enabled = true;

    }

    public void enableRecordingDialog()
    {
        dialogRec.SetActive(true);
    }

    public void disableRecordingDialog()
    {
        dialogRec.SetActive(false);
    }

    public void disableStartButton()
    {
        startButton.SetActive(false);
    }

    public void enableStartButton()
    {
        startButton.SetActive(true);
    }

    public void disableStopButton()
    {
        stopButton.SetActive(false);
    }

    public void enableStopButton()
    {
        stopButton.SetActive(true);
    }

    public void disablePictureButton()
    {
        pictureButton.SetActive(false);
    }

    public void enablePictureButton()
    {
        pictureButton.SetActive(true);
    }

    public void sendConfirmation()
    {
        previewAnnotation.SetActive(true);
        annotationBar.SetActive(true);
        HerokuVideoTablet.captureTaken = true;
    }

    void OnVideoCaptureCreated(VideoCapture videoCapture)
    {
        if (videoCapture != null)
        {
            m_VideoCapture = videoCapture;

            Resolution cameraResolution = VideoCapture.SupportedResolutions.OrderByDescending((res) => res.width * res.height).First();
            float cameraFramerate = VideoCapture.GetSupportedFrameRatesForResolution(cameraResolution).OrderByDescending((fps) => fps).First();

            CameraParameters cameraParameters = new CameraParameters();
            cameraParameters.hologramOpacity = 0.0f;
            cameraParameters.frameRate = cameraFramerate;
            cameraParameters.cameraResolutionWidth = cameraResolution.width;
            cameraParameters.cameraResolutionHeight = cameraResolution.height;
            cameraParameters.pixelFormat = CapturePixelFormat.BGRA32;

            m_VideoCapture.StartVideoModeAsync(cameraParameters,
                                                VideoCapture.AudioState.None,
                                                OnStartedVideoCaptureMode);
        }
        else
        {
            Debug.LogError("Failed to create VideoCapture Instance!");
        }
    }

    void OnStartedVideoCaptureMode(VideoCapture.VideoCaptureResult result)
    {
        if (result.success)
        {
            string filename = string.Format("MyVideo_{0}.mp4", Time.time);
            string filepath = System.IO.Path.Combine(Application.persistentDataPath, filename);

            m_VideoCapture.StartRecordingAsync(filepath, OnStartedRecordingVideo);
        }
    }

    void OnStartedRecordingVideo(VideoCapture.VideoCaptureResult result)
    {
        Debug.Log("Started Recording Video!");
    }

    void OnStoppedRecordingVideo(VideoCapture.VideoCaptureResult result)
    {
        Debug.Log("Stopped Recording Video!");
        m_VideoCapture.StopVideoModeAsync(OnStoppedVideoCaptureMode);
    }

    void OnStoppedVideoCaptureMode(VideoCapture.VideoCaptureResult result)
    {
        m_VideoCapture.Dispose();
        m_VideoCapture = null;
    }
}
