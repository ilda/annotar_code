﻿using Socket.Quobject.SocketIoClientDotNet.Client;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

using UnityEngine.UI;
using Microsoft.MixedReality.Toolkit.UI;
using TMPro;
using Newtonsoft.Json;

public class HerokuTextTablet : MonoBehaviour
{
    private QSocket socket;
    //public GameObject colorObject;
    private string color1;
    private Color color2;
    GameObject gameObjectColor;
    private GameObject[] annotations;
    List<string> colors = new List<string>();
    List<Annotation> receivedAnnotations = new List<Annotation>();
    List<GameObject> displayedAnnotations = new List<GameObject>();

    private GameObject currAnn;
    private string currPos;
    public double maxScreen = 750;
    public double minY = 0.1; //prev 0.07
    public double maxY = -0.1;


    public GameObject phoneHolder;

    private Vector3 devicePosition = Vector3.zero;
    private Quaternion deviceRotation = Quaternion.identity;
    private Vector3 offsetAnnotation = new Vector3(0.07f, 0.0f, 0.07f);
    public Quaternion offsetRotation = new Quaternion(-1, -1, -1, 1);
    public static bool received = false;
    bool receivedImageAnnotation = false;
    bool receivedHoloAnnotation = false;
    public GameObject captureMenu;

    string str;

    public GameObject notePrefab;
    public GameObject HoloNotePrefab;

    private ImageAnnotation imgAnnotation;
    private Annotation hololensAnnotation;
    public static bool captureTaken = false;

    bool receivedLoadText = false;
    bool receivedLoadMap = false;
    bool receivedLoadVideo = false;
    bool receivedLoadConsumeText = false;
    bool receivedLoadConsumeMap = false;
    bool receivedLoadConsumeVideo = false;

    void Start()
    {
        captureMenu.SetActive(false);
        if (annotations == null)
            annotations = GameObject.FindGameObjectsWithTag("Annotation");

        foreach (GameObject annotation in annotations)
        {
            annotation.SetActive(false);
        }


        Debug.Log("start");
        socket = IO.Socket("http://mobile-headset.herokuapp.com/");

        socket.On(QSocket.EVENT_CONNECT, () =>
        {
            Debug.Log("Connected");
            socket.Emit("chat", "test");
        });

        socket.On("chat", data =>
        {
            Debug.Log("data : " + data);
        });

        socket.On("ann color", annotationsJSON =>
        {
            receivedAnnotations = JsonConvert.DeserializeObject<List<Annotation>>(annotationsJSON.ToString());
            received = true;
            //color1 = annotationsJSON.ToString();
            //color1 = color.ToString();

            //empty the array when?
            //colors.Add(color1);

            /*if (ColorUtility.TryParseHtmlString("#ff00ff", out colorRGB))
            {
                logo.GetComponent<SpriteRenderer>().color = colorRGB;
            }*/
            //ColorUtility.TryParseHtmlString("#FF0090", out color1);

            //logo.GetComponent<SpriteRenderer>().color = color1;
            //Debug.Log("colorRGB : " + newCol.ToString());
            //Debug.Log("color : " + color.ToString());
            //Debug.Log(logo.GetComponent<TMPro.TextMeshProUGUI>().text);


            //Debug.Log("color : " + color);
            //Debug.Log("colorRGB : " + colorRGB.ToString());
        });

        socket.On("img annotation", imageAnnotation =>
        {
            imgAnnotation = JsonConvert.DeserializeObject<ImageAnnotation>(imageAnnotation.ToString());
            receivedImageAnnotation = true;
        });

        socket.On("holo annotation", holoAnnotation =>
        {
            hololensAnnotation = JsonConvert.DeserializeObject<Annotation>(holoAnnotation.ToString());
            receivedHoloAnnotation = true;
        });

        socket.On("loadTextScene", confirmation =>
        {
            receivedLoadText = true;
        });

        socket.On("loadMapScene", confirmation =>
        {
            receivedLoadMap = true;
        });

        socket.On("loadVideoScene", confirmation =>
        {
            receivedLoadVideo = true;
        });

        socket.On("loadConsumeTextScene", confirmation =>
        {
            receivedLoadConsumeText = true;
        });

        socket.On("loadConsumeMapScene", confirmation =>
        {
            receivedLoadConsumeMap = true;
        });

        socket.On("loadConsumeVideoScene", confirmation =>
        {
            receivedLoadConsumeVideo = true;
        });

    }

    void Update()
    {
        if (receivedLoadText)
        {
            receivedLoadText = false;
            SceneManager.LoadScene("TextTabletScene");
        }

        if (receivedLoadMap)
        {
            receivedLoadMap = false;
            SceneManager.LoadScene("TextMapScene");
        }

        if (receivedLoadVideo)
        {
            receivedLoadVideo = false;
            SceneManager.LoadScene("VideoTabletScene");
        }

        if (receivedLoadConsumeText)
        {
            receivedLoadConsumeText = false;
            SceneManager.LoadScene("ConsumeTextScene");
        }

        if (receivedLoadConsumeMap)
        {
            receivedLoadConsumeMap = false;
            SceneManager.LoadScene("ConsumeMapScene");
        }

        if (receivedLoadConsumeVideo)
        {
            receivedLoadConsumeVideo = false;
            SceneManager.LoadScene("ConsumeVideoScene");
        }


        if (phoneHolder.transform.hasChanged)
        {
            received = true;
            phoneHolder.transform.hasChanged = false;
        }
        /*socket.On("start", start =>
        {

            //empty the array at the beginning of each scroll
            colors.Clear(); 
            
            // if the color is still there, then check position and instead of deactivating move it
            foreach (GameObject annotation in annotations)
            {
                annotation.SetActive(false);
            }

        });*/

        //DebuLog("color : " + color1);
        /*str ="";
        foreach (var x in receivedAnnotations)
        {
            str = str + " " + x.color.ToString();
        }
        Debug.Log(str);*/

        //gameObjects = GameObject.FindGameObjectsWithTag("Player");
        //Debug.Log(gameObjects[0].name);
        /*testing for changing color to an object
        if (color1 == "#ff00ff")
            //wrong, needs to be something like: GameObject.Find(color1)
            gameObjectColor.SetActive(true);

        if (ColorUtility.TryParseHtmlString(color1, out color2))
            Debug.Log("color : " + color2.ToString());
            colorObject.GetComponent<MeshRenderer>().material.color = color2;
        */

        //when you receive a message
        if (received)
        {
            bool oppositeColumn;

            if (displayedAnnotations.Count == 0)
            {
                if (receivedAnnotations.Count == 1) //inserting one annotation
                {
                    Debug.Log("inserting one annotation: " + receivedAnnotations[0].id);
                    currAnn = FindAnnotation(receivedAnnotations[0].id);
                    currPos = receivedAnnotations[0].position;
                    InsertAnnotation(currAnn, currPos, 1, true);
                }
                else 
                {
                    bool tempLeft = true;
                    Debug.Log("initialize the view with multiple annotations");

                    //initialize the view with multiple annotations
                    for (int j = 0; j < receivedAnnotations.Count; j++)
                    {
                        currAnn = FindAnnotation(receivedAnnotations[j].id);
                        currPos = receivedAnnotations[j].position;

                        if (j == 0)
                        {
                            InsertAnnotation(currAnn, currPos, 1, tempLeft);
                        }
                        else
                        {
                            InsertAnnotation(currAnn, currPos, 3, tempLeft);
                        }
                        tempLeft = !tempLeft;
                    }
                }
            }
            /*else if (receivedAnnotations.Count == 0)
            {
                foreach (GameObject ann in displayedAnnotations)
                {
                    ann.GetComponent<Canvas>().enabled = false;
                    displayedAnnotations.Remove(ann);
                }
                //remove everything from displayed
            }*/
            else if (displayedAnnotations.Count == receivedAnnotations.Count)
            {
                if (displayedAnnotations[0].name == receivedAnnotations[0].id)
                {
                    for (int j = 0; j < receivedAnnotations.Count; j++)
                    {
                        currAnn = FindAnnotation(receivedAnnotations[j].id);
                        currPos = receivedAnnotations[j].position;
                        double currPosDouble = double.Parse(currPos, System.Globalization.CultureInfo.InvariantCulture);
                        setAnnotationPosition(currAnn, currPosDouble, currAnn.GetComponent<AnnotationExtension>().isLeft); //send the position
                    }

                    //update position
                }
                else
                {
                    if (receivedAnnotations.Count == 1)
                    {
                        DeleteAnnotation(displayedAnnotations[0], 1);
                        currAnn = FindAnnotation(receivedAnnotations[0].id);
                        currPos = receivedAnnotations[0].position;

                        InsertAnnotation(currAnn, currPos, 1, true);
                    }
                    else
                    {
                        if (displayedAnnotations[0].name == receivedAnnotations[1].id)
                        {
                            Debug.Log("Case 1");

                            DeleteAnnotation(displayedAnnotations[displayedAnnotations.Count-1], 1);
                            currAnn = FindAnnotation(receivedAnnotations[0].id);
                            currPos = receivedAnnotations[0].position;
                            oppositeColumn = displayedAnnotations[0].GetComponent<AnnotationExtension>().isLeft;
                            InsertAnnotation(currAnn, currPos, 1, !oppositeColumn);
                            //case 1
                        }
                        else
                        {
                            Debug.Log("Case 2");

                            DeleteAnnotation(displayedAnnotations[0], 1);
                            currAnn = FindAnnotation(receivedAnnotations[receivedAnnotations.Count-1].id);
                            currPos = receivedAnnotations[receivedAnnotations.Count - 1].position;
                            oppositeColumn = displayedAnnotations[displayedAnnotations.Count - 1].GetComponent<AnnotationExtension>().isLeft;

                            InsertAnnotation(currAnn, currPos, 1, !oppositeColumn);
                            //case 2
                        }
                    }
                }

            }
            else if (displayedAnnotations.Count < receivedAnnotations.Count)
            {
                if (displayedAnnotations[0].name == receivedAnnotations[0].id)
                {
                    Debug.Log("Case 4");

                    currAnn = FindAnnotation(receivedAnnotations[receivedAnnotations.Count-1].id);
                    currPos = receivedAnnotations[receivedAnnotations.Count - 1].position;

                    oppositeColumn = displayedAnnotations[displayedAnnotations.Count-1].GetComponent<AnnotationExtension>().isLeft;
                    InsertAnnotation(currAnn, currPos, 3, !oppositeColumn);

                    //case 4
                }
                else
                {
                    Debug.Log("Case 3");

                    currAnn = FindAnnotation(receivedAnnotations[0].id);
                    currPos = receivedAnnotations[0].position;

                    oppositeColumn = displayedAnnotations[0].GetComponent<AnnotationExtension>().isLeft;
                    InsertAnnotation(currAnn, currPos, 2, !oppositeColumn);
                    //case 3
                }
            }
            else if (displayedAnnotations.Count > receivedAnnotations.Count)
            {
                if (displayedAnnotations.Count == 1)
                {
                    Debug.Log("edge case only one to delete");
                    currAnn = displayedAnnotations[0];
                    DeleteAnnotation(currAnn, 1);
                }
                else
                {
                    if (displayedAnnotations[0].name == receivedAnnotations[0].id)
                    {
                        Debug.Log("Case 6");
                        currAnn = displayedAnnotations[displayedAnnotations.Count - 1];
                        DeleteAnnotation(currAnn, 3);
                        //case 6
                    }
                    else
                    {
                        Debug.Log("Case 5");
                        currAnn = displayedAnnotations[0];
                        DeleteAnnotation(currAnn, 2);
                        //case 5
                    }
                }

            }

            received = false;
            
        }

        if (receivedImageAnnotation)
        {
            GameObject newPressableAnnotation = Instantiate(notePrefab, Vector3.zero, Quaternion.identity) as GameObject;
            GameObject newAnnotation = newPressableAnnotation.transform.GetChild(0).gameObject;
            //GameObject newAnnotation = Instantiate(notePrefab, Vector3.zero, Quaternion.identity) as GameObject;
            Debug.Log("id : " + imgAnnotation.id);
            newPressableAnnotation.name = imgAnnotation.id;
            GameObject shade = newAnnotation.transform.GetChild(0).gameObject;
            if (ColorUtility.TryParseHtmlString(imgAnnotation.color, out color2))
                shade.GetComponent<Image>().color = color2;
            byte[] imageBytes = Convert.FromBase64String(imgAnnotation.img);
            Texture2D tex = new Texture2D(2, 2);
            tex.LoadImage(imageBytes);
            Sprite spriteImage = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f);

            GameObject imageHolder = shade.transform.GetChild(0).gameObject;
            imageHolder.GetComponent<Image>().sprite = spriteImage;

            //update the annotations array
            annotations = GameObject.FindGameObjectsWithTag("Annotation");

            //send confirmation of creating the new annotation
            socket.Emit("annotationOK", "ok");

            receivedImageAnnotation = false;
        }

        if (receivedHoloAnnotation)
        {
            GameObject newPressableAnnotation = Instantiate(notePrefab, Vector3.zero, Quaternion.identity) as GameObject;
            GameObject newAnnotation = newPressableAnnotation.transform.GetChild(0).gameObject;
            //GameObject newAnnotation = Instantiate(HoloNotePrefab, Vector3.zero, Quaternion.identity) as GameObject;
            Debug.Log("id : " + hololensAnnotation.id);

            newPressableAnnotation.name = hololensAnnotation.id;
            GameObject shade = newAnnotation.transform.GetChild(0).gameObject;
            if (ColorUtility.TryParseHtmlString(hololensAnnotation.color, out color2))
                shade.GetComponent<Image>().color = color2;
            GameObject imageHolder = shade.transform.GetChild(0).gameObject;
            GameObject playHolder = imageHolder.transform.GetChild(0).gameObject;
            playHolder.SetActive(false);
            newPressableAnnotation.SetActive(false);
            //byte[] imageBytes = Convert.FromBase64String(imgAnnotation.img);

            //Texture2D tex = new Texture2D(2, 2);
            //tex.LoadImage(imageBytes);



            //Sprite spriteImage = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f);

            //GameObject imageHolder = shade.transform.GetChild(0).gameObject;
            //imageHolder.GetComponent<Image>().sprite = spriteImage;
            captureMenu.SetActive(true);

            PhotoCaptureHandler.pressableAnnotation = newPressableAnnotation;
            FramePhotoCaptureHandler.pressableAnnotation = newPressableAnnotation;

            receivedHoloAnnotation = false;

        }

        if (captureTaken)
        {
            //update the annotations array
            annotations = GameObject.FindGameObjectsWithTag("Annotation");

            //send confirmation of creating the new annotation
            socket.Emit("HololensAnnotationOK", "ok");
            captureMenu.SetActive(false);
            captureTaken = false;
        }

    }


    private void InsertAnnotation(GameObject ann, string currPos, int caseSwitch, bool isLeft)
    {

        ann.SetActive(true);
        //ann.GetComponent<Canvas>().enabled = true;
        ann.GetComponent<AnnotationExtension>().isLeft = isLeft;
        double currPosDouble = double.Parse(currPos, System.Globalization.CultureInfo.InvariantCulture);
        setAnnotationPosition(ann, currPosDouble, isLeft);
        switch (caseSwitch)
        {
            case 1:
                Debug.Log("Case 1, add only this annotation");
                displayedAnnotations.Add(ann);
                break;
            case 2:
                Debug.Log("Case 2, add annotation on top");
                displayedAnnotations.Insert(0, ann);
                break;
            case 3:
                Debug.Log("Case 3, add annotation below");
                displayedAnnotations.Add(ann);
                break;
            case 4:
                Debug.Log("Case 4, add annotation in the middle");
                break;
            default:
                Debug.Log("Default case");
                break;
        }
    }

    private void DeleteAnnotation(GameObject ann, int caseSwitch)
    {
        ann.SetActive(false);
        //ann.GetComponent<Canvas>().enabled = false;
        displayedAnnotations.Remove(ann);

        switch (caseSwitch)
        {
            case 1:
                Debug.Log("Case 1, remove only this annotation");
                break;
            case 2:
                Debug.Log("Case 2, remove annotation on top");
                break;
            case 3:
                Debug.Log("Case 3, remove annotation below");
                break;
            case 4:
                Debug.Log("Case 4, remove annotation in the middle");
                break;
            default:
                Debug.Log("Default case");
                break;
        }
    }

    private void setAnnotationPosition(GameObject ann, double currPos, bool isLeft) 
    {
        if (!ann.GetComponent<AnnotationExtension>().isOpen)
        {
            double percentOnScreen = (double)Math.Round((double)(100 * currPos) / maxScreen);
            float offsetY = (float)((percentOnScreen * (maxY - minY) / 100) + minY);
            if (isLeft)
            {
                offsetAnnotation.z = -0.15f;
            }
            else
            {
                offsetAnnotation.z = -0.21f;
            }

            offsetAnnotation.x = offsetY;
            ann.transform.position = phoneHolder.transform.position + offsetAnnotation.x * phoneHolder.transform.right + offsetAnnotation.z * phoneHolder.transform.forward;
            ann.transform.rotation = phoneHolder.transform.rotation * offsetRotation;
        } else
        {
            offsetAnnotation.x = 0.01f;
            offsetAnnotation.z = -0.18f;
            offsetAnnotation.y = 0.04f;

            ann.transform.position = phoneHolder.transform.position + offsetAnnotation.x * phoneHolder.transform.right + offsetAnnotation.z * phoneHolder.transform.forward + offsetAnnotation.y * phoneHolder.transform.up;
            ann.transform.rotation = phoneHolder.transform.rotation * offsetRotation;
        }

    }

    private GameObject FindAnnotation(string id){
        foreach (GameObject annotation in annotations)
        {
            if (annotation.name == id)
            {
                return annotation;
                break;
            }
        }
        return null;
    }

    private void OnDestroy()
    {
        socket.Disconnect();
    }

    public class Annotation
    {
        public string id;
        public string color;
        public string position;
    }

    public class ImageAnnotation
    {
        public string id;
        public string color;
        public string position;
        public string img;
    }
}