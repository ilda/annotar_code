﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;
using TMPro;



public class MenuTesting : MonoBehaviour
{
    private ImageTargetBehaviour imageTargetLeftBehaviour;
    private ImageTargetBehaviour imageTargetMiddleBehaviour;
    private ImageTargetBehaviour imageTargetRightBehaviour;
    public GameObject imageTargetLeft;
    public GameObject imageTargetMiddle;
    public GameObject imageTargetRight;
    public GameObject ann1;
    public GameObject ann2;
    public GameObject ann3;
    public TextMeshPro textMeshProL;
    public TextMeshPro textMeshProM;
    public TextMeshPro textMeshProR;


    // Start is called before the first frame update
    void Start()
    {
        imageTargetLeftBehaviour = imageTargetLeft.GetComponent<ImageTargetBehaviour>();
        imageTargetMiddleBehaviour = imageTargetMiddle.GetComponent<ImageTargetBehaviour>();
        imageTargetRightBehaviour = imageTargetRight.GetComponent<ImageTargetBehaviour>();
    }

    // Update is called once per frame
    void Update()
    {
        //textMeshProL.text = imageTargetLeftBehaviour.CurrentStatus.ToString();
        //textMeshProM.text = imageTargetMiddleBehaviour.CurrentStatus.ToString();
        //textMeshProR.text = imageTargetRightBehaviour.CurrentStatus.ToString();
        textMeshProL.text = ann1.transform.position.z.ToString();
        textMeshProM.text = ann2.transform.position.z.ToString();
        textMeshProR.text = ann3.transform.position.z.ToString();

    }
}
