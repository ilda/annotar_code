﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnnotationPosition1 : MonoBehaviour
{
    public GameObject phoneHolder;

    private Vector3 devicePosition = Vector3.zero;
    private Quaternion deviceRotation = Quaternion.identity;
    private Vector3 offsetAnnotation = new Vector3(0.13f, 0.0f, 0.04f);
    private Quaternion offsetRotation = new Quaternion(0, 90, 90, 0);


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        devicePosition = phoneHolder.transform.position + offsetAnnotation.x * phoneHolder.transform.right + offsetAnnotation.z * phoneHolder.transform.forward;
        deviceRotation = phoneHolder.transform.rotation * offsetRotation;

        this.transform.position = devicePosition;
        this.transform.rotation = deviceRotation;
    }
}
