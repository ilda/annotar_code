﻿using Socket.Quobject.SocketIoClientDotNet.Client;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Microsoft.MixedReality.Toolkit.UI;
using TMPro;
using Newtonsoft.Json;

public class HerokuVideo : MonoBehaviour
{
    private QSocket socket;
    //public GameObject colorObject;
    private string color1;
    private Color color2;
    GameObject gameObjectColor;
    private GameObject[] annotations;
    List<string> colors = new List<string>();
    List<Annotation> receivedAnnotations = new List<Annotation>();
    List<GameObject> displayedAnnotations = new List<GameObject>();
    private GameObject[] annotationBars;

    private GameObject currAnn;
    private int currPos;
    //private double maxScreen = 750;
    private double minY = 0.1;
    private double maxY = -0.1;

    public GameObject timelineCanvas;

    public GameObject phoneHolder;

    private Vector3 devicePosition = Vector3.zero;
    private Quaternion deviceRotation = Quaternion.identity;
    private Vector3 offsetAnnotation = new Vector3(0.07f, 0.0f, 0.07f);
    private Quaternion offsetRotation = new Quaternion(90, 0, 0, 90);
    private Quaternion timelineRotation = new Quaternion(0, 90, 90, 0);

    private Vector3 scale75perc = new Vector3(0.75f, 0.75f, 0f);
    private Vector3 scale1 = new Vector3(1f, 1f, 1f);


    bool received = false;
    //bool receivedCurrTime = false;
    bool receivedDuration = false;
    bool receivedAnnSetup = false;

    List<Annotation> setUpAnnotations = new List<Annotation>();
    Color grayBarColor = new Color(0.64f, 0.64f, 0.64f, 1f);


    string str;

    public TimeLine timeline;

    private float currentTime;
    private float durationTime;

    string str1;
    string str2;


    void Start()
    {
        timelineCanvas.SetActive(true);
        

        if (annotations == null)
            annotations = GameObject.FindGameObjectsWithTag("Annotation");

        if(annotationBars == null)
            annotationBars = GameObject.FindGameObjectsWithTag("AnnotationBar");

        foreach (GameObject annotation in annotations)
        {
            annotation.GetComponent<Canvas>().enabled = false;
        }

        Debug.Log("start");
        socket = IO.Socket("http://mobile-headset.herokuapp.com/");

        socket.On(QSocket.EVENT_CONNECT, () =>
        {
            Debug.Log("Connected");
            socket.Emit("chat", "test");
        });

        socket.On("chat", data =>
        {
            Debug.Log("data : " + data);
        });

        socket.On("ann color", annotationsJSON =>
        {
            Debug.Log("received annotations");
            receivedAnnotations = JsonConvert.DeserializeObject<List<Annotation>>(annotationsJSON.ToString());
            received = true;
            //color1 = annotationsJSON.ToString();
            //color1 = color.ToString();

            //empty the array when?
            //colors.Add(color1);

            /*if (ColorUtility.TryParseHtmlString("#ff00ff", out colorRGB))
            {
                logo.GetComponent<SpriteRenderer>().color = colorRGB;
            }*/
            //ColorUtility.TryParseHtmlString("#FF0090", out color1);

            //logo.GetComponent<SpriteRenderer>().color = color1;
            //Debug.Log("colorRGB : " + newCol.ToString());
            //Debug.Log("color : " + color.ToString());
            //Debug.Log(logo.GetComponent<TMPro.TextMeshProUGUI>().text);


            //Debug.Log("color : " + color);
            //Debug.Log("colorRGB : " + colorRGB.ToString());
        });

        socket.On("setup annotations", annotationsJSON =>
        {
            Debug.Log("received setup annotations");
            setUpAnnotations = JsonConvert.DeserializeObject<List<Annotation>>(annotationsJSON.ToString());
            receivedAnnSetup = true;
        });

        socket.On("videocurrenttime", current =>
        {
            str1 = current.ToString();
            currentTime = float.Parse(str1);
            
        });

        socket.On("videoduration", duration =>
        {
            str2 = duration.ToString();
            durationTime = float.Parse(str2);
            receivedDuration = true;
        });

    }

    void Update()
    {
        setTimelinePosition();

        /*socket.On("start", start =>
        {

            //empty the array at the beginning of each scroll
            colors.Clear(); 
            
            // if the color is still there, then check position and instead of deactivating move it
            foreach (GameObject annotation in annotations)
            {
                annotation.SetActive(false);
            }

        });*/

        //DebuLog("color : " + color1);
        /*str ="";
        foreach (var x in receivedAnnotations)
        {
            str = str + " " + x.color.ToString();
        }
        Debug.Log(str);*/

        //gameObjects = GameObject.FindGameObjectsWithTag("Player");
        //Debug.Log(gameObjects[0].name);
        /*testing for changing color to an object
        if (color1 == "#ff00ff")
            //wrong, needs to be something like: GameObject.Find(color1)
            gameObjectColor.SetActive(true);

        if (ColorUtility.TryParseHtmlString(color1, out color2))
            Debug.Log("color : " + color2.ToString());
            colorObject.GetComponent<MeshRenderer>().material.color = color2;
        */

        //when you receive a message
        if (received)
        {
            timeline.SetTime(currentTime);
            Debug.Log("Received a message");
            bool oppositeColumn;

            if (displayedAnnotations.Count == 0)
            {
                if (receivedAnnotations.Count == 1) //inserting one annotation
                {
                    currAnn = FindAnnotation(receivedAnnotations[0].id);
                    currPos = 0;
                    InsertAnnotation(currAnn, currPos, 1, true);
                }
                else
                {
                    Debug.Log("Initialize the view with multiple annotations");
                    bool tempLeft = true;
                    //initialize the view with multiple annotations
                    for (int j = 0; j < receivedAnnotations.Count; j++)
                    {
                        currAnn = FindAnnotation(receivedAnnotations[j].id);
                        currPos = j;
                        currAnn.GetComponent<AnnotationExtension>().bar.GetComponent<Image>().color = currAnn.GetComponent<AnnotationExtension>().color;
                        if(receivedAnnotations[j].active == 1)
                        {
                            currAnn.gameObject.transform.localScale = scale1;
                            currAnn.GetComponent<CanvasGroup>().alpha = 1f;
                        }
                        else {
                            currAnn.gameObject.transform.localScale = scale75perc;
                            currAnn.GetComponent<CanvasGroup>().alpha = 0.7f;
                        }

                        if (j == 0)
                        {
                            InsertAnnotation(currAnn, currPos, 1, tempLeft);
                        }
                        else
                        {
                            InsertAnnotation(currAnn, currPos, 3, tempLeft);
                        }
                        tempLeft = !tempLeft;
                    }
                }
            }
            /*else if (receivedAnnotations.Count == 0)
            {
                foreach (GameObject ann in displayedAnnotations)
                {
                    ann.GetComponent<Canvas>().enabled = false;
                    displayedAnnotations.Remove(ann);
                }
                //remove everything from displayed
            }*/
            else if (displayedAnnotations.Count == receivedAnnotations.Count)
            {
                if (displayedAnnotations[0].name == receivedAnnotations[0].id)
                {
                    Debug.Log("same count and same first name");
                    for (int j = 0; j < receivedAnnotations.Count; j++)
                    {
                        currAnn = FindAnnotation(receivedAnnotations[j].id);
                        currPos = j;
                        currAnn.GetComponent<AnnotationExtension>().bar.GetComponent<Image>().color = currAnn.GetComponent<AnnotationExtension>().color;
                        if (receivedAnnotations[j].active == 1)
                        {
                            currAnn.gameObject.transform.localScale = scale1;
                            currAnn.GetComponent<CanvasGroup>().alpha = 1f;
                        }
                        else
                        {
                            currAnn.gameObject.transform.localScale = scale75perc;
                            currAnn.GetComponent<CanvasGroup>().alpha = 0.7f;

                        }
                    }

                    //update position
                }
                else
                {
                    if (receivedAnnotations.Count == 1)
                    {
                        Debug.Log("receivedAnnotations.Count == 1");
                        displayedAnnotations[0].GetComponent<AnnotationExtension>().bar.GetComponent<Image>().color = grayBarColor;

                        DeleteAnnotation(displayedAnnotations[0], 1);
                        currAnn = FindAnnotation(receivedAnnotations[0].id);
                        currPos = 0;

                        InsertAnnotation(currAnn, 0, 1, true);
                    }
                    else
                    {
                        Debug.Log("first disp id: " + displayedAnnotations[0].name);
                        foreach(var annotat in receivedAnnotations)
                        {
                            Debug.Log("rec id: " + annotat.id);
                        }
                        //remove the first, add the next at the end and relayout all of them
                        displayedAnnotations[0].GetComponent<AnnotationExtension>().bar.GetComponent<Image>().color = grayBarColor;
                        DeleteAnnotation(displayedAnnotations[0], 1);
                        currAnn = FindAnnotation(receivedAnnotations[receivedAnnotations.Count - 1].id);
                        currPos = receivedAnnotations.Count - 1;
                        oppositeColumn = displayedAnnotations[displayedAnnotations.Count - 1].GetComponent<AnnotationExtension>().isLeft;
                        InsertAnnotation(currAnn, currPos, 1, !oppositeColumn);
                        for (int j = 0; j < receivedAnnotations.Count; j++)
                        {
                            currAnn = FindAnnotation(receivedAnnotations[j].id);
                            currPos = j;
                            currAnn.GetComponent<AnnotationExtension>().bar.GetComponent<Image>().color = currAnn.GetComponent<AnnotationExtension>().color;
                            if (receivedAnnotations[j].active == 1)
                            {
                                currAnn.gameObject.transform.localScale = scale1;
                                currAnn.GetComponent<CanvasGroup>().alpha = 1f;
                            }
                            else
                            {
                                currAnn.gameObject.transform.localScale = scale75perc;
                                currAnn.GetComponent<CanvasGroup>().alpha = 0.7f;
                            }
                            setAnnotationPosition(currAnn, currPos, currAnn.GetComponent<AnnotationExtension>().isLeft); //send the position
                        }
                    }
                }

            }
            /*else if (displayedAnnotations.Count < receivedAnnotations.Count)
            {
                if (displayedAnnotations[0].name == receivedAnnotations[0].id)
                {
                    Debug.Log("Case 4");

                    currAnn = FindAnnotation(receivedAnnotations[receivedAnnotations.Count - 1].id);
                    currPos = receivedAnnotations.Count - 1;

                    oppositeColumn = displayedAnnotations[displayedAnnotations.Count - 1].GetComponent<AnnotationExtension>().isLeft;
                    InsertAnnotation(currAnn, currPos, 3, !oppositeColumn);

                    //case 4
                }
                else
                {
                    Debug.Log("Case 3");

                    currAnn = FindAnnotation(receivedAnnotations[0].id);
                    currPos = 0;

                    oppositeColumn = displayedAnnotations[0].GetComponent<AnnotationExtension>().isLeft;
                    InsertAnnotation(currAnn, currPos, 2, !oppositeColumn);
                    //case 3
                }
            }
            else if (displayedAnnotations.Count > receivedAnnotations.Count)
            {
                if (displayedAnnotations.Count == 1)
                {
                    Debug.Log("edge case only one to delete");
                    currAnn = displayedAnnotations[0];
                    DeleteAnnotation(currAnn, 1);
                }
                else
                {
                    if (displayedAnnotations[0].name == receivedAnnotations[0].id)
                    {
                        Debug.Log("Case 6");
                        currAnn = displayedAnnotations[displayedAnnotations.Count - 1];
                        DeleteAnnotation(currAnn, 3);
                        //case 6
                    }
                    else
                    {
                        Debug.Log("Case 5");
                        currAnn = displayedAnnotations[0];
                        DeleteAnnotation(currAnn, 2);
                        //case 5
                    }
                }

            }*/

            received = false;

        }

        if (receivedDuration)
        {
            timeline.SetMaxValue(durationTime);
            receivedDuration = false;
        }

        if (receivedAnnSetup)
        {
            GameObject tempAnn;
            GameObject tempBar;
            Color tempColor;
            foreach (Annotation ann in setUpAnnotations) {
                tempAnn = FindAnnotation(ann.id);
                tempBar = GameObject.Find("bar" + ann.id);
                ColorUtility.TryParseHtmlString(ann.color, out tempColor);
                tempAnn.GetComponent<AnnotationExtension>().time = ann.time;
                tempAnn.GetComponent<AnnotationExtension>().interval = ann.interval;
                tempAnn.GetComponent<AnnotationExtension>().bar = tempBar;
                tempAnn.GetComponent<AnnotationExtension>().color = tempColor;
                setAnnotationBarPosition(tempAnn, ann.time, ann.interval);

                tempBar.GetComponent<Image>().color = grayBarColor;

            }
            receivedAnnSetup = false;
        }
    }


    private void InsertAnnotation(GameObject ann, int currPos, int caseSwitch, bool isLeft)
    {
        ann.GetComponent<Canvas>().enabled = true;
        ann.GetComponent<AnnotationExtension>().isLeft = isLeft;
        //double currPosDouble = double.Parse(currPos, System.Globalization.CultureInfo.InvariantCulture);
        setAnnotationPosition(ann, currPos, isLeft);
        switch (caseSwitch)
        {
            case 1:
                Debug.Log("Case 1, add only this annotation");
                displayedAnnotations.Add(ann);
                break;
            case 2:
                Debug.Log("Case 2, add annotation on top");
                displayedAnnotations.Insert(0, ann);
                break;
            case 3:
                Debug.Log("Case 3, add annotation below");
                displayedAnnotations.Add(ann);
                break;
            case 4:
                Debug.Log("Case 4, add annotation in the middle");
                break;
            default:
                Debug.Log("Default case");
                break;
        }
    }

    private void DeleteAnnotation(GameObject ann, int caseSwitch)
    {
        ann.GetComponent<Canvas>().enabled = false;
        displayedAnnotations.Remove(ann);

        switch (caseSwitch)
        {
            case 1:
                Debug.Log("Case 1, remove only this annotation");
                break;
            case 2:
                Debug.Log("Case 2, remove annotation on top");
                break;
            case 3:
                Debug.Log("Case 3, remove annotation below");
                break;
            case 4:
                Debug.Log("Case 4, remove annotation in the middle");
                break;
            default:
                Debug.Log("Default case");
                break;
        }
    }

    private void setTimelinePosition()
    {

        offsetAnnotation.x = 0.07f;

        timelineCanvas.transform.position = phoneHolder.transform.position + offsetAnnotation.x * phoneHolder.transform.right;
        timelineCanvas.transform.rotation = phoneHolder.transform.rotation * timelineRotation;

    }

    private void setAnnotationBarPosition(GameObject ann, float time, float interval)
    {
        Debug.Log("id: " + ann.name);
        double percentTop = (double)Math.Round((double)(100 * (time + 1)) / durationTime);
        double percentBottom = (double)Math.Round((double)(100 * ((durationTime-(time + interval)) + 1)) / durationTime);
        double max = timelineCanvas.GetComponent<RectTransform>().rect.height;
        Debug.Log("max: " + max);
        double min = 0;

        float offsetTop = (float)((percentTop * (max - min) / 100) + min);
        Debug.Log("offsetTop: " + offsetTop);

        float offsetBottom = (float)((percentBottom * (max - min) / 100) + min);
        Debug.Log("offsetBottom: " + offsetBottom);

        RectTransform rt = ann.GetComponent<AnnotationExtension>().bar.GetComponent<RectTransform>();

        rt.offsetMax = new Vector2(rt.offsetMax.x, -offsetTop);
        rt.offsetMin = new Vector2(rt.offsetMin.x, offsetBottom);

    }

    private void setAnnotationPosition(GameObject ann, double currPos, bool isLeft)
    {

        double percentOnScreen = (double)Math.Round((double)(100 * (currPos+1)) / receivedAnnotations.Count);
        float offsetY = (float)((percentOnScreen * (maxY - minY) / 100) + minY);
        if (isLeft)
        {
            offsetAnnotation.x = 0.12f;
        }
        else
        {
            offsetAnnotation.x = 0.18f;
        }

        offsetAnnotation.z = offsetY;
        ann.transform.position = phoneHolder.transform.position + offsetAnnotation.x * phoneHolder.transform.right + offsetAnnotation.z * phoneHolder.transform.forward;
        ann.transform.rotation = phoneHolder.transform.rotation * offsetRotation;
    }

    private GameObject FindAnnotation(string id)
    {
        foreach (GameObject annotation in annotations)
        {
            if (annotation.name == id)
            {
                return annotation;
                break;
            }
        }
        return null;
    }

    private void OnDestroy()
    {
        socket.Disconnect();
    }

    public class Annotation
    {
        public string id;
        public int time;
        public int interval;
        public string color;
        public int active;
    }
}
