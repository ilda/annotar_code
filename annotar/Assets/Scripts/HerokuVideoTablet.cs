﻿using Socket.Quobject.SocketIoClientDotNet.Client;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Microsoft.MixedReality.Toolkit.UI;
using TMPro;
using Newtonsoft.Json;

public class HerokuVideoTablet : MonoBehaviour
{
    private QSocket socket;
    //public GameObject colorObject;
    private string color1;
    private Color color2;
    GameObject gameObjectColor;
    private GameObject[] annotations;
    List<string> colors = new List<string>();
    List<Annotation> receivedAnnotations = new List<Annotation>();
    List<GameObject> displayedAnnotations = new List<GameObject>();
    private GameObject[] annotationBars;

    private GameObject currAnn;
    private int currPos;
    //private double maxScreen = 750;
    private double minY = 0.20;
    private double maxY = -0.13;

    public GameObject timelineCanvas;

    public GameObject phoneHolder;

    private Vector3 devicePosition = Vector3.zero;
    private Quaternion deviceRotation = Quaternion.identity;
    private Vector3 offsetAnnotation = new Vector3(0.07f, 0.0f, 0.07f);
    public Quaternion offsetRotation = new Quaternion(90, 0, 0, 90);
    private Quaternion timelineRotation = new Quaternion(0, 90, 90, 0);

    private Vector3 scale75perc = new Vector3(0.75f, 0.75f, 0f);
    private Vector3 scale1 = new Vector3(1f, 1f, 1f);


    public static bool received = false;
    //bool receivedCurrTime = false;
    bool receivedDuration = false;
    bool receivedAnnSetup = false;

    List<Annotation> setUpAnnotations = new List<Annotation>();
    Color grayBarColor = new Color(0.64f, 0.64f, 0.64f, 1f);


    string str;

    public TimeLine timeline;

    private float currentTime;
    private float durationTime;
    bool receivedCurrentTime;

    string str1;
    string str2;

    bool receivedImageAnnotation = false;
    bool receivedHoloAnnotation = false;
    public GameObject captureMenu;

    public GameObject notePrefab;
    public GameObject HoloNotePrefab;
    public GameObject annBarPrefab;

    private ImageAnnotation imgAnnotation;
    private Annotation hololensAnnotation;
    public static bool captureTaken = false;

    bool receivedVideoRelayout = false;
    List<Annotation> receivedRelayoutAnnotations = new List<Annotation>();

    bool receivedLoadText = false;
    bool receivedLoadMap = false;
    bool receivedLoadVideo = false;
    bool receivedLoadConsumeText = false;
    bool receivedLoadConsumeMap = false;
    bool receivedLoadConsumeVideo = false;


    void Start()
    {
        captureMenu.SetActive(false);

        timelineCanvas.SetActive(true);
        

        if (annotations == null)
            annotations = GameObject.FindGameObjectsWithTag("Annotation");

        if(annotationBars == null)
            annotationBars = GameObject.FindGameObjectsWithTag("AnnotationBar");

        foreach (GameObject annotation in annotations)
        {
            annotation.SetActive(false);
        }

        Debug.Log("start");
        socket = IO.Socket("http://mobile-headset.herokuapp.com/");

        socket.On(QSocket.EVENT_CONNECT, () =>
        {
            Debug.Log("Connected");
            socket.Emit("chat", "test");
        });

        socket.On("chat", data =>
        {
            Debug.Log("data : " + data);
        });

        socket.On("ann color", annotationsJSON =>
        {
            Debug.Log("received annotations");
            receivedAnnotations = JsonConvert.DeserializeObject<List<Annotation>>(annotationsJSON.ToString());
            received = true;
            //color1 = annotationsJSON.ToString();
            //color1 = color.ToString();

            //empty the array when?
            //colors.Add(color1);

            /*if (ColorUtility.TryParseHtmlString("#ff00ff", out colorRGB))
            {
                logo.GetComponent<SpriteRenderer>().color = colorRGB;
            }*/
            //ColorUtility.TryParseHtmlString("#FF0090", out color1);

            //logo.GetComponent<SpriteRenderer>().color = color1;
            //Debug.Log("colorRGB : " + newCol.ToString());
            //Debug.Log("color : " + color.ToString());
            //Debug.Log(logo.GetComponent<TMPro.TextMeshProUGUI>().text);


            //Debug.Log("color : " + color);
            //Debug.Log("colorRGB : " + colorRGB.ToString());
        });

        socket.On("videorelayout", videoannotationsJSON =>
        {
            Debug.Log("received annotations to relayout");
            receivedRelayoutAnnotations = JsonConvert.DeserializeObject<List<Annotation>>(videoannotationsJSON.ToString());
            receivedVideoRelayout = true;
        });

        socket.On("setup annotations", annotationsJSON =>
        {
            Debug.Log("received setup annotations");
            setUpAnnotations = JsonConvert.DeserializeObject<List<Annotation>>(annotationsJSON.ToString());
            receivedAnnSetup = true;
        });

        socket.On("videocurrenttime", current =>
        {
            str1 = current.ToString();
            currentTime = float.Parse(str1);
            receivedCurrentTime = true;
        });

        socket.On("videoduration", duration =>
        {
            str2 = duration.ToString();
            durationTime = float.Parse(str2);
            receivedDuration = true;
        });

        socket.On("img annotation", imageAnnotation =>
        {
            Debug.Log("received image annotation");
            imgAnnotation = JsonConvert.DeserializeObject<ImageAnnotation>(imageAnnotation.ToString());
            receivedImageAnnotation = true;
        });

        socket.On("holo annotation", holoAnnotation =>
        {
            hololensAnnotation = JsonConvert.DeserializeObject<Annotation>(holoAnnotation.ToString());
            receivedHoloAnnotation = true;
        });

        socket.On("loadTextScene", confirmation =>
        {
            receivedLoadText = true;
        });

        socket.On("loadMapScene", confirmation =>
        {
            receivedLoadMap = true;
        });

        socket.On("loadVideoScene", confirmation =>
        {
            receivedLoadVideo = true;
        });

        socket.On("loadConsumeTextScene", confirmation =>
        {
            receivedLoadConsumeText = true;
        });

        socket.On("loadConsumeMapScene", confirmation =>
        {
            receivedLoadConsumeMap = true;
        });

        socket.On("loadConsumeVideoScene", confirmation =>
        {
            receivedLoadConsumeVideo = true;
        });


    }

    void Update()
    {
        if (receivedLoadText)
        {
            receivedLoadText = false;
            SceneManager.LoadScene("TextTabletScene");
        }

        if (receivedLoadMap)
        {
            receivedLoadMap = false;
            SceneManager.LoadScene("TextMapScene");
        }

        if (receivedLoadVideo)
        {
            receivedLoadVideo = false;
            SceneManager.LoadScene("VideoTabletScene");
        }

        if (receivedLoadConsumeText)
        {
            receivedLoadConsumeText = false;
            SceneManager.LoadScene("ConsumeTextScene");
        }

        if (receivedLoadConsumeMap)
        {
            receivedLoadConsumeMap = false;
            SceneManager.LoadScene("ConsumeMapScene");
        }

        if (receivedLoadConsumeVideo)
        {
            receivedLoadConsumeVideo = false;
            SceneManager.LoadScene("ConsumeVideoScene");
        }



        if (phoneHolder.transform.hasChanged)
        {
            //received = true;
            phoneHolder.transform.hasChanged = false;
        }

        setTimelinePosition();

        /*socket.On("start", start =>
        {

            //empty the array at the beginning of each scroll
            colors.Clear(); 
            
            // if the color is still there, then check position and instead of deactivating move it
            foreach (GameObject annotation in annotations)
            {
                annotation.SetActive(false);
            }

        });*/

        //DebuLog("color : " + color1);
        /*str ="";
        foreach (var x in receivedAnnotations)
        {
            str = str + " " + x.color.ToString();
        }
        Debug.Log(str);*/

        //gameObjects = GameObject.FindGameObjectsWithTag("Player");
        //Debug.Log(gameObjects[0].name);
        /*testing for changing color to an object
        if (color1 == "#ff00ff")
            //wrong, needs to be something like: GameObject.Find(color1)
            gameObjectColor.SetActive(true);

        if (ColorUtility.TryParseHtmlString(color1, out color2))
            Debug.Log("color : " + color2.ToString());
            colorObject.GetComponent<MeshRenderer>().material.color = color2;
        */
        if (receivedVideoRelayout)
        {
            receivedAnnotations.Clear();

            foreach (GameObject ann in displayedAnnotations)
            {
                DeleteAnnotation(ann, 1);
                ann.GetComponent<AnnotationExtension>().bar.GetComponent<Image>().color = grayBarColor;
            }
            displayedAnnotations.Clear();

            Debug.Log("Reinitialize the view with multiple annotations");
            bool tempLeft = true;
            //initialize the view with multiple annotations
            for (int j = 0; j < receivedRelayoutAnnotations.Count; j++)
            {
                currAnn = FindAnnotation(receivedRelayoutAnnotations[j].id);
                Debug.Log("id:" + receivedRelayoutAnnotations[j].id);

                currPos = j;
                currAnn.GetComponent<AnnotationExtension>().bar.GetComponent<Image>().color = currAnn.GetComponent<AnnotationExtension>().color;
                if (receivedRelayoutAnnotations[j].active == 1)
                {
                    currAnn.gameObject.transform.localScale = scale1;
                    currAnn.GetComponent<CanvasGroup>().alpha = 1f;
                }
                else
                {
                    currAnn.gameObject.transform.localScale = scale75perc;
                    currAnn.GetComponent<CanvasGroup>().alpha = 0.7f;
                }

                if (j == 0)
                {
                    InsertAnnotation(currAnn, currPos, 1, tempLeft);
                }
                else
                {
                    InsertAnnotation(currAnn, currPos, 3, tempLeft);
                }
                tempLeft = !tempLeft;
            }
            receivedRelayoutAnnotations.Clear();
            receivedVideoRelayout = false;
        }

        if (receivedCurrentTime)
        {
            timeline.SetTime(currentTime);
            receivedCurrentTime = false;
        }

        //when you receive a message
        if (received)
        {
            Debug.Log("Received the -received- message");
            bool oppositeColumn;

            if (displayedAnnotations.Count == 0)
            {
                if (receivedAnnotations.Count == 1) //inserting one annotation
                {
                    currAnn = FindAnnotation(receivedAnnotations[0].id);
                    currPos = 0;
                    InsertAnnotation(currAnn, currPos, 1, true);
                }
                else if (receivedAnnotations.Count > 1)
                {
                    Debug.Log("Initialize the view with multiple annotations");
                    bool tempLeft = true;
                    //initialize the view with multiple annotations
                    for (int j = 0; j < receivedAnnotations.Count; j++)
                    {
                        currAnn = FindAnnotation(receivedAnnotations[j].id);
                        currPos = j;
                        currAnn.GetComponent<AnnotationExtension>().bar.GetComponent<Image>().color = currAnn.GetComponent<AnnotationExtension>().color;
                        if(receivedAnnotations[j].active == 1)
                        {
                            currAnn.gameObject.transform.localScale = scale1;
                            currAnn.GetComponent<CanvasGroup>().alpha = 1f;
                        }
                        else {
                            currAnn.gameObject.transform.localScale = scale75perc;
                            currAnn.GetComponent<CanvasGroup>().alpha = 0.7f;
                        }

                        if (j == 0)
                        {
                            InsertAnnotation(currAnn, currPos, 1, tempLeft);
                        }
                        else
                        {
                            InsertAnnotation(currAnn, currPos, 3, tempLeft);
                        }
                        tempLeft = !tempLeft;
                    }
                }
            }
            /*else if (receivedAnnotations.Count == 0)
            {
                foreach (GameObject ann in displayedAnnotations)
                {
                    ann.GetComponent<Canvas>().enabled = false;
                    displayedAnnotations.Remove(ann);
                }
                //remove everything from displayed
            }*/
            else if (displayedAnnotations.Count == receivedAnnotations.Count)
            {
                if (displayedAnnotations[0].name == receivedAnnotations[0].id)
                {
                    Debug.Log("same count and same first name");
                    for (int j = 0; j < receivedAnnotations.Count; j++)
                    {
                        currAnn = FindAnnotation(receivedAnnotations[j].id);
                        currPos = j;
                        currAnn.GetComponent<AnnotationExtension>().bar.GetComponent<Image>().color = currAnn.GetComponent<AnnotationExtension>().color;
                        if (receivedAnnotations[j].active == 1)
                        {
                            currAnn.gameObject.transform.localScale = scale1;
                            currAnn.GetComponent<CanvasGroup>().alpha = 1f;
                        }
                        else
                        {
                            currAnn.gameObject.transform.localScale = scale75perc;
                            currAnn.GetComponent<CanvasGroup>().alpha = 0.7f;

                        }
                    }

                    //update position
                }
                else
                {
                    if (receivedAnnotations.Count == 1)
                    {
                        Debug.Log("receivedAnnotations.Count == 1");
                        displayedAnnotations[0].GetComponent<AnnotationExtension>().bar.GetComponent<Image>().color = grayBarColor;

                        DeleteAnnotation(displayedAnnotations[0], 1);
                        currAnn = FindAnnotation(receivedAnnotations[0].id);
                        currPos = 0;

                        InsertAnnotation(currAnn, 0, 1, true);
                    }
                    else
                    {
                        Debug.Log("first disp id: " + displayedAnnotations[0].name);
                        foreach(var annotat in receivedAnnotations)
                        {
                            Debug.Log("rec id: " + annotat.id);
                        }
                        //remove the first, add the next at the end and relayout all of them
                        displayedAnnotations[0].GetComponent<AnnotationExtension>().bar.GetComponent<Image>().color = grayBarColor;
                        DeleteAnnotation(displayedAnnotations[0], 1);
                        currAnn = FindAnnotation(receivedAnnotations[receivedAnnotations.Count - 1].id);
                        currPos = receivedAnnotations.Count - 1;
                        oppositeColumn = displayedAnnotations[displayedAnnotations.Count - 1].GetComponent<AnnotationExtension>().isLeft;
                        InsertAnnotation(currAnn, currPos, 1, !oppositeColumn);
                        for (int j = 0; j < receivedAnnotations.Count; j++)
                        {
                            currAnn = FindAnnotation(receivedAnnotations[j].id);
                            currPos = j;
                            currAnn.GetComponent<AnnotationExtension>().bar.GetComponent<Image>().color = currAnn.GetComponent<AnnotationExtension>().color;
                            if (receivedAnnotations[j].active == 1)
                            {
                                currAnn.gameObject.transform.localScale = scale1;
                                currAnn.GetComponent<CanvasGroup>().alpha = 1f;
                            }
                            else
                            {
                                currAnn.gameObject.transform.localScale = scale75perc;
                                currAnn.GetComponent<CanvasGroup>().alpha = 0.7f;
                            }
                            setAnnotationPosition(currAnn, currPos, currAnn.GetComponent<AnnotationExtension>().isLeft); //send the position
                        }
                    }
                }

            }
            /*else if (displayedAnnotations.Count < receivedAnnotations.Count)
            {
                if (displayedAnnotations[0].name == receivedAnnotations[0].id)
                {
                    Debug.Log("Case 4");

                    currAnn = FindAnnotation(receivedAnnotations[receivedAnnotations.Count - 1].id);
                    currPos = receivedAnnotations.Count - 1;

                    oppositeColumn = displayedAnnotations[displayedAnnotations.Count - 1].GetComponent<AnnotationExtension>().isLeft;
                    InsertAnnotation(currAnn, currPos, 3, !oppositeColumn);

                    //case 4
                }
                else
                {
                    Debug.Log("Case 3");

                    currAnn = FindAnnotation(receivedAnnotations[0].id);
                    currPos = 0;

                    oppositeColumn = displayedAnnotations[0].GetComponent<AnnotationExtension>().isLeft;
                    InsertAnnotation(currAnn, currPos, 2, !oppositeColumn);
                    //case 3
                }
            }
            else if (displayedAnnotations.Count > receivedAnnotations.Count)
            {
                if (displayedAnnotations.Count == 1)
                {
                    Debug.Log("edge case only one to delete");
                    currAnn = displayedAnnotations[0];
                    DeleteAnnotation(currAnn, 1);
                }
                else
                {
                    if (displayedAnnotations[0].name == receivedAnnotations[0].id)
                    {
                        Debug.Log("Case 6");
                        currAnn = displayedAnnotations[displayedAnnotations.Count - 1];
                        DeleteAnnotation(currAnn, 3);
                        //case 6
                    }
                    else
                    {
                        Debug.Log("Case 5");
                        currAnn = displayedAnnotations[0];
                        DeleteAnnotation(currAnn, 2);
                        //case 5
                    }
                }

            }*/

            received = false;

        }

        if (receivedDuration)
        {
            timeline.SetMaxValue(durationTime);
            receivedDuration = false;
        }

        if (receivedAnnSetup)
        {

            GameObject tempAnn;
            GameObject tempBar;
            Color tempColor;
            if(setUpAnnotations.Count != 0) {
                foreach (Annotation ann in setUpAnnotations)
                {
                    tempAnn = FindAnnotation(ann.id);
                    tempBar = GameObject.Find("bar" + ann.id);
                    ColorUtility.TryParseHtmlString(ann.color, out tempColor);
                    tempAnn.GetComponent<AnnotationExtension>().time = ann.time;
                    tempAnn.GetComponent<AnnotationExtension>().interval = ann.interval;
                    tempAnn.GetComponent<AnnotationExtension>().bar = tempBar;
                    tempAnn.GetComponent<AnnotationExtension>().color = tempColor;
                    setAnnotationBarPosition(tempAnn, ann.time, ann.interval);

                    tempBar.GetComponent<Image>().color = grayBarColor;

                }
            }

            receivedAnnSetup = false;
        }

        if (receivedImageAnnotation)
        {
            Debug.Log("received image annotation");
            GameObject newPressableAnnotation = Instantiate(notePrefab, Vector3.zero, Quaternion.identity) as GameObject;
            GameObject newAnnotation = newPressableAnnotation.transform.GetChild(0).gameObject;
            //GameObject newAnnotation = Instantiate(HoloNotePrefab, Vector3.zero, Quaternion.identity) as GameObject;
            Debug.Log("id : " + imgAnnotation.id);

            newPressableAnnotation.name = imgAnnotation.id;

            GameObject shade = newAnnotation.transform.GetChild(0).gameObject;
            if (ColorUtility.TryParseHtmlString(imgAnnotation.color, out color2))
                shade.GetComponent<Image>().color = color2;
            byte[] imageBytes = Convert.FromBase64String(imgAnnotation.img);
            Texture2D tex = new Texture2D(2, 2);
            tex.LoadImage(imageBytes);
            Sprite spriteImage = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f);

            GameObject imageHolder = shade.transform.GetChild(0).gameObject;
            imageHolder.GetComponent<Image>().sprite = spriteImage;
            GameObject newAnnotationBar = Instantiate(annBarPrefab, timelineCanvas.transform) as GameObject;

            newAnnotationBar.name = "bar" + imgAnnotation.id;
            //newAnnotationBar.transform.parent = timelineCanvas.transform;

            newPressableAnnotation.GetComponent<AnnotationExtension>().time = imgAnnotation.time;
            newPressableAnnotation.GetComponent<AnnotationExtension>().interval = imgAnnotation.interval;
            newPressableAnnotation.GetComponent<AnnotationExtension>().bar = newAnnotationBar;
            newPressableAnnotation.GetComponent<AnnotationExtension>().color = color2;
            setAnnotationBarPosition(newPressableAnnotation, imgAnnotation.time, imgAnnotation.interval);

            newAnnotationBar.GetComponent<Image>().color = grayBarColor;

            //update the annotations array
            annotations = GameObject.FindGameObjectsWithTag("Annotation");

            //send confirmation of creating the new annotation
            socket.Emit("annotationOK", "ok");

            receivedImageAnnotation = false;
        }

        if (receivedHoloAnnotation)
        {
            GameObject newPressableAnnotation = Instantiate(notePrefab, Vector3.zero, Quaternion.identity) as GameObject;
            GameObject newAnnotation = newPressableAnnotation.transform.GetChild(0).gameObject;
            //GameObject newAnnotation = Instantiate(HoloNotePrefab, Vector3.zero, Quaternion.identity) as GameObject;
            Debug.Log("id : " + hololensAnnotation.id);

            newPressableAnnotation.name = hololensAnnotation.id;
            GameObject shade = newAnnotation.transform.GetChild(0).gameObject;
            if (ColorUtility.TryParseHtmlString(hololensAnnotation.color, out color2))
                shade.GetComponent<Image>().color = color2;
            GameObject imageHolder = shade.transform.GetChild(0).gameObject;
            GameObject playHolder = imageHolder.transform.GetChild(0).gameObject;
            playHolder.SetActive(false);

            GameObject newAnnotationBar = Instantiate(annBarPrefab, timelineCanvas.transform) as GameObject;

            newAnnotationBar.name = "bar" + hololensAnnotation.id;

            newPressableAnnotation.GetComponent<AnnotationExtension>().time = hololensAnnotation.time;
            newPressableAnnotation.GetComponent<AnnotationExtension>().interval = hololensAnnotation.interval;
            newPressableAnnotation.GetComponent<AnnotationExtension>().bar = newAnnotationBar;
            newPressableAnnotation.GetComponent<AnnotationExtension>().color = color2;
            setAnnotationBarPosition(newPressableAnnotation, hololensAnnotation.time, hololensAnnotation.interval);

            newAnnotationBar.GetComponent<Image>().color = grayBarColor;
            //newAnnotationBar.transform.parent = timelineCanvas.transform;
            //byte[] imageBytes = Convert.FromBase64String(imgAnnotation.img);

            //Texture2D tex = new Texture2D(2, 2);
            //tex.LoadImage(imageBytes);



            //Sprite spriteImage = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f);

            //GameObject imageHolder = shade.transform.GetChild(0).gameObject;
            //imageHolder.GetComponent<Image>().sprite = spriteImage;
            newPressableAnnotation.SetActive(false);
            newAnnotationBar.SetActive(false);

            captureMenu.SetActive(true);

            VideoMediaPhotoCaptureHandler.pressableAnnotation = newPressableAnnotation;
            VideoMediaFramePhotoCaptureHandler.pressableAnnotation = newPressableAnnotation;
            VideoMediaPhotoCaptureHandler.annotationBar = newAnnotationBar;
            VideoMediaFramePhotoCaptureHandler.annotationBar = newAnnotationBar;
            receivedHoloAnnotation = false;

        }

        if (captureTaken)
        {
            //update the annotations array
            annotations = GameObject.FindGameObjectsWithTag("Annotation");

            //send confirmation of creating the new annotation
            socket.Emit("HololensAnnotationOK", "ok");
            captureMenu.SetActive(false);
            captureTaken = false;
        }
    }


    private void InsertAnnotation(GameObject ann, int currPos, int caseSwitch, bool isLeft)
    {
        ann.SetActive(true);
        ann.GetComponent<AnnotationExtension>().isLeft = isLeft;
        //double currPosDouble = double.Parse(currPos, System.Globalization.CultureInfo.InvariantCulture);
        setAnnotationPosition(ann, currPos, isLeft);
        switch (caseSwitch)
        {
            case 1:
                Debug.Log("Case 1, add only this annotation");
                displayedAnnotations.Add(ann);
                break;
            case 2:
                Debug.Log("Case 2, add annotation on top");
                displayedAnnotations.Insert(0, ann);
                break;
            case 3:
                Debug.Log("Case 3, add annotation below");
                displayedAnnotations.Add(ann);
                break;
            case 4:
                Debug.Log("Case 4, add annotation in the middle");
                break;
            default:
                Debug.Log("Default case");
                break;
        }
    }

    private void DeleteAnnotation(GameObject ann, int caseSwitch)
    {
        ann.SetActive(false);
        displayedAnnotations.Remove(ann);

        switch (caseSwitch)
        {
            case 1:
                Debug.Log("Case 1, remove only this annotation");
                break;
            case 2:
                Debug.Log("Case 2, remove annotation on top");
                break;
            case 3:
                Debug.Log("Case 3, remove annotation below");
                break;
            case 4:
                Debug.Log("Case 4, remove annotation in the middle");
                break;
            default:
                Debug.Log("Default case");
                break;
        }
    }

    private void setTimelinePosition()
    {

        offsetAnnotation.x = 0.15f;

        timelineCanvas.transform.position = phoneHolder.transform.position + offsetAnnotation.x * phoneHolder.transform.right;
        timelineCanvas.transform.rotation = phoneHolder.transform.rotation * timelineRotation;

    }

    private void setAnnotationBarPosition(GameObject ann, float time, float interval)
    {
        Debug.Log("id: " + ann.name);
        //double percentTop = (double)Math.Round((double)(100 * (time + 1)) / durationTime);
        //double percentBottom = (double)Math.Round((double)(100 * ((durationTime-(time + interval)) + 1)) / durationTime);

        double percentTop = ((double)(100 * (time)) / durationTime);
        double percentBottom = ((double)(100 * ((durationTime - (time + interval)))) / durationTime);

        double max = timelineCanvas.GetComponent<RectTransform>().rect.height;
        Debug.Log("max: " + max);
        double min = 0;

        float offsetTop = (float)((percentTop * (max - min) / 100) + min);
        Debug.Log("offsetTop: " + offsetTop);

        float offsetBottom = (float)((percentBottom * (max - min) / 100) + min);
        Debug.Log("offsetBottom: " + offsetBottom);

        RectTransform rt = ann.GetComponent<AnnotationExtension>().bar.GetComponent<RectTransform>();

        //rt.transform.Rotate(0, 0, 0);
        //rt.anchoredPosition.z = 0;

        //float fixedRight = (float)(0.003667831);
        //float fixedLeft = (float)(-0.003667831);

        //rt.offsetMax = new Vector2(-fixedRight, -offsetTop);
        //rt.offsetMin = new Vector2(fixedLeft, offsetBottom);

        rt.offsetMax = new Vector2(rt.offsetMax.x, -offsetTop);
        rt.offsetMin = new Vector2(rt.offsetMin.x, offsetBottom);

    }

    private void setAnnotationPosition(GameObject ann, double currPos, bool isLeft)
    {
        if (!ann.GetComponent<AnnotationExtension>().isOpen)
        {
            double percentOnScreen;
            if (receivedAnnotations.Count == 0)
            {
                if (receivedRelayoutAnnotations.Count == 1)
                {
                    percentOnScreen = 50;
                }
                else if (receivedRelayoutAnnotations.Count == 2)
                {
                    if (currPos == 0)
                    {
                        percentOnScreen = 33;
                    }
                    else
                    {
                        percentOnScreen = 66;
                    }
                }
                else
                {
                    percentOnScreen = (double)Math.Round((double)(100 * (currPos + 1)) / receivedRelayoutAnnotations.Count);
                }

            }
            else
            {
                if (receivedAnnotations.Count == 1)
                {
                    percentOnScreen = 50;
                }
                else if (receivedAnnotations.Count == 2)
                {
                    if (currPos == 0)
                    {
                        percentOnScreen = 33;
                    }
                    else
                    {
                        percentOnScreen = 66;
                    }
                }
                else
                {
                    percentOnScreen = (double)Math.Round((double)(100 * (currPos + 1)) / receivedAnnotations.Count);
                }

            }
            float offsetY = (float)((percentOnScreen * (maxY - minY) / 100) + minY);

            offsetAnnotation.x = 0.21f;


            offsetAnnotation.z = offsetY;
            ann.transform.position = phoneHolder.transform.position + offsetAnnotation.x * phoneHolder.transform.right + offsetAnnotation.z * phoneHolder.transform.forward;
            ann.transform.rotation = phoneHolder.transform.rotation * offsetRotation;
        } else
        {
            offsetAnnotation.x = 0.26f;
            float offsetY = (float)((50 * (maxY - minY) / 100) + minY);
            offsetAnnotation.z = offsetY;
            offsetAnnotation.y = 0.04f;
            ann.transform.position = phoneHolder.transform.position + offsetAnnotation.x * phoneHolder.transform.right + offsetAnnotation.z * phoneHolder.transform.forward + offsetAnnotation.y * phoneHolder.transform.up;
            ann.transform.rotation = phoneHolder.transform.rotation * offsetRotation;

        }

    }

    private GameObject FindAnnotation(string id)
    {
        foreach (GameObject annotation in annotations)
        {
            if (annotation.name == id)
            {
                return annotation;
                break;
            }
        }
        return null;
    }

    private void OnDestroy()
    {
        socket.Disconnect();
    }

    public class Annotation
    {
        public string id;
        public float time;
        public float interval;
        public string color;
        public int active;
    }

    public class ImageAnnotation
    {
        public string id;
        public string color;
        public float time;
        public float interval;
        public string img;
    }
}
